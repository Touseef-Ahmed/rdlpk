<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		
		<meta name="author" content="RoyalOrchard" />
		<!-- Stylesheets
		============================================= -->
		<link href="{{URL::asset('Content/font.css')}}" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="{{URL::asset('Content/font-awesome/css/font-awesome.min.css')}}">
		<link rel="stylesheet" href="{{URL::asset('Content/main.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{URL::asset('Content/css/swiper.css')}}" type="text/css" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->
		<!-- Document Title
		============================================= -->
		<title>Royal Orchard</title>
	</head>
	<body class="stretched no-transition">
		<!-- Document Wrapper
		============================================= -->
		<div id="wrapper" class="clearfix">
			<!-- Header============================================= -->						
			@extends('layouts.header')
<!-- #header end -->
<section id="slider" class="slider-parallax swiper_wrapper full-screen clearfix" data-autoplay="7000" data-speed="650" data-loop="true">
<div class="slider-parallax-inner">
<div class="swiper-container swiper-parent">
	<div class="swiper-wrapper">
		@foreach($slider as $slider)
		<div class="swiper-slide dark" style="background-image: url('{{url('/images/slider/'.$slider->image)}}');">
			<div class="container clearfix">
				<div class="slider-caption slider-caption-center">
					<h2 data-caption-animate="fadeInUp"></h2>
					<!--<p data-caption-animate="fadeInUp" data-caption-delay="200">Turning vision into values.</p>-->
				</div>
			</div>
		</div>
	@endforeach
	</div>
	<div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
	<div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
	<div id="slide-number"><div id="slide-number-current"></div><span>/</span><div id="slide-number-total"></div></div>
	<div class="swiper-pagination"></div>
</div>
</div>
</section>
<!-- Content
============================================= -->
<section id="content">
<div class="section-links" style="background-color: #dfa552; height: 75px;">
<div style="margin-left: 10px;" class="col-sm-6 col-md-6">
	<div>
		<h3 style="color: White; margin-top: 15px;">"Every thing is designed, few things are designed well."</h3>
	</div>
</div>
<div style="margin-top: 15px;" class="col-sm-5 col-md-5">
	<div class="col-sm-6 col-md-2" style="margin-right: 50px;">
		<div>
			<!--  <img class="image_fade" src="images/quick-links/download-icon.png" alt="Image" style="display: block; height: 75px; width: 225px; opacity: 1;">-->
			<a href="Images/Downloads/Brochure.pdf" target="_blank"><button type="button" class="btn btn-primary">Download Brochure</button></a>
		</div>
	</div>
	<div class="col-sm-6 col-md-2" style="margin-right: 50px;">
		<div>
			<!-- <img class="image_fade" src="images/quick-links/terms-conditions.jpg" alt="Image" style="display: block; height: 50px; width: 180px; margin-top: 15px; opacity: 1;">-->
			<a href="Images/Downloads/Terms_Conditions.pdf" target="_blank"><button type="button" class="btn btn-primary">Terms &amp; Conditions</button></a>
		</div>
	</div><div class="col-sm-6 col-md-2">
	<div>
		<!-- <img class="image_fade" src="images/quick-links/terms-conditions.jpg" alt="Image" style="display: block; height: 50px; width: 180px; margin-top: 15px; opacity: 1;">-->
		<a target="_blank" href="http://rdlpk.com/index.php/member/member">	<button type="button" class="btn btn-primary">Member's Portal</button></a>
		

	</div>
</div>
</div>
</div>

<div>
<div class="content-wrap">
<div class="container clearfix">
<div class="row clearfix">
	<div class="col-lg-5">
		
		<h3>About Royal Orchard Housing</h3>
		
		<p style="text-align:justify;">Royal orchard housing schemes are a well-recognized name the league of high–end housing projects across Pakistan. Unmatched in structural planning, design and civic facilities, the projects are a master piece of modern construction and unique features of international standards.
			Each scheme is a manifestation of the lifestyle of its inhabitants since each house is a custom built unit to perfectly accommodate the personality and lifestyle of its owners.
		The crafting and the designing of each aspect is done with artistic perspective which just adds more charm to each unit. Each housing scheme is surrounded by lush greenery from all the sides which gives a fresh airy atmosphere to live in. it is a delight to live in a home that offers a wealth of features and amenities.</p>
	</div>
	<div class="col-lg-7">
		<div style="position: relative; margin-bottom: -60px;" class="ohidden" data-height-lg="426" data-height-md="567" data-height-sm="470" data-height-xs="287" data-height-xxs="183">
			
			<iframe src="https://player.vimeo.com/video/303851855" width="560" height="315" frameborder="0" rel="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			<!--		<iframe src="https://player.vimeo.com/video/227220832" width="560" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			-->
		</div>
	</div>
</div>
</div>
<div class="section nobottommargin">
<div class="heading-block title-center page-section">
	<h3>Our Associates</h3>
	
</div><br />
<div class="row topmargin-sm clearfix">
	@foreach($associates as $associates)
	<div class="col-md-4 bottommargin">
		<img src="{{url('/images/associates/'.$associates->image)}}" style="height:100px; margin-left:47px;"/>
		
		<p style="text-align:justify;"> {{$associates->detail}}<a target="_blank" href="http://{{$associates->link}}"><input type="button" class="btn btn-primary btn-xs" value="More Detail" /> </a></p>
		</div>
		@endforeach
	
		</div>
	</div>
</div>
<!-- Content
============================================= -->
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div id="section-features" class="heading-block title-center page-section">
				<h2>Our Key Features</h2>
				<div>A concept of 24/7 residential support serices make Royal Orchard an icon of comfort</div>
			</div>
			<!-- Portfolio Filter
			============================================= -->
				
			<ul class="portfolio-filter clearfix" data-container="#portfolio">
				<li class="activeFilter"><a href="#" data-filter="*">Show All</a></li>
				@foreach($kfcategory as $kfcategory)

				<li><a href="#" data-filter=".{{$kfcategory->id}}">{{$kfcategory->name}}</a></li>
						
							@endforeach
				</ul><!-- #portfolio-filter end -->
			
				<div class="portfolio-shuffle" data-container="#portfolio">
					<i class="icon-random"></i>
				</div>
				<div class="clear"></div>
				<!-- Portfolio Items
				============================================= -->
				<!---------------------------INFRASTRUCTURE START------------------------------------>
			
				<div id="portfolio" class="portfolio grid-container grid-container clearfix">
					@foreach($kfeature as $kfeature)
								 <div class="col-sm-4 col-md-4 portfolio-item pf-icons {{$kfeature->kfcategory->id}}">
									<h4><a>{{$kfeature['name']}}</a></h4>
									<div class="fslider" data-arrows="false" data-speed="400" data-pause="4000">
									
												@php
$name = DB::table('kfgalleries')->where('kfcategory_id','=', $kfeature->kfcategory->id)->where('kfeature_id','=', $kfeature->id)->pluck('image');
												@endphp 
												 @foreach($name as $kfgallery)
													
													<div class="flexslider">
												 <div class="slider-wrap">
												<div class="slide"><a href="#"><img src="{{url('images/kfeature/'.$kfgallery)}}" alt="{{$kfgallery}}"></a></div>
											</div>
												 <br>
												
										</div>@endforeach
									</div>
								</div>    
					@endforeach
						
						</div><!-- #portfolio end --> 
						
					</div>
				</div>
				</section><!-- #content end -->
				
				<!--Current Project start-->
				<div class="container clearfix">
					<div class="section">
						<div class="heading-block title-center page-section">
							
							<h3>Current Projects</h3>
							<div>Explore our current projects of Royal Orchard Housing</div>
							
						</div>
						
						
						<div class="container clear-bottommargin clearfix">
							<div class="row">
								@foreach($cprojects as $cprojects)
								<div class="col-md-4 col-sm-6 bottommargin">
									<div class="ipost clearfix">
										<div class="">
											<a target="_blank" href="Multan.html">
		
			<img  src="{{url('/images/projects/'.$cprojects->imagemain)}}" style="width:250px;" class="image_fade"/>
										</div>
										<div style="padding-left:50px;">
											{{$cprojects->project_name}}
										</div>
										
										<div>
											<p style="padding-left:50px;">
												<a target="_blank" href="{{$cprojects->url}}">
													<button type="button" class="btn btn-primary">View Detail</button>
												</a></p>
											</div>
										</div>
									</div>@endforeach
								
										</div>
									</div>
								</div>
								<div class="line"></div>
							</div>
							<!--Current Project end-->
							<!--Villas/Homes start-->
							<div class="container clearfix">
								<div class="">
									<div class="container clearfix">
										<div class="heading-block center">
											<h3>Villas/Homes</h3>
										</div>
									</div>
									
									<div class="container clear-bottommargin clearfix">
										<div class="row">
											<div class="col-md-3 col-sm-6 bottommargin">
												<div class="ipost clearfix">
													<div class="">
														<a href="#"><img style="height: 170px; opacity: 1;" class="image_fade" src="Images/projects/ROMN/Progress/Villas/1-Kanal/1.jpg" alt="Image"></a>
													</div>
													<div style="font-size:17px;">
														Royal Villas
													</div>
													
													<div>
														<p>
															
															<a href="#" class="btn btn-primary"><span>View Detail</span></a></p>
														</div>
													</div>
												</div>
												<div class="col-md-3 col-sm-6 bottommargin">
													<div class="ipost clearfix">
														<div >
															<a href="#"><img class="image_fade" style="height: 170px; opacity: 1;" src="Images/projects/ROMN/Progress/Villas/Nova_Homes/Night_View.jpg" alt="Image"></a>
														</div>
														<div style="font-size:17px;">
															Nova-HRL Homes
														</div>
														
														<div>
															<p><a href="#" class="btn btn-primary"><span>View Detail</span></a></p>
														</div>
													</div>
												</div>
												<div class="col-md-3 col-sm-6 bottommargin">
													<div class="ipost clearfix">
														<div >
															<a href="#"><img class="image_fade" src="Images/projects/ROMN/Progress/Villas/Royal_Homes/02.jpg" alt="Image"></a>
														</div>
														<div style="font-size:17px;">
															Royal Homes
														</div>
														
														<div>
															<p><a href="#" class="btn btn-primary"><span>View Detail</span></a></p>
														</div>
													</div>
												</div>
												<div class="col-md-3 col-sm-6 bottommargin">
													<div class="ipost clearfix">
														<div >
															<a href="#"><img class="image_fade" src="Images/projects/ROMN/Houses_Under_Construction/02_1-Kanal.jpg" alt="Image"></a>
														</div>
														<div style="font-size:17px;">
															Members Homes
														</div>
														
														<div>
															<p><a href="#" class="btn btn-primary"><span>View Detail</span></a></p>								</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="line"></div>
									</div>
									<!--Villas/Homes end-->
									<!--Upcoming project start-->
									<div>
										<div class="container clearfix">
											<div class="heading-block center">
												<h3>Upcoming Projects</h3>
											</div>
										</div>
									</div>
									<div class="container clear-bottommargin clearfix">
										<div class="row">
										<div id="oc-clients-full" class="owl-carousel image-carousel carousel-widget" data-margin="30" data-loop="true" data-nav="false" data-autoplay="5000" data-pagi="false"data-items-xxs="2" data-items-xs="3" data-items-sm="4" data-items-md="5" data-items-lg="6">
												@foreach($uprojects as $uprojects)<div class="oc-item"><a href="#"><img src="{{url('/images/projects/'.$uprojects->imagemain)}}" alt="Clients"></a></div>
												@endforeach
											</div>
										</div>
										
										
										
									</div>
									
									<!--Upcoming project end-->
									<div class="clear"></div>
									<!--News eventd start-->
									<div class="container clearfix">
										<section id="section-contact" class="page-section">
											<div id="Div3" class="heading-block title-center topmargin-lg page-section">
												<h2>NEWS AND EVENTS</h2>
												<span>Latest News and Events</span>
											</div>
											
											<div class="container clearfix">
												<div class="col_three_third nobottommargin">
													<div class="fancy-title title-border">
														<h4>Recent News & Events</h4>
													</div>@foreach($news as $news)
													<div class="col-md-4 col-sm-4">
														<div class="ipost clearfix">
															<div class="entry-image">
																<a target="_blank" href="{{url('web/news_detail/'.$news->id)}}"><img class="image_fade" src="{{url('/images/news/'.$news->image1)}}" alt="Image" style="opacity: 1;"></a>
															</div>
															<div class="entry-title">
																<h3><a target="_blank href="Multan/SJ_Visit_19.html">{{$news->heading}}</a></h3>
															</div>
															<ul class="entry-meta clearfix">
																<li><i class="icon-calendar3"></i> {{$news->created_at}}</li>
																
															</ul>
															<div class="entry-content">
																<p>{{$news->heading}}
																	<h5> <a target="_blank" href="{{url('web/news_detail/'.$news->id)}}"><input class="btn btn-primary btn-xs" value="More Detail" type="button"></a></h5>
																</p>
															</div>
														</div>
													</div>@endforeach
												
													<div class="clear"></div>
												</div>
												
												
											</div>
										</section>
									</div>
									<!--news events end-->
									
									<!--sales center start-->
									<div class="container clearfix">
										<div id="section-events" class="heading-block title-center topmargin-lg page-section">
											<h2>Contact Us</h2>
											<span>For booking & detail information, please visit our nearest sales/marketing office</span>
										</div>
										<div class="col-md-4 col-sm-4">
											<h4>Sales Centers</h4>
											@foreach($salescenters as $salescenters)
											<table style="font-size:10px; padding-top:0px; padding-bottom:3px;">
												<tbody>
													<tr>
														<td rowspan="5"><img class="imgclass"  src="{{url('/images/salescenters/'.$salescenters->image)}}"alt="Clients"></td> </tr>
														<tr><td><strong>{{$salescenters->name}}</strong></td></tr>
														<tr><td>{{$salescenters->address}}</td></tr>
														<tr><td>Tel: {{$salescenters->name}}</td></tr>
														<tr><td>Cel : {{$salescenters->cell}}</td></tr>
													</tbody>
												</table>
												@endforeach
												
												
														
															
														</div>
														@if($salescenters->name=='Central Office, Islamabad')
														<div class="col-md-8 col-sm-6 bottommargin">
															<h4>{{$salescenters->name}}</h4>
															<p>{{$salescenters->address}}<br/>
																Phone: {{$salescenters->telephone}}
																<br/>Cell: {{$salescenters->cell}}
																<br />Email : sales@royalorchard.pk
															</p>
															<div  style="height:300px;"><iframe src="{{$salescenters->map}}"></iframe></div>
														</div>
														@endif <div class="clear"></div>
														<div class="col-md-12 col-sm-12">
															<div class="col-md-9 col-sm-3"><img src="Images/call-us.png" />Speak to us on 111 444 475 &nbsp;&nbsp;&nbsp;&nbsp;
																Toll Free :   0800  (ROYAL)  76925
															</div>
															<div class="col-md-2 col-sm-3"><button type="button" class="btn btn-primary">Send Us an Email</button></div>
															
														</div>
													</div>
													<!-----sales center end--->
													<!---SUBSCRIBE US start-->
													<div class="section dark parallax nobottommargin"
														style="padding: 25px 0px; background-image:url('Images/parallax/1.jpg'); background-position: 50% -57.895px;" data-stellar-background-ratio="0.3">
														<div class="container clearfix">
															<div class="heading-block center">
																<h4>Subscribe Us</h4>
																<span>Get amazing offers, breaking news and latest Newseletters etc.</span>
															</div>
															<div class="col-md-5 col-sm-6">
																<div class="fancy-title title-border">
																	<h4>Latest Newsletter</h4>
																</div>
																<table style="font-size:12px; padding-top:0px; padding-bottom:3px;">
																	<tbody>
																		<tr>
																			<td rowspan="5"><img src="Images/newsletter-icon.png"alt="Clients"></td> </tr>
																			<tr><td><strong>"Moving against the tide for a better Housing Future".</strong></td></tr>
																			<tr><td>Open Newsletter archive >></td></tr>
																			<tr><td><a href="Images/news/Newsletter18.pdf" target="_blank"><button type="button" class="btn btn-primary">Download Newsletter</button></a></td></tr>
																			
																		</tbody>
																	</table>
																</div>
																<div class="col-md-6 col-sm-6">
																	<div class="fancy-title title-border">
																		<h4>Subscribe for Offers</h4>
																	</div>
																	<p>
																		Subscribe to Our Newsletter to get Important News, Amazing Offers
																	and latest development progress reports of Royal Orchard Housing Projects.</p>
																	<div class="widget-subscribe-form-result"></div>
																	<form id="widget-subscribe-form2" action="#" role="form" method="post" class="nobottommargin" novalidate="novalidate">
																		<div class="input-group divcenter">
																			<span class="input-group-addon"><i class="icon-email2"></i></span>
																			<input name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email" aria-required="true" type="email">
																			<span class="input-group-btn">
																				<button class="btn btn-default" type="submit">Subscribe</button>
																			</span>
																		</div>
																	</form>
																</div>
															</div>
														</div>
														<!-----SUBSCRIBE US end--->
														
														
														<!--Start of Tawk.to Script-->
														<script type="text/javascript">
														var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
														(function () {
														var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
														s1.async = true;
														s1.src = 'https://embed.tawk.to/592411778028bb7327047522/default';
														s1.charset = 'UTF-8';
														s1.setAttribute('crossorigin', '*');
														s0.parentNode.insertBefore(s1, s0);
														})();
														</script>
														<!--End of Tawk.to Script--></body>
														<!-- Mirrored from royalorchard.pk/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 22 Jul 2019 10:00:12 GMT -->
													</html>
													
													
													
												</div>;
											</section>
											<!-- #content end -->
											<!-- Footer
											============================================= -->
						@extends('layouts.footer')
							<!-- #footer end -->
							
						</div>
						<!-- #wrapper end -->
						<!-- Go To Top
						============================================= -->
						<div id="gotoTop" class="icon-angle-up"></div>
						<!-- External JavaScripts
						============================================= -->
						<script type="text/javascript" src="Scripts/js/jquery.js"></script>
						<!-- Footer Scripts
						============================================= -->
						<script type="text/javascript" src="Scripts/js/functions.js"></script>
						<script>
						(function (i, s, o, g, r, a, m) {
						i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
						(i[r].q = i[r].q || []).push(arguments)
						}, i[r].l = 1 * new Date(); a = s.createElement(o),
						m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
						})(window, document, 'script', 'www.google-analytics.com/analytics.html', 'ga');
						ga('create', 'UA-23255544-12', 'auto');
						ga('send', 'pageview');
						</script>
						<script>                                                                                                                                                                                                                                                                                                                   jQuery(document).ready(function (e) { e("#primary-menu > ul li").find('.new-badge').children("div").append('<span class="label label-danger" style="display:inline-block;margin-left:8px;position:relative;top:-1px;text-transform:none;">New</span>') });</script>
						<script type="text/javascript">window.NREUM || (NREUM = {}); NREUM.info = { "beacon": "bam.nr-data.net", "licenseKey": "839484a19a", "applicationID": "5289971", "transactionName": "ZQEDZxZUD0FZVkxfX1xLNEENGglGVVkXVVFcEgBAS1wPVl1NFkZYQg==", "queueTime": 0, "applicationTime": 2, "atts": "SUYAEV5OHE8=", "errorBeacon": "bam.nr-data.net", "agent": "" }</script>
					</body>
					<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
					<script>
					function initialize() {
					var map_canvas = document.getElementById('map_canvas');
					var map_options = {
					center: new google.maps.LatLng(33.5329332, 73.1296499),
					zoom: 15,
					mapTypeId: google.maps.MapTypeId.ROADMAP
					}
					var map = new google.maps.Map(map_canvas, map_options)
					}
					google.maps.event.addDomListener(window, 'load', initialize);
					</script>
				</html>