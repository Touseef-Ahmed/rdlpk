<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable=['category_id,project_id','size_id','image',];
    public function project()
	    {
	        return $this->belongsTo('\App\Project');

	    }
	    public function category()
	    {
	        return $this->belongsTo('\App\Homes_category');

	    }
	    public function size()
	    {
	        return $this->belongsTo('\App\Size');

	    }
}
