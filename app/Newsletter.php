<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
	protected $fillable=['sort_order,project_id','title','status','link','file','thumbnail',];
    public function project()
	    {
	        return $this->belongsTo('\App\Project');

	    }
	    
}
