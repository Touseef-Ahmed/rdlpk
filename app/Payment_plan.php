<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment_plan extends Model
{
   protected $fillable=['type','project_id','duration_type','duration','booking','image_monthly','image_quarterly',];
    public function project()
	    {
	        return $this->belongsTo('\App\Project');

	    }
}
