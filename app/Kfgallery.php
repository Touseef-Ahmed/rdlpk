<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kfgallery extends Model
{
    public function kfeature()
	    {
	        return $this->belongsTo('\App\kfeature');

	    }
	    public function kfcategory()
    {
    	return $this->belongsTo('App\Kfcategory');
    }
}
