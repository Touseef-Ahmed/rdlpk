<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salescenter extends Model
{
   protected $fillable=['name','address','telephone','cell','image',];
}
