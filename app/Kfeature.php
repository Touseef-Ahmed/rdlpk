<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kfeature extends Model
{
    
    protected $fillable=['kfcategory_id','name','image1','image2','image3','image4','image5',];
    public function kfcategory()
    {
    	return $this->belongsTo('App\Kfcategory');
    }
}
