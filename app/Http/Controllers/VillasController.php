<?php
namespace App\Http\Controllers;
use App\Villas;
use App\Project;
use Illuminate\Http\Request;
class VillasController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
$villas=villas::all();
$projects=project::all();
return view('villas.index',compact('villas','projects'));
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
//
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
//
}
/**
* Display the specified resource.
*
* @param  \App\Villas  $villas
* @return \Illuminate\Http\Response
*/
public function show(Villas $villas)
{
//
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Villas  $villas
* @return \Illuminate\Http\Response
*/
public function edit(Villas $villas)
{
//
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Villas  $villas
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Villas $villas)
{
//
}
/**
* Remove the specified resource from storage.
*
* @param  \App\Villas  $villas
* @return \Illuminate\Http\Response
*/
public function destroy(Villas $villas)
{
//
}
}