<?php
namespace App\Http\Controllers;
use App\Setting;
use App\Web;
use App\Slider;
use App\Associates;
use App\Kfcategory;
use App\Kfeature;
use App\Project;
use App\news;
use App\kfgallery;
use App\Salescenter;
use App\broucher;
use App\Newsletter;
use App\Project_location;
use App\Gallery;
use App\Master_plan;
use App\Sfeature;
use App\file;
use App\Royal_medicare;
use App\Payment_plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Auth;
class WebController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function home()
{
	if(Auth::Check())
	{
	return view('home');
	}else{
		return redirect('/login');
	}
}
/****Admin*****/
public function login()
{
//return view('login');
return view('web.login1');
}
/**************/
public function index()
{  
$setting=Setting::all();
$slider=Slider::all();
$associates=Associates::all();
$kfcategory=Kfcategory::all();
$kfeature=Kfeature::all();
$kfgallery=kfgallery::all();
$cprojects=project::all() ->where('project_status', '=', 'Current');
$uprojects=project::all() ->where('project_status', '=', 'Upcoming');
$news=news::all() ->where('id', '>', 1);
$salescenters=Salescenter::all();
return view(('web.index'),compact('setting','slider','associates','kfgallery','kfcategory','kfeature','cprojects','uprojects','news','salescenters'));
}
public function getImages(Request $request)
{
$data=12121;
return  $data;
}
public function header()
{

$setting=Setting::all();
$slider=Slider::all();
$associates=Associates::all();
$kfcategory=Kfcategory::all();
$kfeature=Kfeature::all();
$cprojects=project::all() ->where('project_status', '=', 'Current');
$uprojects=project::all() ->where('project_status', '=', 'Upcoming');
$news=news::all() ->where('id', '>', 1);
$salescenters=Salescenter::all();
return view(('layouts.header'),compact('setting','slider','associates','kfcategory','kfeature','cprojects','uprojects','news','salescenters'));
}
public function footer()
{

$setting=Setting::all();
$slider=Slider::all();
$associates=Associates::all();
$kfcategory=Kfcategory::all();
$kfeature=Kfeature::all();
$cprojects=project::all() ->where('project_status', '=', 'Current');
$uprojects=project::all() ->where('project_status', '=', 'Upcoming');
$news=news::all() ->where('id', '>', 1);
$salescenters=Salescenter::all();

return view(('layouts.footer'),compact('setting','slider','associates','kfcategory','kfeature','cprojects','uprojects','news','salescenters'));
}
public function mission()
{

return view('web.mission');
}
public function objective()
{
return view('web.objective');
}
public function brouchers()
{
return view('web.brouchers');
}
public function newsletters()
{	$projects=project::all();
    $newsletters=Newsletter::all();
    return view('web.newsletters',compact('newsletters','projects'));
}
public function news()
{	$projects=project::all();
    $news=News::all();
    return view('web.news',compact('news','projects'));
}
public function news_detail($id)
{	$projects=project::all();
    $news=News::find($id);
     $dp=Web::getDevelopmentProgress()->where('project_id','=','1');
    return view('web.news_detail',compact('news','projects','dp'));
}


public function contactus()
{
	$setting=Setting::all();
	$salescenters=Salescenter::all();
	 return view('web.contactus',compact('setting','salescenters'));
}

public function multan()
{
	$location=Project_location::all()->where('project_id','=','1');
	$projects=project::all()->where('id','=','1');
	$gallery=Gallery::all()->where('project_id','=','1');
	$plan=Master_plan::all()->where('project_id','=','1');
	$sfeature=Sfeature::all()->where('project_id','=','1');
	$file=file::all()->where('project_id','=','1');
	$rhomes = Web::getRoyalHomes()->where('project_id','=','1');
	$rvillas1k = Web::getRoyalVillas1k()->where('project_id','=','1');
	$rvillas12m = Web::getRoyalVillas12m()->where('project_id','=','1');
	$rvillas10m = Web::getRoyalVillas10m()->where('project_id','=','1');
	$mr1k = Web::getMemberResidence1k()->where('project_id','=','1');
	$mr2k = Web::getMemberResidence2k()->where('project_id','=','1');
	$mr4k = Web::getMemberResidence4k()->where('project_id','=','1');
    $medicare=Royal_medicare::all()->where('project_id','=','1');
     $pplan=Payment_plan::all()->where('project_id','=','1');
     $dp=Web::getDevelopmentProgress()->where('project_id','=','1');
    $news=Web::getNews()->where('project_id','=','1');
	return view('web.multan',compact('projects','location','gallery','plan','sfeature','file','rhomes','rvillas1k','rvillas12m','rvillas10m','mr1k','mr2k','mr4k','medicare','pplan','dp','news'));
}
public function sargodha()
{
	$location=Project_location::all()->where('project_id','=','2');
	$projects=project::all()->where('id','=','2');
	$gallery=Gallery::all()->where('project_id','=','2');
	$plan=Master_plan::all()->where('project_id','=','2');
	$sfeature=Sfeature::all()->where('project_id','=','2');
	$file=file::all()->where('project_id','=','2');
	$rhomes = Web::getRoyalHomes()->where('project_id','=','2');
	$rvillas1k = Web::getRoyalVillas1k()->where('project_id','=','2');
	$rvillas12m = Web::getRoyalVillas12m()->where('project_id','=','2');
	$rvillas10m = Web::getRoyalVillas10m()->where('project_id','=','2');
	$mr1k = Web::getMemberResidence1k()->where('project_id','=','2');
	$mr2k = Web::getMemberResidence2k()->where('project_id','=','2');
	$mr4k = Web::getMemberResidence4k()->where('project_id','=','2');
    $medicare=Royal_medicare::all()->where('project_id','=','2');
     $pplan=Payment_plan::all()->where('project_id','=','2');
     $dp=Web::getDevelopmentProgress()->where('project_id','=','2');
    $news=Web::getNews()->where('project_id','=','2');
	return view('web.sargodha',compact('projects','location','gallery','plan','sfeature','file','rhomes','rvillas1k','rvillas12m','rvillas10m','mr1k','mr2k','mr4k','medicare','pplan','dp','news'));
}
public function sahiwal()
{
	$location=Project_location::all()->where('project_id','=','3');
	$projects=project::all()->where('id','=','3');
	$gallery=Gallery::all()->where('project_id','=','3');
	$plan=Master_plan::all()->where('project_id','=','3');
	$sfeature=Sfeature::all()->where('project_id','=','3');
	$file=file::all()->where('project_id','=','3');
	$rhomes = Web::getRoyalHomes()->where('project_id','=','3');
	$rvillas1k = Web::getRoyalVillas1k()->where('project_id','=','3');
	$rvillas12m = Web::getRoyalVillas12m()->where('project_id','=','3');
	$rvillas10m = Web::getRoyalVillas10m()->where('project_id','=','3');
	$mr1k = Web::getMemberResidence1k()->where('project_id','=','3');
	$mr2k = Web::getMemberResidence2k()->where('project_id','=','3');
	$mr4k = Web::getMemberResidence4k()->where('project_id','=','3');
    $medicare=Royal_medicare::all()->where('project_id','=','3');
     $pplan=Payment_plan::all()->where('project_id','=','3');
     $dp=Web::getDevelopmentProgress()->where('project_id','=','3');
    $news=Web::getNews()->where('project_id','=','2');
	return view('web.sahiwal',compact('projects','location','gallery','plan','sfeature','file','rhomes','rvillas1k','rvillas12m','rvillas10m','mr1k','mr2k','mr4k','medicare','pplan','dp','news'));
}
 public function mail()
 {
 	$name = 'Touseef';
   Mail::to('tochee147@gmail.com')->send(new SendMailable($name));
   
   return 'Email was sent';

 }
}