<?php
namespace App\Http\Controllers;
use App\Project;
use App\Gallery;
use Auth;
use Illuminate\Http\Request;
class GalleryController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
/**Start: Code for Search***/
public function search(Request $request)
{
if ($request->isMethod('get')){
$rules = [
'title' => 'required', 'project_id' => 'required'];
$this->validate($request, $rules);
$title = $request->input('title');
$project_id = $request->input('project_id');
$gallery = Gallery::where([
['title', 'LIKE', '%' . $title . '%'],
['project_id', 'LIKE', '%' . $project_id . '%'],
])->get();
$projects=project::all();
return view('gallery.index', compact('gallery','projects'))->with('success','Searched Successfully');
}else{
return redirect('/project_location')->with('error','Error!!');
}
}
/***END: Code For Search***/
public function index()
{
if(AUth::Check()){
$gallery=gallery::all();
$projects=Project::all();
return view('gallery.index',compact('gallery','projects'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{ if(Auth::check()){
$projects=project::all();
return view('gallery.create',compact('projects'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
$this->validate($request,[
'project_id' => 'required',
'title'=>'required',
'image.*' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
]);
if($request->hasFile('image'))
{
$names = [];
foreach($request->file('image') as $image)
{
$destinationPath = 'images/gallery/';
$filename = $image->getClientOriginalName();
$image->move($destinationPath, $filename);
array_push($names, $filename);
}
$request->image = json_encode($names);
}
$gallery=new gallery();
$gallery->title = request('title');
$gallery->project_id = request('project_id');
$gallery->image1=$names[0];
if(empty($names[1])){
$gallery->image2 = request('noimg'); }else{$gallery->image2=$names[1];}
if(empty($names[2])){
$gallery->image3 = request('noimg'); }else{$gallery->image3=$names[2];}
if(empty($names[3])){
$gallery->image4 = request('noimg'); }else{$gallery->image4=$names[3];}
if(empty($names[4])){
$gallery->image5 = request('noimg'); }else{$gallery->image5=$names[4];}
if(empty($names[5])){
$gallery->image6 = request('noimg'); }else{$gallery->image6=$names[5];}
if(empty($names[6])){
$gallery->image7 = request('noimg'); }else{$gallery->image7=$names[6];}
if(empty($names[7])){
$gallery->image8 = request('noimg'); }else{$gallery->image8=$names[7];}
if(empty($names[8])){
$gallery->image9 = request('noimg'); }else{$gallery->image9=$names[8];}
if(empty($names[9])){
$gallery->image10 = request('noimg'); }else{$gallery->image10=$names[9];}
$gallery->save();
if($gallery)
{
return redirect('gallery')->with('message','Gallery Addedd Successfully');
}
}
/**
* Display the specified resource.
*
* @param  \App\Gallery  $gallery
* @return \Illuminate\Http\Response
*/
public function show($id)
{
$gallery=Gallery::find($id);
$projects=Project::all();
return view('gallery.show',compact('gallery','projects'));
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Gallery  $gallery
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
    if(Auth::Check()){
$projects=project::all();
$gallery=Gallery::find($id);
return view('gallery.edit',compact('projects','gallery'));
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Gallery  $gallery
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Gallery $gallery)
{
if(!empty($request->file('image1')))
{
$image1=$request->file('image1');
$destinationPath = 'images/gallery/';
$filename1 = $image1->getClientOriginalName();
$image1->move($destinationPath, $filename1);
///echo $filename;exit;
}else
{
$filename1=$gallery->image1;
}
if(!empty($request->file('image2')))
{
$image2=$request->file('image2');
$destinationPath = 'images/gallery/';
$filename2 = $image2->getClientOriginalName();
$image2->move($destinationPath, $filename2);
}else
{
$filename2=$gallery->image2;
}
if(!empty($request->file('image3')))
{
$image3=$request->file('image3');
$destinationPath = 'images/gallery/';
$filename3 = $image3->getClientOriginalName();
$image3->move($destinationPath, $filename3);
}else
{
$filename3=$gallery->image3;
}
if(!empty($request->file('image4')))
{
$image4=$request->file('image4');
$destinationPath = 'images/gallery/';
$filename4 = $image4->getClientOriginalName();
$image4->move($destinationPath, $filename4);
}else
{
$filename4=$gallery->image4;
}
if(!empty($request->file('image5')))
{
$image5=$request->file('image5');
$destinationPath = 'images/gallery/';
$filename5 = $image5->getClientOriginalName();
$image5->move($destinationPath, $filename5);
}else
{
$filename5=$gallery->image5;
}
if(!empty($request->file('image6')))
{
$image6=$request->file('image6');
$destinationPath = 'images/gallery/';
$filename6 = $image6->getClientOriginalName();
$image6->move($destinationPath, $filename6);
}else
{
$filename6=$gallery->image6;
}
if(!empty($request->file('image7')))
{
$image7=$request->file('image7');
$destinationPath = 'images/gallery/';
$filename7 = $image7->getClientOriginalName();
$image7->move($destinationPath, $filename7);
}else
{
$filename7=$gallery->image7;
}
if(!empty($request->file('image8')))
{
$image8=$request->file('image8');
$destinationPath = 'images/gallery/';
$filename8 = $image8->getClientOriginalName();
$image8->move($destinationPath, $filename8);
}else
{
$filename8=$gallery->image8;
}
if(!empty($request->file('image9')))
{
$image9=$request->file('image9');
$destinationPath = 'images/gallery/';
$filename9 = $image9->getClientOriginalName();
$image9->move($destinationPath, $filename9);
}else
{
$filename9=$gallery->image9;
}
if(!empty($request->file('image10')))
{
$image10=$request->file('image10');
$destinationPath = 'images/gallery/';
$filename10 = $image10->getClientOriginalName();
$image10->move($destinationPath, $filename10);
}else
{
$filename10=$gallery->image10;
}
///echo $gallery->image2=$filename2;exit;
$this->validate($request,[
'title' => 'required',
'project_id' => 'required',
]);
///echo $gallery->id;exit;
$gallery=gallery::find($gallery->id);
$gallery->title=$request->title;
$gallery->project_id=$request->project_id;
$gallery->image1=$filename1;
$gallery->image2=$filename2;
$gallery->image3=$filename3;
$gallery->image4=$filename4;
$gallery->image5=$filename5;
$gallery->image6=$filename6;
$gallery->image7=$filename7;
$gallery->image8=$filename8;
$gallery->image9=$filename9;
$gallery->image10=$filename10;
$updated=$gallery->save();
if($updated)
{
return redirect('/gallery')->with('message','Gallery Successfully Updated');
}
}
/**
* Remove the specified resource from storage.
*
* @param  \App\Gallery  $gallery
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
$gallery=Gallery::findOrFail($id);
$gallery->delete();
if($gallery)
{
return redirect('gallery')->with('message','Gallery Deleted Successfully');
}
}
}