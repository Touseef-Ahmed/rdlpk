<?php
namespace App\Http\Controllers;
use App\Project;
use Auth;
use App\Royal_medicare;
use Illuminate\Http\Request;
class RoyalMedicareController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
if(Auth::Check()){
$projects=project::all();
$royal_medicare=royal_medicare::all();
return view('royal_medicare.index',compact('royal_medicare','projects'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
if(Auth::Check()){
$projects=project::all();
return view('royal_medicare.create',compact('projects'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
if(Auth::Check()){
$this->validate($request,[
'title'=>'required',
'project_id' => 'required',
'detail'=>'required',
'image.*' => 'required|image|mimes:jpeg,png,jpg,gif',
]);
if($request->hasFile('image'))
{
$names = [];
foreach($request->file('image') as $image)
{
$destinationPath = 'images/royal_medicare/';
$filename = $image->getClientOriginalName();
$image->move($destinationPath, $filename);
array_push($names, $filename);
}
$request->image = json_encode($names);
}
$royal_medicare=new royal_medicare();
$royal_medicare->title = request('title');
$royal_medicare->project_id = request('project_id');
$royal_medicare->detail = request('detail');
$royal_medicare->image1=$names[0];
if(empty($names[1])){
$royal_medicare->image2 = request('noimg'); }else{$royal_medicare->image2=$names[1];}
if(empty($names[2])){
$royal_medicare->image3 = request('noimg'); }else{$royal_medicare->image3=$names[2];}
if(empty($names[3])){
$royal_medicare->image4 = request('noimg'); }else{$royal_medicare->image4=$names[3];}
if(empty($names[4])){
$royal_medicare->image5 = request('noimg'); }else{$royal_medicare->image5=$names[4];}
if(empty($names[5])){
$royal_medicare->image6 = request('noimg'); }else{$royal_medicare->image6=$names[5];}
if(empty($names[6])){
$royal_medicare->image7 = request('noimg'); }else{$royal_medicare->image7=$names[6];}
if(empty($names[7])){
$royal_medicare->image8 = request('noimg'); }else{$royal_medicare->image8=$names[7];}
if(empty($names[8])){
$royal_medicare->image9 = request('noimg'); }else{$royal_medicare->image9=$names[8];}
if(empty($names[9])){
$royal_medicare->image10 = request('noimg'); }else{$royal_medicare->image10=$names[9];}
$royal_medicare->save();
if($royal_medicare)
{
return redirect('royal_medicare')->with('message','Royal Medicare Addedd Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Display the specified resource.
*
* @param  \App\Royal_medicare  $royal_medicare
* @return \Illuminate\Http\Response
*/
public function show($id)
{
if(Auth::check()){
$royal_medicare=royal_medicare::find($id);
$projects=Project::all();
return view('royal_medicare.show',compact('royal_medicare','projects'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Royal_medicare  $royal_medicare
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
if(Auth::Check()){
$projects=project::all();
$royal_medicare=royal_medicare::find($id);
return view('royal_medicare.edit',compact('projects','royal_medicare'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Royal_medicare  $royal_medicare
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Royal_medicare $royal_medicare)
{
if(Auth::Check()){
if(!empty($request->file('image1')))
{
$image1=$request->file('image1');
$destinationPath = 'images/royal_medicare/';
$filename1 = $image1->getClientOriginalName();
$image1->move($destinationPath, $filename1);
///echo $filename;exit;
}else
{
$filename1=$royal_medicare->image1;
}
if(!empty($request->file('image2')))
{
$image2=$request->file('image2');
$destinationPath = 'images/royal_medicare/';
$filename2 = $image2->getClientOriginalName();
$image2->move($destinationPath, $filename2);
}else
{
$filename2=$royal_medicare->image2;
}
if(!empty($request->file('image3')))
{
$image3=$request->file('image3');
$destinationPath = 'images/royal_medicare/';
$filename3 = $image3->getClientOriginalName();
$image3->move($destinationPath, $filename3);
}else
{
$filename3=$royal_medicare->image3;
}
if(!empty($request->file('image4')))
{
$image4=$request->file('image4');
$destinationPath = 'images/royal_medicare/';
$filename4 = $image4->getClientOriginalName();
$image4->move($destinationPath, $filename4);
}else
{
$filename4=$royal_medicare->image4;
}
if(!empty($request->file('image5')))
{
$image5=$request->file('image5');
$destinationPath = 'images/royal_medicare/';
$filename5 = $image5->getClientOriginalName();
$image5->move($destinationPath, $filename5);
}else
{
$filename5=$royal_medicare->image5;
}
if(!empty($request->file('image6')))
{
$image6=$request->file('image6');
$destinationPath = 'images/royal_medicare/';
$filename6 = $image6->getClientOriginalName();
$image6->move($destinationPath, $filename6);
}else
{
$filename6=$royal_medicare->image6;
}
if(!empty($request->file('image7')))
{
$image7=$request->file('image7');
$destinationPath = 'images/royal_medicare/';
$filename7 = $image7->getClientOriginalName();
$image7->move($destinationPath, $filename7);
}else
{
$filename7=$royal_medicare->image7;
}
if(!empty($request->file('image8')))
{
$image8=$request->file('image8');
$destinationPath = 'images/royal_medicare/';
$filename8 = $image8->getClientOriginalName();
$image8->move($destinationPath, $filename8);
}else
{
$filename8=$royal_medicare->image8;
}
if(!empty($request->file('image9')))
{
$image9=$request->file('image9');
$destinationPath = 'images/royal_medicare/';
$filename9 = $image9->getClientOriginalName();
$image9->move($destinationPath, $filename9);
}else
{
$filename9=$royal_medicare->image9;
}
if(!empty($request->file('image10')))
{
$image10=$request->file('image10');
$destinationPath = 'images/royal_medicare/';
$filename10 = $image10->getClientOriginalName();
$image10->move($destinationPath, $filename10);
}else
{
$filename10=$royal_medicare->image10;
}
$this->validate($request,[
'title' => 'required',
'project_id' => 'required',
'detail'=>'required',]);
$royal_medicare=royal_medicare::find($royal_medicare->id);
$royal_medicare->title=$request->title;
$royal_medicare->project_id=$request->project_id;
$royal_medicare->detail=$request->detail;
$royal_medicare->image1=$filename1;
$royal_medicare->image2=$filename2;
$royal_medicare->image3=$filename3;
$royal_medicare->image4=$filename4;
$royal_medicare->image5=$filename5;
$royal_medicare->image6=$filename6;
$royal_medicare->image7=$filename7;
$royal_medicare->image8=$filename8;
$royal_medicare->image9=$filename9;
$royal_medicare->image10=$filename10;
$updated=$royal_medicare->save();
if($updated)
{
return redirect('/royal_medicare')->with('message','Royal Medicare Successfully Updated');
}
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Remove the specified resource from storage.
*
* @param  \App\Royal_medicare  $royal_medicare
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
if(Auth::Check()){
$royal_medicare=royal_medicare::findOrFail($id);
$royal_medicare->delete();
if($royal_medicare)
{
return redirect('royal_medicare')->with('message','Royal Medicare Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
}