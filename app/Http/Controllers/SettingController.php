<?php
namespace App\Http\Controllers;
use App\Setting;
use Auth;
use Illuminate\Http\Request;
class SettingController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{if(Auth::Check())
{
$setting=Setting::all();
return view('setting.index',compact('setting'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
return view('setting.create');
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
if(Auth::Check()){
$this->validate($request,[
'name' => 'required',
'value' => 'required',
]);
$setting=new setting();
$setting->name = request('name');
$setting->value = request('value');
$setting->save();
if($setting)
{
return redirect('setting')->with('message','Setting Addedd Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Display the specified resource.
*
* @param  \App\Setting  $setting
* @return \Illuminate\Http\Response
*/
public function show(Setting $setting)
{
//
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Setting  $setting
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
if(Auth::Check()){
$setting=Setting::find($id);
return view('setting.edit',compact('setting'));
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Setting  $setting
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Setting $setting)
{
if(Auth::Check()){
$this->validate($request,[
'name' => 'required',
'value' => 'required',
]);
$setting=setting::find($setting->id);
$setting->name = request('name');
$setting->value = request('value');
$updated=$setting->save();
if($updated)
{
return redirect('setting')->with('message','Setting updated Successfully');
}
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Remove the specified resource from storage.
*
* @param  \App\Setting  $setting
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
if(Auth::Check()){
$setting=setting::findOrFail($id);
$setting->delete();
if($setting)
{
return redirect('setting')->with('message','Setting Deleted Successfully');
}
}else{
            Auth::logout();
           return redirect('/login');
        }
}
}