<?php
namespace App\Http\Controllers;
use App\Slider;
use Auth;
use Validator, Input, Redirect; 

use Illuminate\Http\Request;
class SliderController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
	if(Auth::Check()){
// $slider=slider::all()-;
///$slider = slider::where('status', '=','1' )->get();
return view('slider.index',compact('slider'));
}else{
Auth::logout();
return redirect('/login');
}
}
public function getSliderData(){
    	
        $slider = Slider::get();
        return json_encode(array('data'=>$slider));
    }
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{ 
	if(Auth::Check()){
return view('slider.create');
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
function store(Request $request)
{
$validation = Validator::make($request->all(), [
'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
'title'=>'required',
'status'=>'required'
]);
if($validation->passes())
{
$image = $request->file('image');
$new_name = rand() . '.' . $image->getClientOriginalExtension();
$request->image->move('images/slider/',$new_name);	
$oldval=slider::orderBy('sort_order', 'desc')->first();
 $old=$oldval['sort_order'];
 if($old==0)
 {
 $old=1;
 }
 else {

 $old=$old+1;
 }
	$slider=new slider();
		$slider->title = request('title');
		$slider->status = request('status');
		$slider->image = $new_name;
		$slider->sort_order = $old;
		$slider->save();
		if($slider)
		{
		

return response()->json([
'message'   => 'Image Upload Successfully',
'uploaded_image' => '<img src="royalorchard/images/slider/'.$new_name.'" class="img-thumbnail" width="300" />',
'class_name'  => 'alert-success'
]);
return json_encode(array(
"statusCode"=>200
));
			}


}
else
{
return response()->json([
'message'   => $validation->errors()->all(),
'uploaded_image' => '',
'class_name'  => 'alert-danger'
]);
}
}

public function store123(Request $request)
{
	if(Auth::Check()){
///  $old=slider::orderBy('sort_order', 'desc')->find(1)->sort_order;
$oldval=slider::orderBy('sort_order', 'desc')->first();
$old=$oldval['sort_order'];
if($old==0)
{
$old=1;
}
else
{
////echo 'outside 0';
$old=$old+1;
}
$this->validate($request,[
'title' => 'required',
'status'=>'required',
'image' => 'required|image|mimes:jpeg,png,jpg|max:10048',
]);
$image = $request->file('select_file');
      $filename = rand() . '.' . $image->getClientOriginalExtension();
      $image->move(public_path('images'), $filename);
      return response()->json([
       'message'   => 'Image Upload Successfully',
       'uploaded_image' => '<img src="/images/slider'.$filename.'" class="img-thumbnail" width="300" />',
       'class_name'  => 'alert-success'
      ]);
/*if($request->hasFile('image') && $request->image->isValid())
{
$extension=$request->image->extension();
$filename=time()."_.".$extension;
$request->image->move('images/slider/',$filename);
} else
{
$filename="no-image.png";
}*/
$slider=new slider();
$slider->title = request('title');
$slider->status = request('status');
$slider->image = $filename;
$slider->sort_order = $old;
$slider->save();
if($slider)
{
return redirect('slider')->with('message','Slider Addedd Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Display the specified resource.
*
* @param  \App\Slider  $slider
* @return \Illuminate\Http\Response
*/
public function show(Slider $slider)
{
//
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Slider  $slider
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
	if(Auth::Check()){
$slider=slider::find($id);
return view('slider.edit',compact('slider'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Slider  $slider
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Slider $slider)
{
	if(Auth::Check()){
if(!empty($request->file('image')))
{
$image=$request->file('image');
$destinationPath = 'images/slider/';
$filename1 = $image->getClientOriginalName();
$image->move($destinationPath, $filename1);
//echo $filename1;exit;
}else
{
$filename1=$slider->image;
}
$this->validate($request,[
'title' => 'required',
'status'=>'required',]);
$slider=Slider::find($slider->id);
$slider->title = request('title');
$slider->status = request('status');
$slider->image = $filename1;
$slider->sort_order = $slider->sort_order;
$updated=$slider->save();
if($updated)
{
return redirect('slider')->with('message','Slider updated Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Remove the specified resource from storage.
*
* @param  \App\Slider  $slider
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
	if(Auth::Check()){
$slider=slider::findOrFail($id);
$slider->delete();
if($slider)
{
return redirect('slider')->with('message','Slider Deleted Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
}