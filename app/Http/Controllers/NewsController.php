<?php
namespace App\Http\Controllers;
use App\News;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Project;
class NewsController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
/**Start: Code for Search***/
public function search(Request $request)
{
if ($request->isMethod('get')){
$rules = [
'heading' => 'required', 'project_id' => 'required'];
$this->validate($request, $rules);
$heading = $request->input('heading');
$project_id = $request->input('project_id');
//  $news = News::where('heading', 'LIKE', '%'.$search.'%')->paginate(4);
/// $news = News::where('heading','LIKE','%'.$search.'%')->andWhere('project_id','LIKE','%'.$project_id.'%')->get();
$news = News::where([
['heading', 'LIKE', '%' . $heading . '%'],
['project_id', 'LIKE', '%' . $project_id . '%'],
])->get();
$projects=project::all();
return view('news.index', compact('news','projects'))->with('success','Searched Successfully');
}else{
return redirect('/news')->with('error','Error!!');
}
}
/***END: Code For Search***/
public function index()
{
if(Auth::Check()){
$news=news::all() ->where('id', '>', 1);
$projects=project::all();
return view('news.index',compact('news','projects'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
if(Auth::Check()){
$projects=project::all();
return view('news.create',compact('projects'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
if(Auth::Check()){
///$old=News ::orderBy('sort_order', 'desc')->first()->sort_order;
$old= News ::orderBy('sort_order', 'desc')->first()->sort_order;
$this->validate($request,[
'project_id' => 'required',
'heading'=>'required',
'location'=>'required',
'detail'=>'required',
'image.*' => 'required|image|mimes:jpeg,png,jpg,gif|max:7048',
]);
if($request->hasFile('image'))
{
$names = [];
foreach($request->file('image') as $image)
{
$destinationPath = 'images/news/';
$filename = $image->getClientOriginalName();
$image->move($destinationPath, $filename);
array_push($names, $filename);
}
$request->image = json_encode($names);
}
///print_r($names);exit;
$news=new News();
$news->heading = request('heading');
$news->project_id = request('project_id');
$news->location = request('location');
$news->detail = request('detail');
$news->sort_order = $old+1;
//add request
$news->image1=$names[0];
if(empty($names[1])){
$news->image2 = request('noimg'); }else{$news->image2=$names[1];}
if(empty($names[2])){
$news->image3 = request('noimg'); }else{$news->image3=$names[2];}
if(empty($names[3])){
$news->image4 = request('noimg'); }else{$news->image4=$names[3];}
if(empty($names[4])){
$news->image5 = request('noimg'); }else{$news->image5=$names[4];}
if(empty($names[5])){
$news->image6 = request('noimg'); }else{$news->image6=$names[5];}
if(empty($names[6])){
$news->image7 = request('noimg'); }else{$news->image7=$names[6];}
if(empty($names[7])){
$news->image8 = request('noimg'); }else{$news->image8=$names[7];}
if(empty($names[8])){
$news->image9 = request('noimg'); }else{$news->image9=$names[8];}
if(empty($names[9])){
$news->image10 = request('noimg'); }else{$news->image10=$names[9];}
$news->save();
if($news)
{
return redirect('news')->with('message','News Addedd Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Display the specified resource.
*
* @param  \App\News  $news
* @return \Illuminate\Http\Response
*/
public function show($id)
{
if(Auth::Check()){
$news=News::find($id);
$projects=Project::all();
return view('news.show',compact('news','projects'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\News  $news
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
if(Auth::Check()){
$projects=project::all();
$news=News::find($id);
return view('news.edit',compact('projects','news'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\News  $news
* @return \Illuminate\Http\Response
*/
public function update(Request $request,News $news)
{
if(Auth::Check()){
if(!empty($request->file('image1')))
{
$image1=$request->file('image1');
$destinationPath = 'images/news/';
$filename1 = $image1->getClientOriginalName();
$image1->move($destinationPath, $filename1);
///echo $filename;exit;
}else
{
$filename1=$news->image1;
}
if(!empty($request->file('image2')))
{
$image2=$request->file('image2');
$destinationPath = 'images/news/';
$filename2 = $image2->getClientOriginalName();
$image2->move($destinationPath, $filename2);
}else
{
$filename2=$news->image2;
}
if(!empty($request->file('image3')))
{
$image3=$request->file('image3');
$destinationPath = 'images/news/';
$filename3 = $image3->getClientOriginalName();
$image3->move($destinationPath, $filename3);
}else
{
$filename3=$news->image3;
}
if(!empty($request->file('image4')))
{
$image4=$request->file('image4');
$destinationPath = 'images/news/';
$filename4 = $image4->getClientOriginalName();
$image4->move($destinationPath, $filename4);
}else
{
$filename4=$news->image4;
}
if(!empty($request->file('image5')))
{
$image5=$request->file('image5');
$destinationPath = 'images/news/';
$filename5 = $image5->getClientOriginalName();
$image5->move($destinationPath, $filename5);
}else
{
$filename5=$news->image5;
}
if(!empty($request->file('image6')))
{
$image6=$request->file('image6');
$destinationPath = 'images/news/';
$filename6 = $image6->getClientOriginalName();
$image6->move($destinationPath, $filename6);
}else
{
$filename6=$news->image6;
}
if(!empty($request->file('image7')))
{
$image7=$request->file('image7');
$destinationPath = 'images/news/';
$filename7 = $image7->getClientOriginalName();
$image7->move($destinationPath, $filename7);
}else
{
$filename7=$news->image7;
}
if(!empty($request->file('image8')))
{
$image8=$request->file('image8');
$destinationPath = 'images/news/';
$filename8 = $image8->getClientOriginalName();
$image8->move($destinationPath, $filename8);
}else
{
$filename8=$news->image8;
}
if(!empty($request->file('image9')))
{
$image9=$request->file('image9');
$destinationPath = 'images/news/';
$filename9 = $image9->getClientOriginalName();
$image9->move($destinationPath, $filename9);
}else
{
$filename9=$news->image9;
}
if(!empty($request->file('image10')))
{
$image10=$request->file('image10');
$destinationPath = 'images/news/';
$filename10 = $image10->getClientOriginalName();
$image10->move($destinationPath, $filename10);
}else
{
$filename10=$news->image10;
}
///echo $news->image2=$filename2;exit;
$this->validate($request,[
'heading' => 'required',
'project_id' => 'required',
'location'=>'required',
'detail'=>'required',]);
///echo $news->id;exit;
$news=News::find($news->id);
$news->heading=$request->heading;
$news->project_id=$request->project_id;
$news->location=$request->location;
$news->detail=$request->detail;
$news->image1=$filename1;
$news->image2=$filename2;
$news->image3=$filename3;
$news->image4=$filename4;
$news->image5=$filename5;
$news->image6=$filename6;
$news->image7=$filename7;
$news->image8=$filename8;
$news->image9=$filename9;
$news->image10=$filename10;
$updated=$news->save();
if($updated)
{
return redirect('/news')->with('message','News Successfully Updated');
}
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Remove the specified resource from storage.
*
* @param  \App\News  $news
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
if(Auth::Check()){
$news=News::findOrFail($id);
$news->delete();
if($news)
{
return redirect('news')->with('message','News Deleted Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
}