<?php
namespace App\Http\Controllers;
use App\Sfeature;
use App\Project;
use Auth;
use Illuminate\Http\Request;
class SfeatureController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
	if(Auth::Check()){
	
$sfeature=Sfeature::all();
$projects=project::all();
return view('sfeature.index',compact('sfeature','projects'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
	if(Auth::Check()){
$projects=project::all();
return view('sfeature.create',compact('projects'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
	if(Auth::Check()){
$this->validate($request,[
'title' => 'required',
'project_id'=>'required',
'detail'=>'required',
'image.*' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
]);
if($request->hasFile('image'))
{
$names = [];
foreach($request->file('image') as $image)
{
$destinationPath = 'images/sfeatures/';
$filename = $image->getClientOriginalName();
$image->move($destinationPath, $filename);
array_push($names, $filename);
}
$request->image = json_encode($names);
}
$sfeature=new sfeature();
$sfeature->title = request('title');
$sfeature->project_id = request('project_id');
$sfeature->detail = request('detail');
$sfeature->image1=$names[0];
if(empty($names[1])){
$sfeature->image2 = request('noimg'); }else{$sfeature->image2=$names[1];}
if(empty($names[2])){
$sfeature->image3 = request('noimg'); }else{$sfeature->image3=$names[2];}
if(empty($names[3])){
$sfeature->image4 = request('noimg'); }else{$sfeature->image4=$names[3];}
if(empty($names[4])){
$sfeature->image5 = request('noimg'); }else{$sfeature->image5=$names[4];}
$sfeature->save();
if($sfeature)
{
return redirect('sfeature')->with('message','Feature Addedd Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Display the specified resource.
*
* @param  \App\Sfeature  $sfeature
* @return \Illuminate\Http\Response
*/
public function show($id)
{
	if(Auth::Check()){
$sfeature=sfeature::find($id);
$projects=Project::all();
return view('sfeature.show',compact('sfeature','projects'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Sfeature  $sfeature
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
	if(Auth::Check()){
$projects=project::all();
$sfeature=sfeature::find($id);
return view('sfeature.edit',compact('projects','sfeature'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Sfeature  $sfeature
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Sfeature $sfeature)
{
	if(Auth::Check()){
if(!empty($request->file('image1')))
{
$image1=$request->file('image1');
$destinationPath = 'images/sfeatures/';
$filename1 = $image1->getClientOriginalName();
$image1->move($destinationPath, $filename1);
///echo $filename;exit;
}else
{
$filename1=$sfeature->image1;
}
if(!empty($request->file('image2')))
{
$image2=$request->file('image2');
$destinationPath = 'images/sfeatures/';
$filename2 = $image2->getClientOriginalName();
$image2->move($destinationPath, $filename2);
}else
{
$filename2=$sfeature->image2;
}
if(!empty($request->file('image3')))
{
$image3=$request->file('image3');
$destinationPath = 'images/sfeatures/';
$filename3 = $image3->getClientOriginalName();
$image3->move($destinationPath, $filename3);
}else
{
$filename3=$sfeature->image3;
}
if(!empty($request->file('image4')))
{
$image4=$request->file('image4');
$destinationPath = 'images/sfeatures/';
$filename4 = $image4->getClientOriginalName();
$image4->move($destinationPath, $filename4);
}else
{
$filename4=$sfeature->image4;
}
if(!empty($request->file('image5')))
{
$image5=$request->file('image5');
$destinationPath = 'images/sfeatures/';
$filename5 = $image5->getClientOriginalName();
$image5->move($destinationPath, $filename5);
}else
{
$filename5=$sfeature->image5;
}
$this->validate($request,[
'title' => 'required',
'project_id' => 'required',
'detail'=>'required',]);
$sfeature=sfeature::find($sfeature->id);
$sfeature->title=$request->title;
$sfeature->detail=$request->detail;
$sfeature->image1=$filename1;
$sfeature->image2=$filename2;
$sfeature->image3=$filename3;
$sfeature->image4=$filename4;
$sfeature->image5=$filename5;
$updated=$sfeature->save();
if($updated)
{
return redirect('/sfeature')->with('message','Feature Successfully Updated');
}
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Remove the specified resource from storage.
*
* @param  \App\Sfeature  $sfeature
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
	if(Auth::Check()){
$sfeature=sfeature::findOrFail($id);
$sfeature->delete();
if($sfeature)
{
return redirect('sfeature')->with('message','Feature Deleted Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
}