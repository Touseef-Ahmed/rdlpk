<?php
namespace App\Http\Controllers;
use App\Salescenter;
use Auth;
use Illuminate\Http\Request;
class SalescenterController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
	if(Auth::Check()){
$salescenter=salescenter::all();
return view('salescenter.index',compact('salescenter'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
	if(Auth::Check()){
return view('salescenter.create');
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
	if(Auth::Check()){
$this->validate($request,[
'name' => 'required',
'address'=>'required',
'telephone'=>'required',
'cell'=>'required',
'image' => 'required|image|mimes:jpeg,png,jpg|max:10048',
]);
if($request->hasFile('image') && $request->image->isValid())
{
$extension=$request->image->extension();
$filename=time()."_.".$extension;
$request->image->move('images/salescenters/',$filename);
} else
{
$filename="no-image.png";
}
$salescenter=new salescenter();
$salescenter->name = request('name');
$salescenter->address = request('address');
$salescenter->telephone = request('telephone');
$salescenter->cell = request('cell');
$salescenter->image = $filename;
$salescenter->save();
if($salescenter)
{
return redirect('salescenter/')->with('message','Salescenter Addedd Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Display the specified resource.
*
* @param  \App\Salescenter  $salescenter
* @return \Illuminate\Http\Response
*/
public function show(Salescenter $salescenter)
{
//
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Salescenter  $salescenter
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
	if(Auth::Check()){
$salescenter=Salescenter::find($id);
return view('salescenter.edit',compact('salescenter'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Salescenter  $salescenter
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Salescenter $salescenter)
{
	if(Auth::Check()){
if(!empty($request->file('image')))
{
$image=$request->file('image');
$destinationPath = 'images/salescenters/';
$filename1 = $image->getClientOriginalName();
$image->move($destinationPath, $filename1);
//echo $filename1;exit;
}else
{
$filename1=$salescenter->image;
}
$this->validate($request,[
'name' => 'required',
'address'=>'required',
'telephone'=>'required',
'cell'=>'required',]);
$salescenter=salescenter::find($salescenter->id);
$salescenter->name = request('name');
$salescenter->address = request('address');
$salescenter->telephone = request('telephone');
$salescenter->cell = request('cell');
$salescenter->image = $filename1;
$updated=$salescenter->save();
if($updated)
{
return redirect('salescenter/')->with('message','Salescenter updated Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Remove the specified resource from storage.
*
* @param  \App\Salescenter  $salescenter
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
	if(Auth::Check()){
$salescenter=salescenter::findOrFail($id);
$salescenter->delete();
if($salescenter)
{
return redirect('salescenter/')->with('message','Salescenter Deleted Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
}