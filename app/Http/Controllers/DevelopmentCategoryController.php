<?php
namespace App\Http\Controllers;
use App\Development_category;
use Illuminate\Http\Request;
use Auth;
class DevelopmentCategoryController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
	if(Auth::Check())
	{
$category=Development_category::all();
return view('development_categories.index',compact('category'));
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
	if(AUth::Check())
	{
return view('development_categories.create');
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
$this->validate($request,[
'name' => 'required',
]);
$development_categories=new Development_category();
$development_categories->name = request('name');
$development_categories->save();
if($development_categories)
{
return redirect('development_categories')->with('message','Category Addedd Successfully');
}
}
/**
* Display the specified resource.
*
* @param  \App\Development_category  $development_category
* @return \Illuminate\Http\Response
*/
public function show(Development_category $development_category)
{
//
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Development_category  $development_category
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
	if(Auth::Check())
	{

$development_category=Development_category::find($id);
return view('development_categories.edit',compact('development_category'));
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Development_category  $development_category
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Development_category $development_category)
{
	if(Auth::Check())
	{
$this->validate($request,[
'name' => 'required',]);
$development_category=Development_category::find($development_category->id);
$development_category->name = request('name');
$updated=$development_category->save();
if($updated)
{
return redirect('development_categories')->with('message','Category updated Successfully');
}
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Remove the specified resource from storage.
*
* @param  \App\Development_category  $development_category
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
$development_category=Development_category::findOrFail($id);
$development_category->delete();
if($development_category)
{
return redirect('development_categories')->with('message','Category Deleted Successfully');
}
}
}