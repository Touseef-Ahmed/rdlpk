<?php
namespace App\Http\Controllers;
use App\Project;
use Auth;
use App\Development_category;
use App\Development_progress;
use Illuminate\Http\Request;
class DevelopmentProgressController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
/**Start: Code for Search***/
   public function search(Request $request)
     {
       if ($request->isMethod('get')){
          $rules = [
           'project_id' => 'required'];
        $this->validate($request, $rules);
        $category_id = $request->input('category_id');
        $project_id = $request->input('project_id');
         $progress = Development_progress::where([ 
        ['category_id', 'LIKE', '%' . $category_id . '%'],
         ['project_id', 'LIKE', '%' . $project_id . '%'],
    ])->get();
         $projects=project::all();
      $category=Development_category::all();
        return view('development_progress.index', compact('progress','projects','category'))->with('success','Searched Successfully');

    }else{
     return redirect('/file')->with('error','Error!!');
     }
     }

    /***END: Code For Search***/

public function index()
{

    if(Auth::Check())
    {
$progress=Development_progress::all();
$category=development_category::all();
$projects=project::all();
return view('development_progress.index',compact('progress','projects','category'));

}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
    if(Auth::Check())
    {
$projects=project::all();
$category=development_category::all();
return view('development_progress.create',compact('projects','category'));

}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
$this->validate($request,[
'project_id' => 'required',
'category_id'=>'required',
'link1'=>'required',
'image.*' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
]);

if($request->hasFile('image'))
{
$names = [];
foreach($request->file('image') as $image)
{
$destinationPath = 'images/development_progress/';
$filename = $image->getClientOriginalName();
$image->move($destinationPath, $filename);
array_push($names, $filename);
}
$request->image = json_encode($names);
}
$development_progress=new Development_progress();
$development_progress->link1 = request('link1');
$development_progress->link2 = request('link2');
$development_progress->project_id = request('project_id');
$development_progress->category_id = request('category_id');
//add request
$development_progress->image1=$names[0];
if(empty($names[1])){
$development_progress->image2 = request('noimg'); }else{$development_progress->image2=$names[1];}

if(empty($names[2])){
$development_progress->image3 = request('noimg'); }else{$development_progress->image3=$names[2];}
if(empty($names[3])){
$development_progress->image4 = request('noimg'); }else{$development_progress->image4=$names[3];}
if(empty($names[4])){
$development_progress->image5 = request('noimg'); }else{$development_progress->image5=$names[4];}
if(empty($names[5])){
$development_progress->image6 = request('noimg'); }else{$development_progress->image6=$names[5];}

$development_progress->save();

if($development_progress)
{
return redirect('development_progress')->with('message','Development Progress Addedd Successfully');
}


}
/**
* Display the specified resource.
*
* @param  \App\Development_progress  $development_progress
* @return \Illuminate\Http\Response
*/
public function show($id)
{
    if(Auth::Check()){
$progress=development_progress::find($id);
$projects=Project::all();
return view('development_progress.show',compact('progress','projects'));
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Development_progress  $development_progress
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
    if(Auth::Check()){
$projects=project::all();
$category=development_category::all();
$development_progress=development_progress::find($id);
return view('development_progress.edit',compact('projects','development_progress','category'));
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Development_progress  $development_progress
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Development_progress $development_progress)
{
if(!empty($request->file('image1')))
{  
$image1=$request->file('image1');
$destinationPath = 'images/development_progress/';
$filename1 = $image1->getClientOriginalName();
$image1->move($destinationPath, $filename1);
 ///echo $filename;exit;
}else
{
    $filename1=$development_progress->image1;
} 
if(!empty($request->file('image2')))
{  
$image2=$request->file('image2');
$destinationPath = 'images/development_progress/';
$filename2 = $image2->getClientOriginalName();
$image2->move($destinationPath, $filename2);
}else
{
    $filename2=$development_progress->image2;
}       
if(!empty($request->file('image3')))
{  
$image3=$request->file('image3');
$destinationPath = 'images/development_progress/';
$filename3 = $image3->getClientOriginalName();
$image3->move($destinationPath, $filename3);
}else
{
    $filename3=$development_progress->image3;
}
if(!empty($request->file('image4')))
{  
$image4=$request->file('image4');
$destinationPath = 'images/development_progress/';
$filename4 = $image4->getClientOriginalName();
$image4->move($destinationPath, $filename4);
}else
{
    $filename4=$development_progress->image4;
}
if(!empty($request->file('image5')))
{  
$image5=$request->file('image5');
$destinationPath = 'images/development_progress/';
$filename5 = $image5->getClientOriginalName();
$image5->move($destinationPath, $filename5);
}else
{
    $filename5=$development_progress->image5;
}
if(!empty($request->file('image6')))
{  
$image6=$request->file('image6');
$destinationPath = 'images/development_progress/';
$filename6 = $image6->getClientOriginalName();
$image6->move($destinationPath, $filename6);
}else
{
    $filename6=$development_progress->image6;
}

$this->validate($request,[
'category_id' => 'required',
'project_id' => 'required',
'link1'=>'required',
'link2'=>'required',]);
$development_progress=development_progress::find($development_progress->id);
$development_progress->category_id=$request->category_id;
$development_progress->project_id=$request->project_id;
$development_progress->link1=$request->link1;
$development_progress->link2=$request->link2;
$development_progress->image1=$filename1;
$development_progress->image2=$filename2;
$development_progress->image3=$filename3;
$development_progress->image4=$filename4;
$development_progress->image5=$filename5;
$development_progress->image6=$filename6;

$updated=$development_progress->save();
if($updated)
{
return redirect('/development_progress')->with('message','Development Progress Successfully Updated');
}





}
/**
* Remove the specified resource from storage.
*
* @param  \App\Development_progress  $development_progress
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
$development_progress=development_progress::findOrFail($id);
$development_progress->delete();
if($development_progress)
{
return redirect('development_progress')->with('message','Development Progress Deleted Successfully');
}
}
}