<?php

namespace App\Http\Controllers;

use App\Broucher;
use Illuminate\Http\Request;

class BroucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       if(Auth::Check()){
$brouchers=brouchers::all();
$projects=Project::all();
return view('newsletters.index',compact('brouchers','projects'));
}else{
Auth::logout();
return redirect('/login');
}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Broucher  $broucher
     * @return \Illuminate\Http\Response
     */
    public function show(Broucher $broucher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Broucher  $broucher
     * @return \Illuminate\Http\Response
     */
    public function edit(Broucher $broucher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Broucher  $broucher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Broucher $broucher)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Broucher  $broucher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Broucher $broucher)
    {
        //
    }
}
