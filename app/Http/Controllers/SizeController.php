<?php
namespace App\Http\Controllers;
use App\Size;
use Auth;
use Illuminate\Http\Request;
class SizeController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
	if(Auth::Check())
	{
$size=Size::all();
return view('size.index',compact('size'));
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
	if(Auth::Check()){
return view('size.create');
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
	if(Auth::Check()){
$this->validate($request,[
'name' => 'required|unique:sizes',
]);
$size=new size();
$size->name = request('name');
$size->save();
if($size)
{
return redirect('size')->with('message','Size Addedd Successfully');
}
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Display the specified resource.
*
* @param  \App\Size  $size
* @return \Illuminate\Http\Response
*/
public function show(Size $size)
{
//
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Size  $size
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
	if(Auth::Check()){
$size=Size::find($id);
return view('size.edit',compact('size'));
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Size  $size
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Size $size)
{
	if(Auth::Check()){
$this->validate($request,[
'name' => 'required|unique:sizes',]);
$size=size::find($size->id);
$size->name = request('name');
$updated=$size->save();
if($updated)
{
return redirect('size')->with('message','Size updated Successfully');
}
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Remove the specified resource from storage.
*
* @param  \App\Size  $size
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
	if(Auth::Check()){
$size=Size::findOrFail($id);
$size->delete();
if($size)
{
return redirect('size')->with('message','Size Deleted Successfully');
}
}else{
            Auth::logout();
           return redirect('/login');
        }
}
}