<?php
namespace App\Http\Controllers;
use Auth;
use App\Kfcategory;
use Illuminate\Http\Request;
class KfcategoryController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{ if(Auth::Check()){
$category=Kfcategory::all();
return view('kfcategory.index',compact('category'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
 {  echo 123;exit;
        $Kfcategory = new Kfcategory();
        $Kfcategory->name = $request->name;
       /* $grocery->type = $request->type;
        $grocery->price = $request->price;*/

        $grocery->save();
        return response()->json(['success'=>'Data is successfully added']);
 }

public function create()
{ 
if(Auth::Check()){


return view('kfcategory.create');
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store1(Request $request)
{
$this->validate($request,[
'name' => 'required|unique:kfcategories',
]);
$kfcategory=new Kfcategory();
$kfcategory->name = request('name');
$kfcategory->save();
if($kfcategory)
{
return redirect('kfcategory')->with('message','Category Addedd Successfully');
}
}
/**
* Display the specified resource.
*
* @param  \App\Kfcategory  $kfcategory
* @return \Illuminate\Http\Response
*/
public function show(Kfcategory $kfcategory)
{
//
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Kfcategory  $kfcategory
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
if(Auth::Check()){
$kfcategory=Kfcategory::find($id);
return view('kfcategory.edit',compact('kfcategory'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Kfcategory  $kfcategory
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Kfcategory $kfcategory)
{
$this->validate($request,[
'name' => 'required',]);
$kfcategory=kfcategory::find($kfcategory->id);
$kfcategory->name = request('name');
$updated=$kfcategory->save();
if($updated)
{
return redirect('kfcategory')->with('message','Category updated Successfully');
}
}
/**
* Remove the specified resource from storage.
*
* @param  \App\Kfcategory  $kfcategory
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
$kfcategory=kfcategory::findOrFail($id);
$kfcategory->delete();
if($kfcategory)
{
return redirect('kfcategory')->with('message','Category Deleted Successfully');
}
}
}