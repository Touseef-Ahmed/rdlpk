<?php
namespace App\Http\Controllers;
use App\file;
use App\Project;
use App\Homes_category;
use App\size;
use Auth;
use Illuminate\Http\Request;
class FileController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
/**Start: Code for Search***/
   public function search(Request $request)
     {
       if ($request->isMethod('get')){
          $rules = [
           'project_id' => 'required'];
        $this->validate($request, $rules);
        $category_id = $request->input('category_id');
        $project_id = $request->input('project_id');
         $file = file::where([ 
        ['category_id', 'LIKE', '%' . $category_id . '%'],
         ['project_id', 'LIKE', '%' . $project_id . '%'],
    ])->get();
         $projects=project::all();
       $category=Homes_category::all();
        return view('file.index', compact('file','category','projects'))->with('success','Searched Successfully');

    }else{
     return redirect('/file')->with('error','Error!!');
     }
     }

    /***END: Code For Search***/
public function index()
{
    if(Auth::Check())
    {
$file=file::all();
$projects=project::all();
$category=Homes_category::all();
$size=size::all();
return view('file.index',compact('file','projects','size','category'));
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
    if(Auth::Check()){
$projects=project::all();
$category=Homes_category::all();
$size=size::all();
return view('file.create',compact('file','projects','size','category'));
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
$this->validate($request,[
'project_id' => 'required',
'category_id'=>'required',
'size_id'=>'required',
'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:12048',
]);
if($request->hasFile('image') && $request->image->isValid())
        {
        $extension=$request->image->extension();
        $filename=time()."_.".$extension;
         $request->image->move('images/files/',$filename);
        } else
        {
            $filename="no-image.png";
        }
$file=new file();
$file->category_id = request('category_id');
$file->project_id = request('project_id');
$file->size_id = request('size_id');
$file->image = $filename;
$file->save();

if($file)
{
return redirect('file')->with('message','File Addedd Successfully');
}



}
/**
* Display the specified resource.
*
* @param  \App\file  $file
* @return \Illuminate\Http\Response
*/
public function show(file $file)
{
//
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\file  $file
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
    if(Auth::Check()){
$projects=project::all();
$file=file::find($id);
$category=Homes_category::all();
$size=size::all();
return view('file.edit',compact('file','projects','size','category'));
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\file  $file
* @return \Illuminate\Http\Response
*/
public function update(Request $request, file $file)
{
    if(!empty($request->file('image')))
{  
$image=$request->file('image');
$destinationPath = 'images/files/';
$filename = $image->getClientOriginalName();
$image->move($destinationPath, $filename);
//echo $filename1;exit;
}else
{
   $filename=$file->image;
} 
$this->validate($request,[
'project_name' => 'required|unique:projects',
'category_id'=>'required',
]);
$file=file::find($file->id);
$file->project_id=$request->project_id;
$file->category_id=$request->category_id;
$file->size_id=$request->size_id;
$file->image=$filename;

$updated=$file->save();
if($updated)
{
return redirect('/file')->with('message','File Successfully Updated');
}



}
/**
* Remove the specified resource from storage.
*
* @param  \App\file  $file
* @return \Illuminate\Http\Response
*/
public function destroy(file $file)
{
//
}
}