<?php
namespace App\Http\Controllers;
use App\Homes_category;
use Illuminate\Http\Request;
use Auth;
class HomesCategoryController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
	if(Auth::Check()){
$homes_category=homes_category::all();
return view('homes_category.index',compact('homes_category'));
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
	if(Auth::Check()){
return view('homes_category.create');
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
$this->validate($request,[
'name' => 'required',
]);
$homes_category=new homes_category();
$homes_category->name = request('name');
$homes_category->save();
if($homes_category)
{
return redirect('homes_category')->with('message','Homes Category Addedd Successfully');
}
}
/**
* Display the specified resource.
*
* @param  \App\Homes_category  $homes_category
* @return \Illuminate\Http\Response
*/
public function show(Homes_category $homes_category)
{
//
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Homes_category  $homes_category
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
	if(Auth::check()){
$homes_category=homes_category::find($id);
return view('homes_category.edit',compact('homes_category'));
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Homes_category  $homes_category
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Homes_category $homes_category)
{
$this->validate($request,[
'name' => 'required',]);
$homes_category=homes_category::find($homes_category->id);
$homes_category->name = request('name');
$updated=$homes_category->save();
if($updated)
{
return redirect('homes_category')->with('message','Category updated Successfully');
}
}
/**
* Remove the specified resource from storage.
*
* @param  \App\Homes_category  $homes_category
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
$homes_category=homes_category::findOrFail($id);
$homes_category->delete();
if($homes_category)
{
return redirect('homes_category')->with('message','Category Deleted Successfully');
}
}
}