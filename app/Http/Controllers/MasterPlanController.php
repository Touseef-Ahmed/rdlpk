<?php
namespace App\Http\Controllers;
use App\Master_plan;
use App\Project;
use Auth;
use Illuminate\Http\Request;
class MasterPlanController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
	if(Auth::Check()){
$master_plan=master_plan::all();
$projects=Project::all();
return view('master_plan.index',compact('master_plan'));
 }else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
	if(Auth::Check()){
$projects=project::all();
return view('master_plan.create',compact('projects'));
 }else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
$this->validate($request,[
'title' => 'required',
'project_id'=>'required',
'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:8048',
]);
if($request->hasFile('image') && $request->image->isValid())
{
$extension=$request->image->extension();
$filename=time()."_.".$extension;
echo $request->image->move('images/master_plan/',$filename);
} else
{
$filename="no-image.png";
}
$master_plan=new master_plan();
$master_plan->title = request('title');
$master_plan->project_id = request('project_id');
$master_plan->image = $filename;
$master_plan->save();
if($master_plan)
{
return redirect('master_plan')->with('message','Master Plan Addedd Successfully');
}


}
/**
* Display the specified resource.
*
* @param  \App\Master_plan  $master_plan
* @return \Illuminate\Http\Response
*/
public function show(Master_plan $master_plan)
{
//
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Master_plan  $master_plan
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
	if(AUth::Check()){
$projects=project::all();
$master_plan=master_plan::find($id);
return view('master_plan.edit',compact('projects','master_plan'));
 }else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Master_plan  $master_plan
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Master_plan $master_plan)
{
	if(Auth::Check()){
if(!empty($request->file('image')))
{  
$image=$request->file('image');
$destinationPath = 'images/master_plan/';
$filename1 = $image->getClientOriginalName();
$image->move($destinationPath, $filename1);
//echo $filename1;exit;
}else
{
   $filename1=$master_plan->image;
} 
$this->validate($request,[
'title' => 'required',
'project_id' => 'required',]);
$master_plan=Master_plan::find($master_plan->id);
$master_plan->title = request('title');
$master_plan->project_id = request('project_id');
$master_plan->image = $filename1;
$master_plan=$master_plan->save();
if($master_plan)
{
return redirect('master_plan')->with('message','Master Plan updated Successfully');
}
 }else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Remove the specified resource from storage.
*
* @param  \App\Master_plan  $master_plan
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
	if(Auth::Check()){
$master_plan=master_plan::findOrFail($id);
$master_plan->delete();
if($master_plan)
{
return redirect('master_plan')->with('message','Master Plan Deleted Successfully');
}
 }else{
            Auth::logout();
           return redirect('/login');
        }
}
}