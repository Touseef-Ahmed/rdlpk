<?php

namespace App\Http\Controllers;
use Auth;
use App\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**Start: Code for Search***/
   public function search(Request $request)
     {
       if ($request->isMethod('get')){
          $rules = [
           'project_name' => 'required'
       ];
        $this->validate($request, $rules);
        $search = $request->input('project_name');
        $projects = Project::where('project_name', 'LIKE', '%'.$search.'%')->paginate(4);
        return view('projects.index', compact('projects'))->with('success','Searched Successfully');

    }else{
     return redirect('/projects')->with('error','Error!!');
     }
     }


    /***END: Code For Search***/

    public function index()
    { 
        if(Auth::check())
        {
       $projects=project::all();
            return view('projects.index',compact('projects'));
        }else{
            Auth::logout();
           return redirect('/login');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         if(Auth::check())
        {
         $projects=project::all();
        return view('projects.create',compact('projects'));
        }else{
            Auth::logout();
           return redirect('/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { if(Auth::Check()){
      $this->validate($request,[
        'project_name' => 'required|unique:projects',
        'project_code'=>'required',
        'project_status'=>'required',
        'project_detail'=>'required',
        'project_location'=>'required',
        'project_attraction'=>'required',
        'project_type'=>'required',
        'imagemain' => 'required|image|mimes:jpeg,png,jpg|max:10048',

    ]);
       if($request->hasFile('imagemain') && $request->imagemain->isValid())
        {
        $extension=$request->imagemain->extension();
        $imagemain=time()."_.".$extension;
         $request->imagemain->move('images/projects/',$imagemain);
        } else
        {
            $imagemain="no-image.png";
        } // exit;
        if($request->hasFile('imagetop') && $request->imagetop->isValid())
        {
        $extension=$request->imagetop->extension();
        $imagetop=time()."_.".$extension;
        $request->imagetop->move('images/projects/',$imagetop);
        } else
        {
            $imagetop="no-image.png";
        }  
        if($request->hasFile('imagebottom') && $request->imagebottom->isValid())
        {
        $extension=$request->imagebottom->extension();
        $imagebottom=time()."_.".$extension;
        $request->imagebottom->move('images/projects/',$imagebottom);
        } else
        {
            $imagebottom="no-image.png";
        }  
             
        $created=Project::create
      ([
        'project_name'=>$request->project_name,
        'project_code'=>$request->project_code,
        'project_status'=>$request->project_status,
        'project_detail'=>$request->project_detail,
        'project_location'=>$request->project_location,
         'project_attraction'=>$request->project_attraction,
          'project_type'=>$request->project_type,
          'imagemain'=>$imagemain,
          'imagetop'=>$imagetop,
          'imagebottom'=>$imagebottom,

      ]);
      if($created)
      {
        return redirect('projects')->with('message','Project Addedd Successfully');
      }
       }else{
            Auth::logout();
           return redirect('/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(Auth::check())
      {
       $project=project::find($id);

         return view('projects.edit',compact('project','projects'));
         }else{
            Auth::logout();
           return redirect('/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {   $project=project::find($request->id);
        if(Auth::Check()){
 if(!empty($request->file('imagemain')))
{           
$imagemain=$request->file('imagemain');
$destinationPath = 'images/projects/';
$filename1 = $imagemain->getClientOriginalName();
$imagemain->move($destinationPath, $filename1);
//echo $filename1;exit;
}else
{
$filename1=$project->imagemain;
}
if(!empty($request->file('imagetop')))
{  
$imagetop=$request->file('imagetop');
$destinationPath = 'images/projects/';
$filename2 = $imagetop->getClientOriginalName();
$imagetop->move($destinationPath, $filename2);
//echo $filename1;exit;
}else
{
$filename2=$project->imagetop;
}
if(!empty($request->file('imagebottom')))
{
$imagebottom=$request->file('imagebottom');
$destinationPath = 'images/projects/';
$filename3 = $imagebottom->getClientOriginalName();
$imagebottom->move($destinationPath, $filename3);
//echo $filename1;exit;
}else
{
$filename3=$project->imagebottom;
}
       $this->validate($request,[
        'project_code'=>'required',
        'project_status'=>'required',
        'project_detail'=>'required',
        'project_location'=>'required',
        'project_attraction'=>'required',
        'project_type'=>'required',
    ]);
      
        $project->project_code=$request->project_code;
        $project->project_status=$request->project_status;
        $project->project_detail=$request->project_detail;
        $project->project_location=$request->project_location;
        $project->project_attraction=$request->project_attraction;
        $project->project_type=$request->project_type;
        $project->imagemain = $filename1;
        $project->imagetop = $filename2;
        $project->imagebottom = $filename3;
       
     
      
        $project->save();
        if($project)
        {
            return redirect('projects')->with('message','Project Updated Successfully');

        }else
        {
            return redirect('projects')->with('message','Project Not Updated');
        }
         }else{
            Auth::logout();
           return redirect('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        if(Auth::Check()){
        $project=Project::findOrFail($id);
        $project->delete();
        if($project)
        {
            return redirect('projects')->with('message','Project Deleted Successfully');

        }
         }else{
            Auth::logout();
           return redirect('/login');
        }
    }
}
