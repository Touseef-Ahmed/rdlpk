<?php
namespace App\Http\Controllers;
use App\Project_location;
use Illuminate\Http\Request;
use App\Project;
use Auth;
class ProjectLocationController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
/**Start: Code for Search***/
public function search(Request $request)
{
if ($request->isMethod('get')){
$rules = [
'title' => 'required', 'project_id' => 'required'];
$this->validate($request, $rules);
$title = $request->input('title');
$project_id = $request->input('project_id');
$project_location = Project_location::where([
['title', 'LIKE', '%' . $title . '%'],
['project_id', 'LIKE', '%' . $project_id . '%'],
])->get();
$projects=project::all();
return view('project_location.index', compact('project_location','projects'))->with('success','Searched Successfully');
}else{
return redirect('/project_location')->with('error','Error!!');
}
}
/***END: Code For Search***/
public function index()
{ if(Auth::Check()){
$project_location=Project_location::all();
$projects=Project::all();
return view('project_location.index',compact('project_location','projects'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
if(Auth::Check()){
$projects=project::all();
return view('project_location.create',compact('projects'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
if(Auth::Check()){
$this->validate($request,[
'title' => 'required',
'project_id'=>'required',
'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:8048',
]);
if($request->hasFile('image') && $request->image->isValid())
{
$extension=$request->image->extension();
$filename=time()."_.".$extension;
echo $request->image->move('images/project_location/',$filename);
} else
{
$filename="no-image.png";
}
$project_location=new project_location();
$project_location->title = request('title');
$project_location->project_id = request('project_id');
$project_location->image = $filename;
$project_location->save();
if($project_location)
{
return redirect('project_location')->with('message','Project Location Addedd Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Display the specified resource.
*
* @param  \App\Project_location  $project_location
* @return \Illuminate\Http\Response
*/
public function show(Project_location $project_location)
{
//
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Project_location  $project_location
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
if(Auth::Check()){
$projects=project::all();
$project_location=project_location::find($id);
return view('project_location.edit',compact('projects','project_location'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Project_location  $project_location
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Project_location $project_location)
{
if(Auth::Check()){
if(!empty($request->file('image')))
{
$image=$request->file('image');
$destinationPath = 'images/project_location/';
$filename1 = $image->getClientOriginalName();
$image->move($destinationPath, $filename1);
//echo $filename1;exit;
}else
{
$filename1=$project_location->image;
}
$this->validate($request,[
'title' => 'required',
'project_id' => 'required',
]);
$project_location=project_location::find($project_location->id);
$project_location->title = request('title');
$project_location->project_id = request('project_id');
$project_location->image = $filename1;
$updated=$project_location->save();
if($updated)
{
return redirect('project_location')->with('message','Project Location updated Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Remove the specified resource from storage.
*
* @param  \App\Project_location  $project_location
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
if(Auth::Check()){
$project_location=project_location::findOrFail($id);
$project_location->delete();
if($project_location)
{
return redirect('project_location')->with('message','Project Location Deleted Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
}