<?php
namespace App\Http\Controllers;
use App\Project;
use App\Newsletter;
use Auth;
use Illuminate\Http\Request;
class NewsletterController extends Controller
{
/**Start: Code for Search***/
public function search(Request $request)
{
if ($request->isMethod('get')){
$rules = [
'title' => 'required', 'project_id' => 'required'];
$this->validate($request, $rules);
$title = $request->input('title');
$project_id = $request->input('project_id');
$newsletters = Newsletter::where([
['title', 'LIKE', '%' . $title . '%'],
['project_id', 'LIKE', '%' . $project_id . '%'],
])->get();
$projects=project::all();
return view('newsletters.index', compact('newsletters','projects'))->with('success','Searched Successfully');
}else{
return redirect('/project_location')->with('error','Error!!');
}
}
/***END: Code For Search***/
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
if(Auth::Check()){
$newsletters=newsletter::all();
$projects=Project::all();
return view('newsletters.index',compact('newsletters','projects'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
if(Auth::Check()){
$projects=project::all();
return view('newsletters.create',compact('projects'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
if(Auth::Check()){
$oldval=Newsletter::orderBy('sort_order', 'desc')->first();
$old=$oldval['sort_order'];
if($old==0)
{
$old=1;
}
else
{
////echo 'outside 0';
$old=$old+1;
}
///$old=Newsletter ::orderBy('sort_order', 'desc')->first()->sort_order;
$this->validate($request,[
'title' => 'required',
'project_id'=>'required',
'link'=>'required',
'status'=>'required',
'image' => 'required|mimes:pdf',
'thumbnail' => 'required|image|mimes:jpeg,png,jpg,gif|max:8048',
]);
if($request->hasFile('image') && $request->image->isValid())
{
$extension=$request->image->extension();
$filename=time()."_.".$extension;
echo $request->image->move('images/newsletters/',$filename);
} else
{
$filename="no-image.png";
}
if($request->hasFile('thumbnail') && $request->thumbnail->isValid())
{
$extension=$request->thumbnail->extension();
$thumbnail=time()."_.".$extension;
$request->thumbnail->move('images/newsletters/',$thumbnail);
} else
{
$thumbnail="no-image.png";
}
///print_r($names);exit;
$newsletters=new newsletter();
$newsletters->title = request('title');
$newsletters->project_id = request('project_id');
$newsletters->link = request('link');
$newsletters->status = request('status');
$newsletters->file = $filename;
$newsletters->thumbnail = $thumbnail;
$newsletters->sort_order = $old+1;
$newsletters->save();
if($newsletters)
{
return redirect('newsletters')->with('message','Newsletter Addedd Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Display the specified resource.
*
* @param  \App\Newsletter  $newsletter
* @return \Illuminate\Http\Response
*/
public function show(Newsletter $newsletter)
{
//
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Newsletter  $newsletter
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
if(Auth::Check()){
$projects=project::all();
$newsletters=newsletter::find($id);
return view('newsletters.edit',compact('projects','newsletters'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Newsletter  $newsletter
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Newsletter $newsletter)
{
if(Auth::Check()){
if(!empty($request->file('image')))
{
$image=$request->file('image');
$destinationPath = 'images/newsletters/';
$filename1 = $image->getClientOriginalName();
$image->move($destinationPath, $filename1);
//echo $filename1;exit;
}else
{
$filename1=$newsletter->file;
}
if(!empty($request->file('thumbnail')))
{
$thumbnail=$request->file('thumbnail');
$destinationPath = 'images/newsletters/';
$filename2 = $thumbnail->getClientOriginalName();
$thumbnail->move($destinationPath, $filename2);
}else
{
$filename2=$newsletter->thumbnail;
}
///echo $news->image2=$filename2;exit;
$this->validate($request,[
'title' => 'required',
'project_id' => 'required',
'link'=>'required',
'status'=>'required',]);
///echo $news->id;exit;
///$newsletters=new newsletter();
$newsletters=Newsletter::find($newsletter->id);
$newsletter->title = request('title');
$newsletter->project_id = request('project_id');
$newsletter->link = request('link');
$newsletter->status = request('status');
$newsletter->file = $filename1;
$newsletter->thumbnail = $filename2;
$newsletter->sort_order = $newsletter->sort_order;
$updated=$newsletter->save();
if($updated)
{
return redirect('newsletters')->with('message','News updated Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Remove the specified resource from storage.
*
* @param  \App\Newsletter  $newsletter
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
if(Auth::Check()){
$newsletter=Newsletter::findOrFail($id);
$newsletter->delete();
if($newsletter)
{
return redirect('newsletters')->with('message','Newsletter Deleted Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
}