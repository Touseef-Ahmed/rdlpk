<?php

namespace App\Http\Controllers;

use App\page;
use Auth;
use App\Project;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::Check())
    {
$pages=page::all();
$projects=project::all();
return view('pages.index',compact('pages','projects'));
}else{
            Auth::logout();
           return redirect('/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       if(Auth::Check()){
$projects=project::all()->where('project_status','%LIKE%','Current');

return view('pages.create',compact('projects'));
}else{
            Auth::logout();
           return redirect('/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
     'project_id' => 'required',
'name'=>'required|unique:pages',
]);

$page=new page();
$page->project_id = request('project_id');
$page->name = request('name');
$page->save();

if($page)
{
return redirect('pages')->with('message','Page Addedd Successfully');
}
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(page $page)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, page $page)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(page $page)
    {
        //
    }
}
