<?php
namespace App\Http\Controllers;
use App\Project;
use App\Payment_plan;
use Auth;
use Illuminate\Http\Request;
class PaymentPlanController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
/**Start: Code for Search***/
public function search(Request $request)
{
if ($request->isMethod('get')){
$rules = [
'project_id' => 'required'];
$this->validate($request, $rules);
$type = $request->input('type');
$project_id = $request->input('project_id');
$payment_plan = Payment_plan::where([
['type', 'LIKE', '%' . $type . '%'],
['project_id', 'LIKE', '%' . $project_id . '%'],
])->get();
$projects=project::all();

return view('payment_plan.index', compact('payment_plan','projects'))->with('success','Searched Successfully');
}else{
return redirect('/file')->with('error','Error!!');
}
}
/***END: Code For Search***/
public function index()
{
if(Auth::Check()){
$payment_plan=payment_plan::all();
$projects=Project::all();
return view('payment_plan.index',compact('payment_plan','projects'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
if(Auth::Check()){
$projects=Project::all();
return view('payment_plan.create',compact('projects'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
if(Auth::Check()){
if($request->hasFile('image_monthly') && $request->image_monthly->isValid())
{
$extension=$request->image_monthly->extension();
$image_monthly=time()."_.".$extension;
$request->image_monthly->move('images/payment_plan/',$image_monthly);
} else
{
$image_monthly="no-image.png";
}
if($request->hasFile('image_quarterly') && $request->image_quarterly->isValid())
{
$extension=$request->image_quarterly->extension();
$image_quarterly=time().time()."_.".$extension;
$request->image_quarterly->move('images/payment_plan/',$image_quarterly);
} else
{
$image_quarterly="no-image.png";
}
$this->validate($request,[
'type' => 'required',
'project_id'=>'required',
'duration'=>'required',
'duration_type'=>'required',
'booking'=>'required',
]);
$payment_plan = new payment_plan();
$payment_plan->type = request('type');
$payment_plan->project_id = request('project_id');
$payment_plan->duration = request('duration');
$payment_plan->duration_type = request('duration_type');
$payment_plan->booking = request('booking');
$payment_plan->image_monthly = $image_monthly;
$payment_plan->image_quarterly = $image_quarterly;
$payment_plan->save();
if($payment_plan)
{
return redirect('payment_plan')->with('message','Payment Plan Addedd Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Display the specified resource.
*
* @param  \App\Payment_plan  $payment_plan
* @return \Illuminate\Http\Response
*/
public function show(Payment_plan $payment_plan)
{
//
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Payment_plan  $payment_plan
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
if(Auth::Check()){
$projects=project::all();
$payment_plan=payment_plan::find($id);
return view('payment_plan.edit',compact('projects','payment_plan'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Payment_plan  $payment_plan
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Payment_plan $payment_plan)
{
if(Auth::Check()){
if(!empty($request->file('image_monthly')))
{
$image_monthly=$request->file('image_monthly');
$destinationPath = 'images/payment_plan/';
$filename1 = $image_monthly->getClientOriginalName();
$image_monthly->move($destinationPath, $filename1);
//echo $filename1;exit;
}else
{
$filename1=$payment_plan->image_monthly;
}
if(!empty($request->file('image_quarterly')))
{
$image_quarterly=$request->file('image_quarterly');
$destinationPath = 'images/payment_plan/';
$filename2 = $image_quarterly->getClientOriginalName();
$image_quarterly->move($destinationPath, $filename2);
}else
{
$filename2=$payment_plan->image_quarterly;
}
$this->validate($request,[
'type' => 'required',
'project_id'=>'required',
'duration'=>'required',
'duration_type'=>'required',
'booking'=>'required',
]);
///echo $news->id;exit;
$payment_plan=payment_plan::find($payment_plan->id);
$payment_plan->type = request('type');
$payment_plan->project_id = request('project_id');
$payment_plan->duration = request('duration');
$payment_plan->duration_type = request('duration_type');
$payment_plan->booking = request('booking');
$payment_plan->image_monthly = $filename1;
$payment_plan->image_quarterly = $filename2;
$updated=$payment_plan->save();
if($updated)
{
return redirect('payment_plan')->with('message','Payment Plan Updated Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Remove the specified resource from storage.
*
* @param  \App\Payment_plan  $payment_plan
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
if(Auth::Check()){
$payment_plan=payment_plan::findOrFail($id);
$payment_plan->delete();
if($payment_plan)
{
return redirect('payment_plan')->with('message','Payment Plan Deleted Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
}