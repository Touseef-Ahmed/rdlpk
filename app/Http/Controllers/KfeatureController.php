<?php
namespace App\Http\Controllers;
use App\kfcategory;
use App\Kfeature;
use Auth;
use Illuminate\Http\Request;
class KfeatureController extends Controller
{

/**Start: Code for Search***/
public function search(Request $request)
{
if ($request->isMethod('get')){
$rules = [
'name' => 'required'];
$this->validate($request, $rules);
$name = $request->input('name');
$kfeature = Kfeature::where([
['name', 'LIKE', '%' . $name . '%'],
])->get();
$kfcategory=kfcategory::all();
return view('kfeature.index', compact('kfeature','kfcategory'))->with('success','Searched Successfully');
}else{
return redirect('/project_location')->with('error','Error!!');
}
}
/***END: Code For Search***/
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
if(Auth::Check()){
$kfeature=Kfeature::all();
$kfcategory=kfcategory::all();
return view('kfeature.index',compact('kfeature','kfcategory'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
if(Auth::Check()){
$kfcategory=kfcategory::all();
return view('kfeature.create',compact('kfcategory'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
$this->validate($request,[
'name' => 'required',
'kfcategory_id'=>'required',
]);

$kfeature = new Kfeature();
$kfeature->name = request('name');
$kfeature->kfcategory_id = request('kfcategory_id');

$kfeature->save();
if($kfeature)
{
return redirect('kfeature')->with('message','Feature Addedd Successfully');
}
}
/**
* Display the specified resource.
*
* @param  \App\Kfeature  $kfeature
* @return \Illuminate\Http\Response
*/
public function show(Kfeature $kfeature)
{
//
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Kfeature  $kfeature
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
if(AUth::Check()){
$kfcategory=kfcategory::all();
$kfeature=kfeature::find($id);
return view('kfeature.edit',compact('kfcategory','kfeature'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Kfeature  $kfeature
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Kfeature $kfeature)
{


$this->validate($request,[
'name' => 'required',
'kfcategory_id' => 'required',
]);
$kfeature=kfeature::find($kfeature->id);
$kfeature->name=$request->name;
$kfeature->kfcategory_id=$request->kfcategory_id;
$updated=$kfeature->save();
if($updated)
{
return redirect('/kfeature')->with('message','Feature Successfully Updated');
}
}
/**
* Remove the specified resource from storage.
*
* @param  \App\Kfeature  $kfeature
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
if(Auth::Check()){
$kfeature=kfeature::findOrFail($id);
$kfeature->delete();
if($kfeature)
{
return redirect('kfeature')->with('message','Feature Deleted Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}




}