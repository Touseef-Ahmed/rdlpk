<?php
namespace App\Http\Controllers;
use App\Kfgallery;
use Illuminate\Http\Request;
use App\Kfeature;
use App\kfcategory;
use Auth;
class KfgalleryController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{

if(Auth::Check()){
$kfeature=Kfeature::all();
$kfcategory=kfcategory::all();
$kfgallery=kfgallery::all();
return view('kfgallery.index',compact('kfeature','kfcategory','kfgallery'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
if(Auth::Check()){
$kfcategory=kfcategory::all();
$kfeature=Kfeature::all();
return view('kfgallery.create',compact('kfeature','kfcategory'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
$this->validate($request,[
'kfeature_id' => 'required',
'kfcategory_id'=>'required',
'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:8048',
]);
if($request->hasFile('image') && $request->image->isValid())
{
$extension=$request->image->extension();
$filename=time()."_.".$extension;
 $request->image->move('images/kfeature/',$filename);
} else
{
$filename="no-image.png";
}
$Kfgallery=new Kfgallery();
$Kfgallery->kfeature_id = request('kfeature_id');
$Kfgallery->kfcategory_id = request('kfcategory_id');
$Kfgallery->image = $filename;
$Kfgallery->save();
if($Kfgallery)
{
return redirect('kfgallery')->with('message','Image Addedd Successfully');
}



}
/**
* Display the specified resource.
*
* @param  \App\Kfgallery  $kfgallery
* @return \Illuminate\Http\Response
*/
public function show(Kfgallery $kfgallery)
{
//
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Kfgallery  $kfgallery
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
if(Auth::Check()){
$kfgallery=kfgallery::find($id);
$kfcategory=kfcategory::all();
$kfeature=kfeature::all();
return view('kfgallery.edit',compact('kfgallery','kfcategory','kfeature'));
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Kfgallery  $kfgallery
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Kfgallery $kfgallery)
{

    if(Auth::Check()){
if(!empty($request->file('image')))
{
$image=$request->file('image');
$destinationPath = 'images/kfeature/';
$filename1 = $image->getClientOriginalName();
$image->move($destinationPath, $filename1);
//echo $filename1;exit;
}else
{
$filename1=$kfgallery->image;
}
$this->validate($request,[
'kfeature_id' => 'required',
'kfcategory_id'=>'required',]);
$kfgallery=kfgallery::find($kfgallery->id);
$kfgallery->kfeature_id = request('kfeature_id');
$kfgallery->kfcategory_id = request('kfcategory_id');
$kfgallery->image = $filename1;

$updated=$kfgallery->save();
if($updated)
{
return redirect('kfgallery')->with('message','Image updated Successfully');
}
}else{
Auth::logout();
return redirect('/login');
}
}
/**
* Remove the specified resource from storage.
*
* @param  \App\Kfgallery  $kfgallery
* @return \Illuminate\Http\Response
*/
public function destroy(Kfgallery $kfgallery)
{
//
}
}