<?php
namespace App\Http\Controllers;
use App\Associates;
use Auth;
use Illuminate\Http\Request;
class AssociatesController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
	if(Auth::Check())
	{
$associates=Associates::all();
return view('associates.index',compact('associates'));
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
	if(AUth::Check())
	{
return view('associates.create');
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
$this->validate($request,[
'detail' => 'required',
'link'=>'required',
'image' => 'required|image|mimes:jpeg,png,jpg|max:10048',
]);
if($request->hasFile('image') && $request->image->isValid())
{
$extension=$request->image->extension();
$filename=time()."_.".$extension;
 $request->image->move('images/associates/',$filename);
} else
{
$filename="no-image.png";
}
$associates=new associates();
$associates->detail = request('detail');
$associates->link = request('link');
$associates->image = $filename;
$associates->save();
if($associates)
{
return redirect('associates')->with('message','Associates Added Successfully');
}
}
/**
* Display the specified resource.
*
* @param  \App\Associates  $associates
* @return \Illuminate\Http\Response
*/
public function show(Associates $associates)
{
//
}
/**
* Show the form for editing the specified resource.
*
* @param  \App\Associates  $associates
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
	if(AUth::Check())
	{
$associates=Associates::find($id);
return view('associates.edit',compact('associates'));
}else{
            Auth::logout();
           return redirect('/login');
        }
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Associates  $associates
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Associates $associates)
{ 
///echo $request->image;exit;
$this->validate($request,[
'detail' => 'required',
'link'=>'required',]);
if(!empty($request->file('image')))
{  
$image=$request->file('image');
$destinationPath = 'images/associates/';
$filename1 = $image->getClientOriginalName();
$image->move($destinationPath, $filename1);
//echo $filename1;exit;
}else
{
    $filename1=$associates->image;
} 
$associates=Associates::find($request->id);
///echo $request->id;exit;
//$slider=Slider::find($slider->id);

$associates->detail =request('detail');
$associates->link =request('link');
$associates->image = $filename1;
$updated=$associates->save();
if($updated)
{
return redirect('associates')->with('message','Associates updated Successfully');
}


}
/**
* Remove the specified resource from storage.
*
* @param  \App\Associates  $associates
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
$associates=associates::findOrFail($id);
$associates->delete();
if($associates)
{
return redirect('associates')->with('message','Associates Deleted Successfully');
}
}
}