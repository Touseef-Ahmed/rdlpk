<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
  protected $fillable=['project_name','project_code','project_status','project_detail','project_location','project_attraction','project_type','imagemain','imagetop','imagebottom',];
}
