<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sfeature extends Model
{
   protected $fillable=['title,project_id','detail','image1','image2','image3','image4','image5',];

 public function project()
	    {
	        return $this->belongsTo('\App\Project');

	    }
}
