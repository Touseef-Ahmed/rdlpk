<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable=['title,project_id','image1','image2','image3','image4','image5','image6','image7','image8','image9','image10',];

 public function project()
	    {
	        return $this->belongsTo('\App\Project');

	    }
}
