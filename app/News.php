<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
      protected $fillable=['sort_order,project_id','heading','location','detail','image1','image2','image3','image4','image5','image6','image7','image8','image9','image10',];

 public function project()
	    {
	        return $this->belongsTo('\App\Project');

	    }


  }

