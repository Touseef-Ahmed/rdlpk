<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Development_progress extends Model
{
     protected $fillable=['project_id','category_id','link1','link2','image1','image2','image3','image4','image5','image6',];

 public function project()
	    {
	        return $this->belongsTo('\App\Project');

	    }
	     public function category()
	    {
	        return $this->belongsTo('App\development_category');

	    }
}
