<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Development_category extends Model
{
    protected $fillable=['name',];
}
