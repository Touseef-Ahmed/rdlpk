<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Royal_medicare extends Model
{
    protected $fillable=['project_id','title','detail','image1','image2','image3','image4','image5','image6','image7','image8','image9','image10',];
    public function project()
	    {
	        return $this->belongsTo('\App\Project');

	    }
}
