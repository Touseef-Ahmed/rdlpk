<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project_location extends Model
{
   
	protected $fillable=['project_id','title','image',];
   public function project()
	    {
	        return $this->belongsTo('\App\Project');

	    }
}
