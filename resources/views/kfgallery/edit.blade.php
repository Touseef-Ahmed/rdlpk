@extends('layouts.app')
@section('title','Edit feature')
@section('content')
<div class="row-fluid">
	@if(count($errors)>0)
	<ul>
		
		<div class="note note-success"> @foreach($errors->all() as $error)
			<strong style="color: red"> {{  $error }}</strong><br>
			@endforeach
		</div>
		
	</ul>
	@endif
	<div class="span12">
		
		<div class="widget-box">
			<div class="widget-header">
				<h4>Edit Feature</h4>
				<span class="widget-toolbar">
				</span>	</div>
				<div class="widget-body"><div class="widget-body-inner">
					<div class="widget-main">
					{{ Form::open(array('method'=>'PUT','route' => ['kfgallery.update', $kfgallery->id],'files' =>true,'enctype'=>'multipart/form-data')) }}
			
							<div class="row-fluid">
								<label for="form-field-9">Category Name</label>
								<select  name="kfcategory_id" class="span6" id="form-field-select-1">
									
									<option selected value="{{$kfgallery->kfcategory->id}}">{{$kfgallery->kfcategory->name}}</option>
									@foreach($kfcategory as $kfcategory)
									<option value="{{$kfcategory->id}}">{{$kfcategory->name}}</option>
									@endforeach
								</select>
							</div>
							<div class="row-fluid">
								<label for="form-field-9">Feature Name</label>
								<select  name="kfeature_id" class="span6" id="form-field-select-1">
									
									<option selected value="{{$kfgallery->kfeature->id}}">{{$kfgallery->kfeature->name}}</option>
									@foreach($kfeature as $kfeature)
									<option value="{{$kfeature->id}}">{{$kfeature->name}}</option>
									@endforeach
								</select>
							</div>
							
							<div class="row-fluid">
								<label for="form-field-8">Image</label>
								<img src="{{url('/images/kfeature/'.$kfgallery->image)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image" value="" placeholder="">
							</div><hr>
						
							<div class="form-actions">
								<button class="btn btn-info" type="submit"><i class="icon-ok"></i> Submit</button>
								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset"><i class="icon-undo"></i> Reset</button>
							</div> </form>
						</div>
					</div></div>
				</div>
			</div>
			
		</div>
		@endsection