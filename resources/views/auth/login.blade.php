@extends('layouts.app')
@section('content')
<div style="height:200px;" class="12"></div>
<div class="span4"></div><div class="span8">
    
    <div class="widget-box">
        <div class="widget-header">
            <h4>Admin  Panel Login</h4>
            <span class="widget-toolbar">
            </span> </div>
            <div class="widget-body"><div class="widget-body-inner">
                <div class="widget-main">
               
                            <div class="card-body">
                            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                                @csrf
                                <div class="row-fluid">
                                    <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                                    <div class="col-md-6">
                                        <input id="email" type="email"  class="span6 limited {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                        @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                    <div class="col-md-6">
                                        <input id="password" type="password" class="span6 limited {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                        @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                 <div class="row-fluid">
                                    <div class="col-md-6 offset-md-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                          
                                        </div>
                                    </div>
                                </div>
                               <div class="row-fluid">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-info">
                                        {{ __('Login') }}
                                        </button>
                                        
                                    </div>
                                </div>
                            </form>
                        </div>
                    
                </div>
            </div>
            </div>
        </div>
        
    </div>
    @endsection