@extends('layouts.app')
@section('title','Create salescenter')
@section('content')
<div class="row-fluid">
	@if(count($errors)>0)
	<ul>
		
		<div class="note note-success"> @foreach($errors->all() as $error)
			<strong style="color: red"> {{  $error }}</strong><br>
			@endforeach
		</div>
		
	</ul>
	@endif
	<div class="span12">
		
		<div class="widget-box">
			<div class="widget-header">
				<h4>Create Salescenter</h4>
				<span class="widget-toolbar">
				</span>	</div>
				<div class="widget-body"><div class="widget-body-inner">
					<div class="widget-main">
						<form   action="{{url('/salescenter')}}" method="post" enctype="multipart/form-data" accept-charset="utf-8">
							<div class="row-fluid">
								  <input type="hidden" name="noimg" value="No-Image" placeholder="">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<label for="form-field-8">Name</label>
								<input name="name" value="{{old('name')}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Sales Center Name">
							</div>
							<div class="row-fluid">
								  <input type="hidden" name="noimg" value="No-Image" placeholder="">
								<label for="form-field-8">Address</label>
								<input name="address" value="{{old('address')}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Sales Center Address">
							</div>
							<div class="row-fluid">
								  <input type="hidden" name="noimg" value="No-Image" placeholder="">
								<label for="form-field-8">Telephone</label>
								<input name="telephone" value="{{old('telephone')}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Enter Sales Center Phone">
							</div>
							<div class="row-fluid">
								  <input type="hidden" name="noimg" value="No-Image" placeholder="">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<label for="form-field-8">Cell #</label>
								<input name="cell" value="{{old('cell')}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Enter Sales Center Cell No">
							</div>
							<hr>
							
							<div class="row-fluid">	
								<label for="form-field-8">salescenter Image</label>
								 <input type="file" name="image" value="" placeholder="Select Images for salescenter" >
							</div>
							<div class="form-actions">
								<button class="btn btn-info" type="submit"><i class="icon-ok"></i> Submit</button>
								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset"><i class="icon-undo"></i> Reset</button>
							</div> </form>
						</div>
					</div></div>
				</div>
			</div>
			
		</div>
		@endsection