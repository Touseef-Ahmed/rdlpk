@extends('layouts.app')
@section('title','Edit News')
@section('content')
<div class="row-fluid">
	@if(count($errors)>0)
	<ul>
		
		<div class="note note-success"> @foreach($errors->all() as $error)
			<strong style="color: red"> {{  $error }}</strong><br>
			@endforeach
		</div>
		
	</ul>
	@endif
	<div class="span12">
		
		<div class="widget-box">
			<div class="widget-header">
				<h4>Edit News</h4>
				<span class="widget-toolbar">
				</span>	</div>
				<div class="widget-body"><div class="widget-body-inner">
					<div class="widget-main">
					{{ Form::open(array('method'=>'PUT','route' => ['royal_medicare.update', $royal_medicare->id],'files' =>true,'enctype'=>'multipart/form-data')) }}

							<div class="row-fluid">
								  <input type="hidden" name="noimg" value="No-Image" placeholder="">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<label for="form-field-8">Title</label>
								<input name="title" value="{{$royal_medicare->title}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Title">
							</div>
							<hr>
							<div class="row-fluid">
								<label for="form-field-9">Project Name</label>
								<select  name="project_id" class="span6" id="form-field-select-1">
									
									<option selected value="{{$royal_medicare->project->id}}">{{$royal_medicare->project->project_name}}</option>
									@foreach($projects as $project)
									<option value="{{$project->id}}">{{$project->project_name}}</option>
									@endforeach
								</select>
							</div>
							
							<hr>
							
							<div class="row-fluid">
								<label for="form-field-11">Detail</label>
								<textarea name="detail" id="form-field-11" class="autosize-transition span6" style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 58px;">{{$royal_medicare->detail}}</textarea>
							</div>
							<table>
								
								<tbody>
									<tr>
										<td><div class="row-fluid">
								<label for="form-field-8">News Image 1</label>
								<img src="{{url('/images/royal_medicare/'.$royal_medicare->image1)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image1" value="" placeholder="">
							</div></td>
							<td><div class="row-fluid">
								<label for="form-field-8">News Image 2</label>
								<img src="{{url('/images/royal_medicare/'.$royal_medicare->image2)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image2" value="" placeholder="">
							</div></td>
							<td>	
							<div class="row-fluid">
								<label for="form-field-8">News Image 3</label>
								<img src="{{url('/images/royal_medicare/'.$royal_medicare->image3)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image3" value="" placeholder="">
							</div></td>
								<td><div class="row-fluid">
								<label for="form-field-8">News Image 4</label>
								<img src="{{url('/images/royal_medicare/'.$royal_medicare->image4)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image4" value="" placeholder="">
							</div></td>
								<td><div class="row-fluid">
								<label for="form-field-8">News Image 5</label>
								@if(empty($news->image5))
								no-image.png
								@else
								<img src="{{url('/images/royal_medicare/'.$royal_medicare->image5)}}" width="100px"  alt="No Image"><br>
								@endif
								<input type="file" name="image5" value="" placeholder="">
							</div></td>
									</tr>
									<tr><td style="height: 50px;" colspan="10"></td></tr>
									<tr>
										<td><div class="row-fluid">
								<label for="form-field-8">News Image 6</label>
								<img src="{{url('/images/royal_medicare/'.$royal_medicare->image6)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image6" value="" placeholder="">
							</div></td>
										<td><div class="row-fluid">
								<label for="form-field-8">News Image 7</label>
								<img src="{{url('/images/royal_medicare/'.$royal_medicare->image7)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image7" value="" placeholder="">
							</div></td>
										<td><div class="row-fluid">
								<label for="form-field-8">News Image 8</label>
								<img src="{{url('/images/royal_medicare/'.$royal_medicare->image8)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image8" value="" placeholder="">
							</div></td>
										<td><div class="row-fluid">
								<label for="form-field-8">News Image 9</label>
								<img src="{{url('/images/royal_medicare/'.$royal_medicare->image9)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image9" value="" placeholder="">
							</div></td>
										<td><div class="row-fluid">
								<label for="form-field-8">News Image 10</label>
								<img src="{{url('/images/royal_medicare/'.$royal_medicare->image10)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image10" value="" placeholder="">
							</div></td>


									</tr>
								</tbody>
							</table>
							
						
						
							<div class="form-actions">
								<button class="btn btn-info" type="submit"><i class="icon-ok"></i> Submit</button>
								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset"><i class="icon-undo"></i> Reset</button>
							</div> </form>
						</div>
					</div></div>
				</div>
			</div>
			
		</div>
		@endsection