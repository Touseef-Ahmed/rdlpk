@extends('layouts.app')
@section('title','Projects List')
@section('content')<button class="btn-link" style="float: right">  <a href="{{url('royal_medicare/create')}}">Add New Royal Medicare</a></button><br><br>
<div class="row-fluid">
	<!-- <h3 class="header smaller lighter blue">jQuery dataTables</h3> -->
	
	<div class="table-header">
		News Detail 
	</div>
	  @if(session('message'))
        <div class="note note-success"><p>{{session('message')}}</p> </div>
        @endif
		<div id="table_report_wrapper" class="dataTables_wrapper" role="grid"><div class="row-fluid"><div class="span6"><div id="table_report_length" class="dataTables_length"><label>Display <select size="1" name="table_report_length" aria-controls="table_report"><option value="10" selected="selected">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> records</label></div></div><div class="span6"><div class="dataTables_filter" id="table_report_filter"><label>Search: <input type="text" aria-controls="table_report"></label></div></div></div><table id="table_report" class="table table-striped table-bordered table-hover dataTable" aria-describedby="table_report_info">
			<thead>
				<tr role="row"><th class="center sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="
						
					" style="width: 71px;">
						<label><input type="checkbox"><span class="lbl"></span></label>
					</th><th class="sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Domain: activate to sort column ascending" style="width: 307px;">Title</th><th class="sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Price: activate to sort column ascending" style="width: 140px;">Project Name</th><th class="hidden-480 sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 302px;">Detail</th>
					<th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="" style="width: 150px;">Created Date</th>
					<th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="" style="width: 150px;">Updated Date</th><th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="" style="width: 150px;"></th>
				</tr>
			</thead>
									
			
		<tbody role="alert" aria-live="polite" aria-relevant="all">
								
			<tr class="odd">
					<td class="center  sorting_1">
						<label><input type="checkbox"><span class="lbl"></span></label>
					</td>
					<td class=" "><a href="#">{{$royal_medicare->title}}</a></td>
					<td class=" ">
							

						{{$royal_medicare->project->project_name}}</td>
					
					<td class="">{{$royal_medicare->detail}}</td>
						<td class="">{{$royal_medicare->created_at}}</td><td class="">{{$royal_medicare->created_at}}</td><td class=""></td>
				</tr>
				<tr ><td colspan="10" style="text-align: center;"><h4>Images</h4></td></tr>
                      <tr>     <td colspan="10">
                      	<div style="float: left;">
                    
					@if($royal_medicare->image1=='No-Image') 
							<img src="{{url('/images/no-image.png')}}" width="100px"  alt="">

						@else
					<img src="{{url('/images/royal_medicare/'.$royal_medicare->image1)}}" width="150px"  alt="">
						@endif
					</div>
					<div style="float: left;  margin-left: 15px;">
					@if($royal_medicare->image2=='No-Image') 
							<img src="{{url('/images/no-image.png')}}" width="100px"  alt="">

						@else
					<img src="{{url('/images/royal_medicare/'.$royal_medicare->image2)}}" width="150px"  alt="">
						@endif

					</div>
					<div style="float: left;  margin-left: 15px;">
						@if($royal_medicare->image3=='No-Image') 
							<img src="{{url('/images/no-image.png')}}" width="100px"  alt="">

						@else
					<img src="{{url('/images/royal_medicare/'.$royal_medicare->image3)}}" width="150px"  alt="">
						@endif
					</div>
					<div style="float: left;  margin-left: 15px;">
					@if($royal_medicare->image4=='No-Image') 
							<img src="{{url('/images/no-image.png')}}" width="100px"  alt="">

						@else
					<img src="{{url('/images/royal_medicare/'.$royal_medicare->image4)}}" width="150px"  alt="">
						@endif
					</div>
					<div style="float: left; margin-left: 15px;">
					@if($royal_medicare->image5=='No-Image') 
							<img src="{{url('/images/no-image.png')}}" width="100px"  alt="">

						@else
					<img src="{{url('/images/royal_medicare/'.$royal_medicare->image5)}}" width="150px"  alt="">
						@endif
					</div>
                      	<div style="float: left;">
                    
					@if($royal_medicare->image6=='No-Image') 
							<img src="{{url('/images/no-image.png')}}" width="100px"  alt="">

						@else
					<img src="{{url('/images/royal_medicare/'.$royal_medicare->image6)}}" width="150px"  alt="">
						@endif
					</div>
					<div style="float: left;  margin-left: 15px;">
					@if($royal_medicare->image7=='No-Image') 
							<img src="{{url('/images/no-image.png')}}" width="100px"  alt="">

						@else
					<img src="{{url('/images/royal_medicare/'.$royal_medicare->image7)}}" width="150px"  alt="">
						@endif

					</div>
					<div style="float: left;  margin-left: 15px;">
						@if($royal_medicare->image8=='No-Image') 
							<img src="{{url('/images/no-image.png')}}" width="100px"  alt="">

						@else
					<img src="{{url('/images/royal_medicare/'.$royal_medicare->image8)}}" width="150px"  alt="">
						@endif
					</div>
					<div style="float: left;  margin-left: 15px;">
					@if($royal_medicare->image9=='No-Image') 
							<img src="{{url('/images/no-image.png')}}" width="100px"  alt="">

						@else
					<img src="{{url('/images/royal_medicare/'.$royal_medicare->image9)}}" width="150px"  alt="">
						@endif
					</div>
					<div style="float: left; margin-left: 15px;">
					@if($royal_medicare->image10=='No-Image') 
							<img src="{{url('/images/no-image.png')}}" width="100px"  alt="">

						@else
					<img src="{{url('/images/royal_medicare/'.$royal_medicare->image10)}}" width="150px"  alt="">
						@endif
					</div></td>

			</tbody></table><div class="row-fluid">
				<div class="span6">
				</div><div class="span6">
					<div class="dataTables_paginate paging_bootstrap pagination">
					</div></div></div></div>
	
</div>
  @endsection