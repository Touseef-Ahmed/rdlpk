@extends('layouts.app')
@section('title','Create Newsletter')
@section('content')
<div class="row-fluid">
	@if(count($errors)>0)
	<ul>
		
		<div class="note note-success"> @foreach($errors->all() as $error)
			<strong style="color: red"> {{  $error }}</strong><br>
			@endforeach
		</div>
		
	</ul>
	@endif
	<div class="span12">
		
		<div class="widget-box">
			<div class="widget-header">
				<h4>Create Payment Plan</h4>
				<span class="widget-toolbar">
				</span>	</div>
				<div class="widget-body"><div class="widget-body-inner">
					<div class="widget-main">
						<form   action="{{url('/payment_plan')}}" method="post" enctype="multipart/form-data" accept-charset="utf-8">
							<div class="row-fluid">
								  <input type="hidden" name="noimg" value="No-Image" placeholder="">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<label for="form-field-8">Property Type</label>
								<select  name="type" class="span6" id="form-field-select-1">
									
									<option selected value="">Select Type</option>
									
									<option value="RESIDENTIAL PLOTS">RESIDENTIAL PLOTS </option>
									<option value="COMMERCIAL PLOTS">COMMERCIAL PLOTS</option>
									<option value="SHOPS / OFFICES">SHOPS / OFFICES</option>
									<option value="RESIDENTIAL VILLAS">RESIDENTIAL VILLAS</option>
								</select>
							</div>
							<hr>
							<div class="row-fluid">
								<label for="form-field-9">Project Name</label>
								<select  name="project_id" class="span6" id="form-field-select-1">
									
									<option selected value="">Select Project</option>
									@foreach($projects as $project)
									<option value="{{$project->id}}">{{$project->project_name}}</option>
									@endforeach
								</select>
							</div>
							
							<div class="row-fluid">
								  <input type="hidden" name="noimg" value="No-Image" placeholder="">
								<label for="form-field-8">Duration (in Months)</label>
								<input name="duration" value="{{old('duration')}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Duration ">
							</div>
							<div class="row-fluid">
								  <input type="hidden" name="noimg" value="No-Image" placeholder="">
								<label for="form-field-8">Booking </label>
								<input name="booking" value="{{old('booking')}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Booking ">
							</div>
							<div class="row-fluid">
								<label for="form-field-9">Duration Type</label>
								<select  name="duration_type" class="span6" id="form-field-select-1">
									
									<option selected value="">Select Type</option>
									
									<option value="Monthly">Monthly</option>
									<option value="Quarterly">Quarterly</option>
								
								</select>
							</div>
									<hr>
							<div class="row-fluid">	
								<label for="form-field-8">Monthly Plan (Pdf File)</label>
								 <input type="file" name="image_monthly" value="" placeholder="Select Pdf File" >
							</div><hr>
							<div class="row-fluid">	
								<label for="form-field-8">Quarterly Plan (Pdf File)</label>
								 <input type="file" name="image_quarterly" value="" placeholder="Select Image" multiple>
							</div>
							<div class="form-actions">
								<button class="btn btn-info" type="submit"><i class="icon-ok"></i> Submit</button>
								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset"><i class="icon-undo"></i> Reset</button>
							</div> </form>
						</div>
					</div></div>
				</div>
			</div>
			
		</div>
		@endsection