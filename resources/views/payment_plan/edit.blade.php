@extends('layouts.app')
@section('title','Edit News')
@section('content')
<div class="row-fluid">
	@if(count($errors)>0)
	<ul>
		
		<div class="note note-success"> @foreach($errors->all() as $error)
			<strong style="color: red"> {{  $error }}</strong><br>
			@endforeach
		</div>
		
	</ul>
	@endif
	<div class="span12">
		
		<div class="widget-box">
			<div class="widget-header">
				<h4>Edit Payment Plan</h4>
				<span class="widget-toolbar">
				</span>	</div>
				<div class="widget-body"><div class="widget-body-inner">
					<div class="widget-main">
					{{ Form::open(array('method'=>'PUT','route' => ['payment_plan.update', $payment_plan->id],'files' =>true,'enctype'=>'multipart/form-data')) }}

							<div class="row-fluid">
								  <input type="hidden" name="noimg" value="No-Image" placeholder="">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<label for="form-field-8">Property Type</label>
							<select  name="type" class="span6" id="form-field-select-1">
									
									<option selected value="{{$payment_plan->type}}">{{$payment_plan->type}}</option>
									
								<option value="RESIDENTIAL PLOTS">RESIDENTIAL PLOTS </option>
									<option value="COMMERCIAL PLOTS">COMMERCIAL PLOTS</option>
									<option value="SHOPS / OFFICES">SHOPS / OFFICES</option>
									<option value="RESIDENTIAL VILLAS">RESIDENTIAL VILLAS</option>
								
								</select>
							</div>
							<hr>
							<div class="row-fluid">
								<label for="form-field-9">Project Name</label>
								<select  name="project_id" class="span6" id="form-field-select-1">
									
									<option selected value="{{$payment_plan->project->id}}">{{$payment_plan->project->project_name}}</option>
									@foreach($projects as $project)
									<option value="{{$project->id}}">{{$project->project_name}}</option>
									@endforeach
								</select>
							</div>
							<div class="row-fluid">
								<label for="form-field-8">Duration</label>
								<input name="duration" value="{{$payment_plan->duration}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Duration">
							</div>
							<div class="row-fluid">
								<label for="form-field-9">Duration Type</label>
								<select  name="duration_type" class="span6" id="form-field-select-1">
									
									<option selected value="{{$payment_plan->duration_type}}">{{$payment_plan->duration_type}}</option>
									<option value="Monthly">Monthly</option>
									<option value="Quarterly">Quarterly</option>
									</select>
							</div>
							<div class="row-fluid">
								  <input type="hidden" name="noimg" value="No-Image" placeholder="">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<label for="form-field-8">Booking</label>
								<input name="booking" value="{{$payment_plan->booking}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Booking">
							</div>
						
							
							<div class="row-fluid">
								<label for="form-field-8">Monthly Plan (Pdf File)</label>
								@if($payment_plan->image_monthly=='no-image.png')
		No Plan Exists
			@else
	<a href="{{url('/images/payment_plan/'.$payment_plan->image_monthly)}}" class="btn btn-danger " download="">View Plan </a>@endif
								<br>
								<input type="file" name="image_monthly" value="" placeholder="">
							</div><hr>
						<div class="row-fluid">
								<label for="form-field-8">Quraterly Plan</label>
								

								@if($payment_plan->image_quarterly=='no-image.png')
			No Plan Exists
			@else
	<a href="{{url('/images/payment_plan/'.$payment_plan->image_quarterly)}}" class="btn btn-danger " download="">View Plan </a>
			@endif
								<br>
								<input type="file" name="image_quarterly" value="" placeholder="">
							</div>
						
							<div class="form-actions">
								<button class="btn btn-info" type="submit"><i class="icon-ok"></i> Submit</button>
								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset"><i class="icon-undo"></i> Reset</button>
							</div> </form>
						</div>
					</div></div>
				</div>
			</div>
			
		</div>
		@endsection