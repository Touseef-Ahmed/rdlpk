<!DOCTYPE html>
<html dir="ltr" lang="en-US">
	<!-- Mirrored from royalorchard.pk/AboutUs/Mission by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 22 Jul 2019 10:01:25 GMT -->
	<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="author" content="RoyalOrchard" />
		<!-- Stylesheets
		============================================= -->
		<link href="{{URL::asset('Content/font.css')}}" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="{{URL::asset('Content/font-awesome/css/font-awesome.min.css')}}">
		<link rel="stylesheet" href="{{URL::asset('Content/main.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{URL::asset('Content/css/swiper.css')}}" type="text/css" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->
		<!-- Document Title
		============================================= -->
		<title>Royal Orchard</title>
	</head>
	<body class="stretched no-transition">
		<!-- Document Wrapper
		============================================= -->
		<div id="wrapper" class="clearfix">
			<!-- Header============================================= -->
			@extends('layouts.header')
			<!-- #header end -->
			<!-- Page Title
			============================================= -->
			<!-- #page-title end -->
				<!-- Content
				============================================= -->
				<section id="content">
					<div class="content-wrap">
						<div class="container clearfix">
							<div id="section-events" class="heading-block title-center topmargin-lg page-section">
								<h2>Contact Us</h2>
								<span>For booking & detail information, please visit our nearest sales/marketing office</span>
							</div>
							<div class="col-md-4 col-sm-4">
								<h4>Sales Centers</h4>
								@foreach($salescenters as $salescenters)
								<table style="font-size:10px; padding-top:0px; padding-bottom:3px;">
									<tbody>
										<tr>
											<td rowspan="5"><img class="imgclass"  src="{{url('/images/salescenters/'.$salescenters->image)}}"alt="Clients"></td> </tr>
											<tr><td><strong>Name: {{$salescenters->name}}</strong></td></tr>
											<tr><td>Address: {{$salescenters->name}}</td></tr>
											<tr><td>Telephone: {{$salescenters->name}}</td></tr>
											<tr><td>Cell : {{$salescenters->cell}}</td></tr>
										</tbody>
									</table>
									@endforeach
									
									
									
									
								</div>
								@if($salescenters->name=='Central Office, Islamabad')
								<div class="col-md-8 col-sm-6 bottommargin">
									<h4>{{$salescenters->name}}</h4>
									<p>{{$salescenters->address}}<br/>
										Phone: {{$salescenters->telephone}}
										<br/>Cell: {{$salescenters->cell}}
										<br />Email : sales@royalorchard.pk
									</p>
									<div  style="height:300px;"><iframe src="{{$salescenters->map}}"></iframe></div>
								</div>
								@endif <div class="clear"></div>
								<div class="col-md-12 col-sm-12">
									<div class="col-md-9 col-sm-3"><img src="Images/call-us.png" />Speak to us on 111 444 475 &nbsp;&nbsp;&nbsp;&nbsp;
										Toll Free :   0800  (ROYAL)  76925
									</div>
									<div class="col-md-2 col-sm-3"><button type="button" class="btn btn-primary">Send Us an Email</button></div>
									
								</div>
							</div>
							
								<script type="text/javascript">
								var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
								(function () {
								var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
								s1.async = true;
								s1.src = 'https://embed.tawk.to/592411778028bb7327047522/default';
								s1.charset = 'UTF-8';
								s1.setAttribute('crossorigin', '*');
								s0.parentNode.insertBefore(s1, s0);
								})();
								</script>
								<!--End of Tawk.to Script--></body>
								<!-- Mirrored from royalorchard.pk/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 22 Jul 2019 10:00:12 GMT -->
							</html>
							
							
							
						</div>;
					</section>
					<!-- #content end -->
					<!-- Footer
					============================================= -->
				</div>
			</section>
			<!-- Footer============================================= -->
			@extends('layouts.footer')
			<!-- #footer end -->
		</div>
		<!-- #wrapper end -->
		<!-- Go To Top
		============================================= -->
		<div id="gotoTop" class="icon-angle-up"></div>
		<!-- External JavaScripts
		============================================= -->
		<script type="text/javascript" src="../Scripts/js/jquery.js"></script>
		<!-- Footer Scripts
		============================================= -->
		<script type="text/javascript" src="../Scripts/js/functions.js"></script>
	</body>
</html>