<!DOCTYPE html>
<html dir="ltr" lang="en-US">
	<!-- Mirrored from royalorchard.pk/AboutUs/Mission by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 22 Jul 2019 10:01:25 GMT -->
	<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />

		<meta name="author" content="RoyalOrchard" />
		<!-- Stylesheets
		============================================= -->
		<link href="{{URL::asset('Content/font.css')}}" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="{{URL::asset('Content/font-awesome/css/font-awesome.min.css')}}">
		<link rel="stylesheet" href="{{URL::asset('Content/main.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{URL::asset('Content/css/swiper.css')}}" type="text/css" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		
		<title>Royal Orchard</title>
	</head>
	<body class="stretched no-transition">
		<!-- Document Wrapper
		============================================= -->
		<div id="wrapper" class="clearfix">
	<!-- Header============================================= -->
	@extends('layouts.header')
<!-- #header end -->

<!-- Page Title
============================================= -->
<section id="page-title">
<div class="container clearfix">
<!--<h1>Mission And Vision</h1>-->
<img src="../Images/MissionVision.jpg"  />
</div>
</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">
<div class="content-wrap">
	<div class="container clearfix">
		<div class="col_full">
			<h3>Misssion</h3>
			To develop quality projects meeting the modern housing trends and highest living standards of town planning, engineering, aesthetically landscape and homes designing and constructions is our mission. Team of Top Professionals of Real Estate Developers is combined to achieve this goal.
		</div>
		<div class="col_full">
			<h3>Vision</h3>
			To develop quality projects meeting the modern housing trends and highest living standards of town planning, engineering, aesthetically landscape and homes designing and constructions is our mission. Team of Top Professionals of Real Estate Developers is combined to achieve this goal.
		</div>
	
		<div class="clear"></div>
	</div>
</div>
</section>

<!-- Footer============================================= -->
						@extends('layouts.footer')
							<!-- #footer end -->
</div>
<!-- #wrapper end -->
<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>
<!-- External JavaScripts
============================================= -->
<script type="text/javascript" src="../Scripts/js/jquery.js"></script>
<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="../Scripts/js/functions.js"></script>


</body>
</html>