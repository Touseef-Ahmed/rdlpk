<!DOCTYPE html>
<html dir="ltr" lang="en-US">
  <meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="RoyalOrchard" />
    <!-- Stylesheets
    ============================================= -->
    <link href="{{URL::asset('Content/font.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{URL::asset('Content/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('Content/main.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{URL::asset('Content/css/swiper.css')}}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <!-- Document Title
    ============================================= -->
    <title>Royal Orchard</title>
  </head>
	<body class="stretched no-transition">
		<!-- Document Wrapper
		============================================= -->
		
		<div id="wrapper" class="clearfix">
		<!-- Header
============================================= -->
<header id="header" class="transparent-header full-header" data-sticky-class="not-dark">
  <div id="header-wrap">
  <div id="top-bar">
    
    <div class="container clearfix">
      <div class="col_one&quot;">
        <!-- Top Social============================================= -->
        <div id="top-social">
          <ul>
            @php $setting = DB::table('settings')
            ->get();
            @endphp
            @foreach($setting as $setting1)
            
            @if($setting1->name=='UAN')
            <li>&nbsp;&nbsp;&nbsp;UAN:&nbsp;111 444 475&nbsp;&nbsp;&nbsp;</li>@endif
            @if($setting1->name=='Toll Free')
            <li> &nbsp; Toll Free&nbsp;: &nbsp; 0800 &nbsp;(ROYAL) &nbsp;76925</li>
            @endif
            @endforeach
          </ul>
          </div><!-- #top-social end -->
        </div>
        <div class="col_half fright col_last nobottommargin">
          <!-- Top Social
          ============================================= -->
          <div id="top-social"><ul>
            @foreach($setting as $setting)
                @if($setting->name=='Member Login')
            <li>
              <a target="_blank" href="{{$setting->value}}" class="si-facebook" style="width: 40px;" data-hover-width="136"><span class="ts-icon"><i class="fa fa-user"></i></span><span class="ts-text"></span></a>
            </li>
            @endif    
            @if($setting->name=='Facebook Page Link')
            <li><a target="_blank" href="{{$setting->value}}" class="si-facebook" style="width: 40px;" data-hover-width="109"><span class="ts-icon"><i class="fa fa-facebook"></i></span><span class="ts-text">Facebook</span></a></li>@endif
            @if ($setting->name=='Twitter Page Link')
            <li><a target="_blank" href="{{$setting->value}}" class="si-twitter" style="width: 40px;" data-hover-width="95"><span class="ts-icon"><i class="fa fa-twitter"></i></span><span class="ts-text">Twitter</span></a></li>
            @endif
            @if ($setting->name=='Youtube Channel Link')
            <li><a target="_blank" href="{{$setting->value}}" class="si-dribbble" style="width: 40px;" data-hover-width="103"><span class="ts-icon"><i class="fa fa-youtube"></i></span><span class="ts-text">Youtube</span></a></li>@endif
            @if($setting->name=='Email/Mail To')
            <li><a href="{{$setting->value}}" class="si-email3" style="width: 40px;" data-hover-width="153"><span class="ts-icon"><i class="fa fa-envelope"></i></span><span class="ts-text">sales@royalorchard.pk</span></a></li>
            @endif
            
            <!-- #top-social end -->@endforeach</ul></div>
          </div>
                
        </div></div>
        
          <div class="container clearfix">
            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
            <div id="logo">
              <a href="../index.html" class="standard-logo" data-dark-logo="/Images/logo/ro-logo.png"><img   src="http://localhost/royalorchard/images/logo/ro-logo.png"alt="Royal Orchard Logo"></a>
              <a href="../index.html" class="retina-logo" data-dark-logo="/Images/logo/ro-logo.png"><img " src=".http://localhost/royalorchard/images/Images/logo/ro-logo.png" alt="Royal Orchard Logo"></a>
            </div>
            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
            <!-- Primary Navigation
            ============================================= -->
            <nav id="primary-menu" class="dark">
              <ul>
                <li class="current"><a href="{{url('/web')}}"><div>Home</div></a>
                
              </li>
              <li class="current"><a href="#"><div>About US</div></a>
              <ul>
                <li><a href="{{url('/web/mission')}}"><div>Mission & Vision</div></a>
                <li><a href="{{url('/web/objective')}}"><div>Objectives</div></a>
                <li><a href="#"><div>Certifications</div></a>
                
              </li>
              
              
            </ul>
          </li>
          
          <li class="current"><a href="../Multan.html"><div>Current Projects</div></a>
          <ul>
            <li><a href="{{url('/web/multan')}}"><div>Royal Orchard Multan</div></a>
            <li><a href="{{url('/web/sargodha')}}"><div>Royal Orchard Sargodha</div></a>
            <li><a href="{{url('/web/sahiwal')}}"><div>Royal Orchard Sahiwal</div></a>
            
          </li>
          
          
        </ul>
      </li>
      <li class="current"><a href="#"><div>Upcoming Projects</div></a>
      <ul>
        <li><a href="#"><div>Royal Orchard Peshawer</div></a>
        <li><a href="#"><div>Royal Orchard Rahim Yar Khan</div></a>
        <li><a href="#"><div>Royal Orchard Mandi Bahuddin </div></a>
        <li><a href="#"><div>Royal Orchard Gujrat </div></a>
        <li><a href="#"><div>Royal Orchard DG Khan</div></a>
        <li><a href="#"><div>Royal Orchard Islamabad</div></a>
        <li><a href="#"><div>Royal Orchard Lahore</div></a>
        <li><a href="#"><div>Royal Orchard Karachi</div></a>
      </li>
      
      
    </ul>
  </li>
  <li class="current"><a href="#"><div>Media Center</div></a>
  <ul>
    <li><a href="{{url('/web/brouchers')}}"><div>Brouchers</div></a>
    <li><a href="{{url('/web/newsletters')}}"><div>Newsletter Archive</div></a>
    <li><a href="{{url('/web/news')}}"><div>News & Events</div></a>
    
  </li>
  
  
</ul>
</li>
<li class="current"><a href="{{url('/web/contactus')}}"><div>Contact Us</div></a>
</li>
</ul>
<!-- Top Cart
============================================= -->
<!-- Top Search
============================================= -->
</nav><!-- #primary-menu end -->
</div>
</div>
</header><!-- #header end -->

<!-- Page Title
============================================= -->

<section id="development-status" class="page-section">
                
                <!----main entrance start----->
                                <div class="container clearfix">
                  <div class="fancy-title">
                    <h4> {{$news->heading}}</h4>
                  </div>
                  <div class="col-md-8 bottommargin">
                    <div class="col_full bottommargin-lg">
                      <div class="fslider flex-thumb-grid grid-6" data-animation="fade" data-arrows="true" data-thumbs="true">
                        <div class="flexslider" style="height: 402px;">
                          <div class="slider-wrap">
                             @if($news->image1!='No-Image')
                            <div class="slide" data-thumb="{{url('images/news/'.$news->image1)}}" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" data-thumb-alt="">
                              <a href="#">
                                <img src="{{url('images/news/'.$news->image1)}}" alt="" draggable="false">
                                <div class="overlay">
                                  <div class="text-overlay">
                                    <div class="text-overlay-title">
                                      <h3></h3>
                                    </div>
                                    <div class="text-overlay-meta">
                                      <span></span>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </div>@endif
                            @if($news->image2!='No-Image')
                            <div class="slide" data-thumb="{{url('images/news/'.$news->image2)}}" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" data-thumb-alt="">
                              <a href="#">
                                <img src="{{url('images/news/'.$news->image2)}}" alt="" draggable="false">
                                <div class="overlay">
                                  <div class="text-overlay">
                                    <div class="text-overlay-title">
                                      <h3></h3>
                                    </div>
                                    <div class="text-overlay-meta">
                                      <span>/span&gt;
                                    </span></div>
                                  </div>
                                </div>
                              </a>
                            </div>
                            @endif
                              @if($news->image3!='No-Image')
                            <div class="slide flex-active-slide" data-thumb="{{url('images/news/'.$news->image3)}}" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" data-thumb-alt="">
                              <a href="#">
                                <img src="{{url('images/news/'.$news->image3)}}" alt="" draggable="false">
                                <div class="overlay">
                                  <div class="text-overlay">
                                    <div class="text-overlay-title">
                                      <h3></h3>
                                    </div>
                                    <div class="text-overlay-meta">
                                      <span></span>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </div>
                            @endif
                              @if($news->image4!='No-Image')
                            <div class="slide" data-thumb="{{url('images/news/'.$news->image4)}}" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" data-thumb-alt="">
                              <a href="#">
                                <img src="{{url('images/news/'.$news->image4)}}" alt="" draggable="false">
                                <div class="overlay">
                                  <div class="text-overlay">
                                    <div class="text-overlay-title">
                                      <h3></h3>
                                    </div>
                                    <div class="text-overlay-meta">
                                      <span></span>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </div>
                            @endif
                              @if($news->image5!='No-Image')
                            <div class="slide" data-thumb="{{url('images/news/'.$news->image5)}}" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" data-thumb-alt="">
                              <a href="#">
                                <img src="{{url('images/news/'.$news->image5)}}" alt="" draggable="false">
                                <div class="overlay">
                                  <div class="text-overlay">
                                    <div class="text-overlay-title">
                                      <h3></h3>
                                    </div>
                                    <div class="text-overlay-meta">
                                      <span><i class="fa fa-star3"></i><i class="fa fa-star3"></i><i class="fa fa-star3"></i><i class="fa fa-star-half-full"></i><i class="fa fa-star-empty"></i></span>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </div>
                             @endif
                              @if($news->image6!='No-Image')
                            <div class="slide" data-thumb="{{url('images/news/'.$news->image6)}}" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" data-thumb-alt="">
                              <a href="#">
                                <img src="{{url('images/news/'.$news->image6)}}" alt="" draggable="false">
                                <div class="overlay">
                                  <div class="text-overlay">
                                    <div class="text-overlay-title">
                                      <h3></h3>
                                    </div>
                                    <div class="text-overlay-meta">
                                      <span></span>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </div>
                             @endif
                              @if($news->image7!='No-Image')
                             <div class="slide" data-thumb="{{url('images/news/'.$news->image7)}}" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" data-thumb-alt="">
                              <a href="#">
                                <img src="{{url('images/news/'.$news->image7)}}" alt="" draggable="false">
                                <div class="overlay">
                                  <div class="text-overlay">
                                    <div class="text-overlay-title">
                                      <h3></h3>
                                    </div>
                                    <div class="text-overlay-meta">
                                      <span></span>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </div>
                             @endif
                              @if($news->image8!='No-Image')
                             <div class="slide" data-thumb="{{url('images/news/'.$news->image8)}}" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" data-thumb-alt="">
                              <a href="#">
                                <img src="{{url('images/news/'.$news->image8)}}" alt="" draggable="false">
                                <div class="overlay">
                                  <div class="text-overlay">
                                    <div class="text-overlay-title">
                                      <h3></h3>
                                    </div>
                                    <div class="text-overlay-meta">
                                      <span></span>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </div>
                             @endif
                              @if($news->image9!='No-Image')
                             <div class="slide" data-thumb="{{url('images/news/'.$news->image9)}}" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" data-thumb-alt="">
                              <a href="#">
                                <img src="{{url('images/news/'.$news->image9)}}" alt="" draggable="false">
                                <div class="overlay">
                                  <div class="text-overlay">
                                    <div class="text-overlay-title">
                                      <h3></h3>
                                    </div>
                                    <div class="text-overlay-meta">
                                      <span></span>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </div>
                             @endif
                              @if($news->image10!='No-Image')
                             <div class="slide" data-thumb="{{url('images/news/'.$news->image10)}}" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" data-thumb-alt="">
                              <a href="#">
                                <img src="{{url('images/news/'.$news->image10)}}" alt="" draggable="false">
                                <div class="overlay">
                                  <div class="text-overlay">
                                    <div class="text-overlay-title">
                                      <h3></h3>
                                    </div>
                                    <div class="text-overlay-meta">
                                      <span></span>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </div>@endif
                          </div>
                          <ul class="flex-direction-nav">
                            <li class="flex-nav-prev"><a class="flex-prev" href="#"><i class="icon-angle-left"></i></a></li>
                            <li class="flex-nav-next"><a class="flex-next" href="#"><i class="icon-angle-right"></i></a></li>
                          </ul>
                      </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 bottommargin">
                      <div class="panel panel-default events-meta">
                  <div class="panel-heading">
                    <h3 class="panel-title">News Info:</h3>
                  </div>
                  <div class="panel-body">
                    <ul class="iconlist nobottommargin">
                      <li><i class="icon-calendar3"></i>{{$news->created_at}} </li>
                   
                      <li><i class="icon-map-marker2"></i> {{$news->location}}</li>
                    
                    </ul>
                  </div>
                </div>
                     </div>
                 
                    
                  </div>
                </div>
                              
                       
              </section>
    <!-- Content
    ============================================= -->
    <section id="content">

      <div class="content-wrap">

        <div class="container clearfix">

          <div class="postcontent clearfix">

            <div class="single-event">

              <div class="clear"></div>

              <div class="col_full">

              <h3>Details</h3>
              <p>Residentail Plots Balloting Ceremony held at Royal Orchard Housing Multan at evening of 8th April, 2016. Online balloting results has been published on 11th April, 2016.</p>
          
              <div class="clear"></div>
                              </div>
            </div>
          </div>  
        </div>
      </div>

    </section><!-- #content end -->
		<!-- Footer
		============================================= -->
		@extends('layouts.footer')
		<!-- #footer end -->
		
	</div>
	<!-- #wrapper end -->
	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>
	<!-- External JavaScripts
	============================================= -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script type="text/javascript" src="http://localhost/royalorchard/Scripts/js/jquery.js"></script>
        <!-- Footer Scripts
        ============================================= -->
        <script type="text/javascript" src="http://localhost/royalorchard/Scripts/js/functions.js"></script>
	<!-- Footer Scripts
	============================================= -->
	
</body>

</html>