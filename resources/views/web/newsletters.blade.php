<!DOCTYPE html>
<html dir="ltr" lang="en-US">
	<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="author" content="RoyalOrchard" />
		<!-- Stylesheets
		============================================= -->
		<link href="{{URL::asset('Content/font.css')}}" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="{{URL::asset('Content/font-awesome/css/font-awesome.min.css')}}">
		<link rel="stylesheet" href="{{URL::asset('Content/main.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{URL::asset('Content/css/swiper.css')}}" type="text/css" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->
		<!-- Document Title
		============================================= -->
		<title>Royal Orchard</title>
	</head>
	<body class="stretched no-transition">
		<!-- Document Wrapper
		============================================= -->
		<div id="wrapper" class="clearfix">
			<!-- Header============================================= -->
			@extends('layouts.header')
			<!-- #header end -->
			
			<!-- Page Title============================================= -->
			<section id="page-title" >
				<div class="container clearfix">
					<h1>Newsletters </h1>
					
				</div>
			</section>
			<!-- #page-title end -->
			<!-- Content
			============================================= -->
			<section id="content">
				<div class="content-wrap">
					<div class="container clearfix">
						<div class="postcontent nobottommargin">

							
							<div id="posts" class="events small-thumbs">

								@foreach($newsletters as $newsletters)
								<div class="entry clearfix">
									<div class="entry clearfix">
										<div class="entry-image">
											<a href="#" target="_self">
												<img width="200px" height="250px"  src="{{url('/images/newsletters/'.$newsletters->thumbnail)}}" alt="Image">
												
											</a>
										</div>
										<div class="entry-title">
										<h2><a href="#">{{$newsletters->title}}</a></h2>
									</div>
									<div class="entry-title">
										<h4><a href="#">{{$newsletters->project->project_name}}</a></h4>
									</div>
										<div class="entry-content">
									<a href="{{url('/images/newsletters/'.$newsletters->file)}} " class="btn btn-danger" target="_blank">Download</a>
									</div>
										
									</div>									
								</div>

								@endforeach
							</div>

							
						</div>
					</div>
					</section><!-- #content end -->
					<!-- Footer============================================= -->
					@extends('layouts.footer')
					<!-- #footer end -->
				</div>
				<!-- #wrapper end -->
				<!-- Go To Top
				============================================= -->
				<div id="gotoTop" class="icon-angle-up"></div>
				<!-- External JavaScripts
				============================================= -->
				<script type="text/javascript" src="../Scripts/js/jquery.js"></script>
				<!-- Footer Scripts
				============================================= -->
				<script type="text/javascript" src="../Scripts/js/functions.js"></script>
			</body>
		</html>