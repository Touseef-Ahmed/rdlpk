<!DOCTYPE html>
<html dir="ltr" lang="en-US">
	<!-- Mirrored from royalorchard.pk/AboutUs/Mission by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 22 Jul 2019 10:01:25 GMT -->
	<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="author" content="RoyalOrchard" />
		<!-- Stylesheets
		============================================= -->
		<link href="{{URL::asset('Content/font.css')}}" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="{{URL::asset('Content/font-awesome/css/font-awesome.min.css')}}">
		<link rel="stylesheet" href="{{URL::asset('Content/main.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{URL::asset('Content/css/swiper.css')}}" type="text/css" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->
		<!-- Document Title
		============================================= -->
		<title>Royal Orchard</title>
	</head>
	<body class="stretched no-transition">
		<!-- Document Wrapper
		============================================= -->
		<div id="wrapper" class="clearfix">
			<!-- Header============================================= -->
			@extends('layouts.header')
			<!-- #header end -->
			<!-- Page Title
			============================================= -->
			<!-- Page Title
			============================================= -->
			<div style="margin-top:100px;"></div>
			<section id="page-title">
				<div class="container clearfix">
					<!--<h1>Mission And Vision</h1>-->
					<img src="../Images/objectives.jpg"  />
				</div>
				</section><!-- #page-title end -->
				<!-- Content
				============================================= -->
				<section id="content">
					<div class="content-wrap">
						<div class="container clearfix">
							<div class="col_full">
								<h3>Objectivies</h3>
								
								Believing is gradual but sustained success Royal Developers & Builders is focusing on off-mainstream development markets. It also speaks for our commitment towards development of the less and the underserved regions of Pakistan.
								<br />   <br />
								With more & more landmark projects delivery to be recognized as a hallmark of constructing world-class housing facilities & gated communities.<br />   <br />
								<ul><li>
								To be known for uncompromising excellence delivered in unbelievable time.</li>
								<li> Offering most lucrative return on investment with most secure & affordable financial feasibilities.</li>
								<li>  The most experienced & expert team of town planners, architects, engineers and marketers.</li>
								<li> Specialists in concept, design, development, building, sales & delivery.</li>
							</ul>
						</div>
						
						<div class="clear"></div>
					</div>
				</div>
				</section><!-- #content end -->
				<!-- Footer============================================= -->
				@extends('layouts.footer')
				<!-- #footer end -->
			</div>
			<!-- #wrapper end -->
			<!-- Go To Top
			============================================= -->
			<div id="gotoTop" class="icon-angle-up"></div>
			<!-- External JavaScripts
			============================================= -->
			<script type="text/javascript" src="../Scripts/js/jquery.js"></script>
			<!-- Footer Scripts
			============================================= -->
			<script type="text/javascript" src="../Scripts/js/functions.js"></script>
		</body>
	</html>