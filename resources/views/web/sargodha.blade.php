<!DOCTYPE html>
<html dir="ltr" lang="en-US">
	<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="author" content="RoyalOrchard" />
		<!-- Stylesheets
		============================================= -->
		<link href="{{URL::asset('Content/font.css')}}" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="{{URL::asset('Content/font-awesome/css/font-awesome.min.css')}}">
		<link rel="stylesheet" href="{{URL::asset('Content/main.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{URL::asset('Content/css/swiper.css')}}" type="text/css" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->
		<!-- Document Title
		============================================= -->
		<title>Royal Orchard</title>
	</head>
	<body class="stretched no-transition">
		<!-- Document Wrapper
		============================================= -->
		<div id="wrapper" class="clearfix">
			<!-- Header============================================= -->
			@extends('layouts.header')
			<!-- #header end -->
			@foreach($projects as $projects)
			<!-- Page Title============================================= -->
			<section id="slider" class="slider-parallax full-screen force-full-screen with-header swiper_wrapper page-section clearfix">
				<div class="slider-parallax-inner">
					<div class="swiper-container swiper-parent">
						<div class="swiper-wrapper">
							<div class="swiper-slide dark" style="background-image:url({{url('images/projects/'.$projects->imagemain)}})">
								<div class="container clearfix">
									<div class="slider-caption slider-caption-center">
										<h2 data-caption-animate="fadeInUp"> Welcome to Royal Orchard Sargodha
</h2>
										
									</div>
								</div>
							</div>
							
						</div>
						
						
						<a href="#" data-scrollto="#section-about" class="one-page-arrow dark">
							<i class="fa fa-angle-down infinite animated fadeInDown"></i>
						</a>
					</div>
				</div>
			</section>
			<!-- Page Sub Menu
			============================================= -->
			<div id="page-menu">
				<div id="page-menu-wrap">
					<div class="container clearfix">
						<nav class="one-page-menu">
							<ul>
								<li><a href="#" data-href="#header">
									<div>Overview</div>
								</a></li>
								<li><a href="#" data-href="#location-plan">
									<div>Location</div>
								</a></li>
								<li><a href="#" data-href="#image-gallery">
									<div>Gallery</div>
								</a></li>
								<li><a href="#" data-href="#master-plan">
									<div>Master Plans</div>
								</a></li>
								<li><a href="#" data-href="#features">
									<div>Features</div>
								</a></li>
								
								<li><a href="#" data-href="#villas">
									<div>Villas</div>
								</a></li>
								
								
								<li><a href="#" data-href="#section-pricing">
									<div>Payment Schedule</div>
								</a></li>
								
								<li><a href="#development-status" data-href="#development-status">
									<div>Development Status</div>
								</a></li>
								<li><a href="#" data-href="#section-contact" class="no-offset"><div>News & Events</div></a></li>
							</ul>
						</nav>
						<div id="page-submenu-trigger"><i class="fa fa-reorder"></i></div>
					</div>
				</div>
			</div>
			<!-- #page-menu end -->
			<!-- Content
			============================================= -->
			<section id="content">
				<div class="content-wrap">
					<section id="section-about" class="page-section">
						<div class="container clearfix">
							<div class="col-md-6 col-sm-6 bottommargin">
								<h2>{{$projects->project_name}}</h2>
								<h6>
								{{ $projects->project_detail }}
								</h6>
							</div>
							<div class="col-md-6 col-sm-6 bottommargin">
								<img src="{{url('images/projects/'.$projects->imagetop)}}" />
							</div>
							<div class="clear"></div>
						</div>
						<div class="container clearfix">
							<div class="col_one_fourth nobottommargin">
								<div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
									<div class="fbox-icon">
										<a href="#"><i class="i-alt noborder fa fa-globe"></i></a>
									</div>
									<h3>Project Location</h3>
									<span>  {{ $projects->project_location }}</span>
								</div>
							</div>
							<div class="col_one_fourth nobottommargin">
								<div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
									<div class="fbox-icon">
										<a href="#"><i class="i-alt noborder fa fa-check"></i></a>
									</div>
									<h3>Project Attraction</h3>
									<span>  {{ $projects->project_attraction }}</span>
								</div>
							</div>
							<div class="col_one_fourth nobottommargin">
								<div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
									<div class="fbox-icon">
										<a href="#"><i class="i-alt noborder fa fa-edit"></i></a>
									</div>
									<h3>Project Type</h3>
									<span>  {{ $projects->project_type }}</span>
								</div>
							</div>
							<div class="col_one_fourth nobottommargin col_last">
								<div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
									<div class="fbox-icon">
										<a href="#"><i class="i-alt noborder fa fa-bullhorn"></i></a>
									</div>
									<h3>Booking</h3>
									<span>Booking is open for this project.</span>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="section dark parallax nobottommargin" style="padding: 80px 0; background-image:url({{url('images/projects/'.$projects->imagebottom)}})" data-stellar-background-ratio="0.3">
							<div class="container clearfix">
								<div class="col_one_fourth nobottommargin center" data-animate="bounceIn">
									<i class="i-plain i-xlarge divcenter nobottommargin fa fa-code"></i>
									<div class="counter counter-lined"><span data-from="100" data-to="4000" data-refresh-interval="50" data-speed="2000"></span></div>
									<h5>Residential Units</h5>
								</div>
								<div class="col_one_fourth nobottommargin center" data-animate="bounceIn" data-delay="200">
									<i class="i-plain i-xlarge divcenter nobottommargin fa fa-magic"></i>
									<div class="counter counter-lined"><span data-from="3000" data-to="500" data-refresh-interval="100" data-speed="2500"></span></div>
									<h5>Commercial Units</h5>
								</div>
								<div class="col_one_fourth nobottommargin center" data-animate="bounceIn" data-delay="400">
									<i class="i-plain i-xlarge divcenter nobottommargin fa fa-file-text"></i>
									<div class="counter counter-lined"><span data-from="10" data-to="250" data-refresh-interval="25" data-speed="3500"></span></div>
									<h5>Residential Villas</h5>
								</div>
								<div class="col_one_fourth nobottommargin center col_last" data-animate="bounceIn" data-delay="600">
									<i class="i-plain i-xlarge divcenter nobottommargin fa fa-time"></i>
									<div class="counter counter-lined"><span data-from="60" data-to="200" data-refresh-interval="30" data-speed="2700"></span>+</div>
									<h5>Offices/Shops</h5>
								</div>
							</div>
						</div>
						@endforeach
					</section>
					<section id="location-plan" class="page-section">
						<div class="heading-block center">
							<h2>Project Location</h2>
							<span>Location key plan, google plan & distance from important locations</span>
						</div>
						<div class="container clearfix center">
							<!-- Portfolio Items
							============================================= -->
							<div id="portfolio" class="portfolio clearfix">
								@foreach($location as $location)
								<div class="col-sm-6 col-md-6">
									<div class="portfolio-image nobottommargin col_last">
										
										<a target="_blank" href="{{url('images/project_location/'.$location->image)}}" data-lightbox="gallery-item">
										<img class="image_fade" src="{{url('images/project_location/'.$location->image)}}" alt="Gallery Thumb 3"></a>
										
									</div>
									<div class="portfolio-desc">
										<h3><a href="#">{{$location->title}}</a></h3>
									</div>
								</div>
								
								@endforeach
							</div>
							<!-- #portfolio end -->
							<div class="divider"><i class="fa fa-circle"></i></div>
						</div>
					</section>
					<section id="image-gallery" class="page-section">
						<div class="container topmargin clearfix">
							<div class="heading-block noborder">
								<h2 class="center">Image Gallery</h2>
							</div>
							<!---Magestic Entrance Start-->
							@foreach($gallery as $gallery)
							<div class="col-sm-6 col-md-6 portfolio-item">
								<h3><a >{{$gallery->title}}</a></h3>
								<div class="portfolio-image">
									<div class="fslider" data-arrows="false" data-speed="400" data-pause="4000">
										<div class="flexslider">
											<div class="slider-wrap">
												<div class="slide"><a href="#"><img src="{{url('images/gallery/'.$gallery->image1)}}" alt="Morning Dew"></a></div>
												<div class="slide"><a href="#"><img src="{{url('images/gallery/'.$gallery->image2)}}" alt="Morning Dew"></a></div>
													<div class="slide"><a href="#"><img src="{{url('images/gallery/'.$gallery->image3)}}" alt="Morning Dew"></a></div>
														<div class="slide"><a href="#"><img src="{{url('images/gallery/'.$gallery->image4)}}" alt="Morning Dew"></a></div>
															<div class="slide"><a href="#"><img src="{{url('images/gallery/'.$gallery->image5)}}" alt="Morning Dew"></a></div>
																<div class="slide"><a href="#"><img src="{{url('images/gallery/'.$gallery->image6)}}" alt="Morning Dew"></a></div>												
											</div>
										</div>
									</div>
									<div class="portfolio-overlay" data-lightbox="gallery">
										<a href="{{url('images/gallery/'.$gallery->image1)}}" class="left-icon" data-lightbox="gallery-item"><i class="fa fa-line-stack-2"></i></a>
										
										
									</div>
								</div>
							</div>@endforeach
							<!---Magestic Entrance End-->
							
							<div class="clear"></div>
						</div>
					</section>
					<section id="master-plan" class="page-section">
						<div class="heading-block center">
							<h2>Master Plan</h2>
							<span>Every thing is designed, few things are designed well.</span>
						</div>
						<div class="container clearfix">
							<div class="col_full clearfix">
							@foreach($plan as $plan)
								<a href="{{url('images/master_plan'.$plan->image)}}" data-lightbox="gallery-item">
								<img class="image_fade" src="{{url('images/master_plan/'.$plan->image)}}" alt="Gallery Thumb 3"></a>
								@endforeach
							</div>
							<div class="divider"><i class="fa fa-circle"></i></div>
						</div>
					</section>
					<section id="features" class="page-section">
						<div class="heading-block center">
							<h2>Salient Features</h2>
							<span>Major Key Features of the Project.</span>
						</div>
						<div class="container clearfix">
							<!-- Post Content
							============================================= -->
							<div>
								<!-- Portfolio Items
								============================================= -->
								<div id="Div1" class="portfolio portfolio-1 grid-container clearfix"
									style="position: relative;">
									<article class="portfolio-item alt clearfix">
										<div class="col-sm-6 col-md-6">
											<ul class="iconlist iconlist-color">
												<li><i class="fa fa-caret-right"></i>Paramount and unique residential and commercial project with modern facilities backed by Habib Rafiq(Pvt.)Limited</li>
												<li><i class="fa fa-caret-right"></i>2 Minutes Drive from Bosan Road</li>
												<li><i class="fa fa-caret-right"></i>Located at Multan Public School Road and Shakh e Madina Road </li>
												<li><i class="fa fa-caret-right"></i>Pakistan's Largest Roundabout by private sector consisting Multan's biggest Jamia Mosque will be set new development standards in the region.</li>
												<li><i class="fa fa-caret-right"></i>Lifetime maintenance by the Developers itself.</li>
											</ul>
										</div>
										<div class="col-sm-6 col-md-6">
											<ul class="iconlist iconlist-color">
												<li><i class="fa fa-caret-right"></i>Just 9 Minutes drive from Multan Internation Airport</li>
												<li><i class="fa fa-caret-right"></i>1/2 Minutes drive from Nothern Bypass</li>
												<li><i class="fa fa-caret-right"></i>State-of-the-art design consist of Pakistan's Largest Commercial Broadway on a 590 feet wide road naming Great Southern Commercial and Nothern Commercial</li>
												<li><i class="fa fa-caret-right"></i>Secured life style brings International living standards to Southern Punjab</li>
											</ul>
										</div>
									</article>
									@foreach($sfeature as $sfeature)
									<article class="portfolio-item alt clearfix">
										<div class="portfolio-image">
											<div class="fslider" data-arrows="false" data-speed="400" data-pause="4000">
												<div class="flexslider">
													<div class="slider-wrap">
														<div class="slide"><a href="#">
													<img src="{{url('images/sfeatures/'.$sfeature->image1)}}" alt=""></a></div>

														@if($sfeature->image2!='No-Image')
													<div class="slide"><a href="#">
													<img src="{{url('images/sfeatures/'.$sfeature->image2)}}" alt=""></a>
												</div>@endif
												@if($sfeature->image3!='No-Image')
													<div class="slide"><a href="#">
													<img src="{{url('images/sfeatures/'.$sfeature->image3)}}" alt=""></a>
												</div>@endif
												@if($sfeature->image4!='No-Image')
													<div class="slide"><a href="#">
													<img src="{{url('images/sfeatures/'.$sfeature->image4)}}" alt=""></a>
												</div>@endif
												@if($sfeature->image5!='No-Image')
													<div class="slide"><a href="#">
													<img src="{{url('images/sfeatures/'.$sfeature->image5)}}" alt=""></a>
												</div>@endif

													</div>
												</div>
											</div>
											<div class="portfolio-overlay" data-lightbox="gallery">
												<a href="{{url('images/sfeatures/'.$sfeature->image1)}}" class="left-icon" data-lightbox="gallery-item"><i class="fa fa-line-stack-2"></i></a>
													@if($sfeature->image2!='No-Image')
													<a href="{{url('images/sfeatures/'.$sfeature->image2)}}" class="left-icon" data-lightbox="gallery-item"><i class="fa fa-line-stack-2"></i></a>
													@endif
													@if($sfeature->image3!='No-Image')
													<a href="{{url('images/sfeatures/'.$sfeature->image3)}}" class="left-icon" data-lightbox="gallery-item"><i class="fa fa-line-stack-2"></i></a>
													@endif
													@if($sfeature->image4!='No-Image')
													<a href="{{url('images/sfeatures/'.$sfeature->image4)}}" class="left-icon" data-lightbox="gallery-item"><i class="fa fa-line-stack-2"></i></a>
													@endif
													@if($sfeature->image5!='No-Image')
													<a href="{{url('images/sfeatures/'.$sfeature->image5)}}" class="left-icon" data-lightbox="gallery-item"><i class="fa fa-line-stack-2"></i></a>
													@endif
											</div>
											
										</div>
										<div class="portfolio-desc">
											<h3>{{$sfeature->title}}</h3>
											{!! $sfeature->detail !!}
										</div>
									</article>@endforeach
									
									
								
								</div>
								<!-- #portfolio end -->
							</div>
							<!-- .postcontent end -->
						</div>
					</section>
					
							<div class="clear"></div>
							
	
							<section id="section-pricing" class="page-section">
								<div class="heading-block center">
									<h2>Payment Plans</h2>
									<span>We offer easy, affordable & flexible installment options.</span>
								</div>
								<div class="container clearfix">
									<div class="pricing bottommargin clearfix">
										@foreach($pplan as $pplan)
										<div class="col-md-3" data-animate="fadeInLeft">
											<div class="pricing-box">
												<div class="pricing-title">
													<h3>{{$pplan->type}}</h3>
												</div>
												<div class="pricing-price">
													{{$pplan->duration}}<span class="price-tenure">months</span>
												</div>
												<div class="pricing-features">
													<ul>
														<li><strong>{{$pplan->duration_type}}</strong> </li>
														<li>{{$pplan->booking}}</li>
														
													</ul>
												</div>
												<div class="pricing-action" data-lightbox="gallery">
													<a href="{{url('images/payment_plan/'.$pplan->image_monthly)}}" class="button button-3d button-large button-rounded button-red" data-lightbox="gallery-item">View Plan</a>
													<a href="{{url('images/payment_plan/'.$pplan->image_quarterly)}}" target="_parent">
													<img src="Images/pdficon.png" style="height: 45px;" /></a>
												</div>
											</div>
										</div>
										@endforeach
									</div>
								</div>
							</section>
							<section id="development-status" class="page-section">
								<div class="heading-block center">
									<h2>Development Progress</h2>
									<span>Glimpse of Construction Work</span>
								</div>
								<!----main entrance start----->
								@foreach($dp as $dp)
								<div class="container clearfix">
									<div class="fancy-title title-border">
										<h4>{{$dp->category->name}}</h4>
									</div>
									<div class="col-md-8 bottommargin">
										<div class="col_full bottommargin-lg">
											<div class="fslider flex-thumb-grid grid-6" data-animation="fade" data-arrows="true" data-thumbs="true">
												<div class="flexslider" style="height: 422px;">
													<div class="slider-wrap">
														<div class="slide" data-thumb="{{url('images/development_progress/'.$dp->image1)}}" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" data-thumb-alt="">
															<a href="#">
																<img src="{{url('images/development_progress/'.$dp->image1)}}" alt="" draggable="false">
																<div class="overlay">
																	<div class="text-overlay">
																		<div class="text-overlay-title">
																			<h3></h3>
																		</div>
																		<div class="text-overlay-meta">
																			<span></span>
																		</div>
																	</div>
																</div>
															</a>
														</div>
														<div class="slide" data-thumb="{{url('images/development_progress/'.$dp->image2)}}" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" data-thumb-alt="">
															<a href="#">
																<img src="{{url('images/development_progress/'.$dp->image2)}}" alt="" draggable="false">
																<div class="overlay">
																	<div class="text-overlay">
																		<div class="text-overlay-title">
																			<h3></h3>
																		</div>
																		<div class="text-overlay-meta">
																			<span>/span>
																		</div>
																	</div>
																</div>
															</a>
														</div>
														<div class="slide" data-thumb="{{url('images/development_progress/'.$dp->image3)}}" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" data-thumb-alt="">
															<a href="#">
																<img src="{{url('images/development_progress/'.$dp->image3)}}" alt="" draggable="false">
																<div class="overlay">
																	<div class="text-overlay">
																		<div class="text-overlay-title">
																			<h3></h3>
																		</div>
																		<div class="text-overlay-meta">
																			<span></span>
																		</div>
																	</div>
																</div>
															</a>
														</div>
														<div class="slide" data-thumb="{{url('images/development_progress/'.$dp->image4)}}" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" data-thumb-alt="">
															<a href="#">
																<img src="{{url('images/development_progress/'.$dp->image4)}}" alt="" draggable="false">
																<div class="overlay">
																	<div class="text-overlay">
																		<div class="text-overlay-title">
																			<h3></h3>
																		</div>
																		<div class="text-overlay-meta">
																			<span></span>
																		</div>
																	</div>
																</div>
															</a>
														</div>
														<div class="slide" data-thumb="{{url('images/development_progress/'.$dp->image5)}}" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" data-thumb-alt="">
															<a href="#">
																<img src="{{url('images/development_progress/'.$dp->image5)}}" alt="" draggable="false">
																<div class="overlay">
																	<div class="text-overlay">
																		<div class="text-overlay-title">
																			<h3></h3>
																		</div>
																		<div class="text-overlay-meta">
																			<span><i class="fa fa-star3"></i><i class="fa fa-star3"></i><i class="fa fa-star3"></i><i class="fa fa-star-half-full"></i><i class="fa fa-star-empty"></i></span>
																		</div>
																	</div>
																</div>
															</a>
														</div>
														<div class="slide" data-thumb="{{url('images/development_progress/'.$dp->image6)}}" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" data-thumb-alt="">
															<a href="#">
																<img src="{{url('images/development_progress/'.$dp->image6)}}" alt="" draggable="false">
																<div class="overlay">
																	<div class="text-overlay">
																		<div class="text-overlay-title">
																			<h3></h3>
																		</div>
																		<div class="text-overlay-meta">
																			<span></span>
																		</div>
																	</div>
																</div>
															</a>
														</div>
													</div>
													<ul class="flex-direction-nav">
														<li class="flex-nav-prev"><a class="flex-prev" href="#"><i class="fa fa-angle-left"></i></a></li>
														<li class="flex-nav-next"><a class="flex-next" href="#"><i class="fa fa-angle-right"></i></a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4 bottommargin">
										<!--<iframe src="/Images/projects/ROMN/Progress/progress.mp4" allowfullscreen="" frameborder="0"></iframe>-->
										<iframe src="{{$dp->link1}}" width="460" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
										
									</div>
								</div>
								@endforeach
								<!------main entrance end--->
								<!-----Horticulture start--->
								
								<!--------Horticulture end----->
								<!-----Jamia Mosque start--->
								
								<!--------Jamia Mosque end----->
								<!-----Residential Villas start--->
								
								<!--------Residential Villas----->
								<!-----Horticulure start--->
								
								<!--------Horticulure Villas----->
							</section>
							<section id="section-contact" class="page-section">
											<div id="Div3" class="heading-block title-center topmargin-lg page-section">
												<h2>NEWS AND EVENTS</h2>
												<span>Latest News and Events</span>
											</div>
											
											<div class="container clearfix">
												<div class="col_three_third nobottommargin">
													<div class="fancy-title title-border">
														<h4>Recent News & Events</h4>
													</div>@foreach($news as $news)
													<div class="col-md-4 col-sm-4">
														<div class="ipost clearfix">
															<div class="entry-image">
																<a target="_blank" href="{{url('web/news_detail/'.$news->id)}}"><img class="image_fade" src="{{url('/images/news/'.$news->image1)}}" alt="Image" style="opacity: 1;"></a>
															</div>
															<div class="entry-title">
																<h3><a target="_blank href="Multan/SJ_Visit_19.html">{{$news->heading}}</a></h3>
															</div>
															<ul class="entry-meta clearfix">
																<li><i class="icon-calendar3"></i> {{$news->created_at}}</li>
																
															</ul>
															<div class="entry-content">
																<p>{{$news->heading}}
																	<h5> <a target="_blank" href="{{url('web/news_detail/'.$news->id)}}"><input class="btn btn-primary btn-xs" value="More Detail" type="button"></a></h5>
																</p>
															</div>
														</div>
													</div>@endforeach
												
													<div class="clear"></div>
												</div>
												
												
											</div>
										</section>
						</div>
					</section>
					<!-- #content end -->
					
					<!-- Footer============================================= -->
					@extends('layouts.footer')
					<!-- #footer end -->
				</div>
				<!-- #wrapper end -->
				<!-- Go To Top
				============================================= -->
				<div id="gotoTop" class="icon-angle-up"></div>
				<!-- External JavaScripts
				============================================= -->
				<script type="text/javascript" src="../Scripts/js/jquery.js"></script>
				<!-- Footer Scripts
				============================================= -->
				<script type="text/javascript" src="../Scripts/js/functions.js"></script>
			</body>
		</html>