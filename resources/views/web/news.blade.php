<!DOCTYPE html>
<html dir="ltr" lang="en-US">
	<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="author" content="RoyalOrchard" />
		<!-- Stylesheets
		============================================= -->
		<link href="{{URL::asset('Content/font.css')}}" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="{{URL::asset('Content/font-awesome/css/font-awesome.min.css')}}">
		<link rel="stylesheet" href="{{URL::asset('Content/main.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{URL::asset('Content/css/swiper.css')}}" type="text/css" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->
		<!-- Document Title
		============================================= -->
		<title>Royal Orchard</title>
	</head>
	<body class="stretched no-transition">
		<!-- Document Wrapper
		============================================= -->
		<div id="wrapper" class="clearfix">
			<!-- Header============================================= -->
			@extends('layouts.header')
			<!-- #header end -->
			
			<!-- Page Title============================================= -->
			<section id="page-title" >
				<div class="container clearfix">
					<h1>News/Events </h1>
					
				</div>
			</section>
			<!-- #page-title end -->
		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">
				
				<div class="container clearfix">

					<div class="postcontent nobottommargin">

						<div id="posts" class="events small-thumbs">
					@foreach($news as $news)
                             <div class="entry clearfix">
								<div class="entry-image">
								 <a target="_blank" href="{{url('web/news_detail/'.$news->id)}}">
										<img src="{{url('images/news/'.$news->image1)}}" alt="Inventore voluptates velit totam ipsa tenetur">
										<div class="entry-date">{{$news->created_at}}</div>
									</a>
								</div>
								<div class="entry-c">
									<div class="entry-title">
										<h2><a href="#">{{$news->heading}}</a></h2>
									</div>
									<ul class="entry-meta clearfix">
										
										<li><a href="#"><i class="icon-time"></i> 11:00 - 19:00</a></li>
										<li><a href="#"><i class="icon-map-marker2"></i>Royal Orchard Multan,Pakistan</a></li>
									</ul>
									<div class="entry-content">
										<p>{{$news->detail}}</p>
										 <a target="_blank" href="{{url('web/news_detail/'.$news->id)}}" class="btn btn-danger">Read More</a>
									</div>
								</div>
							</div>
						
					@endforeach
						

						</div>

						

					</div>

					<div class="sidebar nobottommargin col_last clearfix">
						<div class="sidebar-widgets-wrap">

							<div class="widget clearfix">

								<h4>Upcoming Events</h4>
								<div id="post-list-footer">

									<div class="spost clearfix">
										<div class="entry-image">
											<a href="#" class="nobg"><img src="canvashtml-cdn.semicolonweb.com/images/events/thumbs/1.html" alt=""></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">No Upcomming Event</a></h4>
											</div>
											<ul class="entry-meta">
												<li>--</li>
											</ul>
										</div>
									</div>

									

									

								</div>

							</div>

							<div class="widget clearfix">

								<h4>Events</h4>
								<div id="oc-portfolio-sidebar" class="owl-carousel carousel-widget" data-items="1" data-margin="10" data-loop="true" data-nav="false" data-autoplay="5000">

									<div class="oc-item">
										<div class="iportfolio">
											<div class="portfolio-image">
												<a href="#">
													<img src="canvashtml-cdn.semicolonweb.com/images/events/thumbs/3.html" alt="Mac Sunglasses">
												</a>
												<div class="portfolio-overlay">
													<a href="http://vimeo.com/89396394" class="center-icon" data-lightbox="iframe"><i class="icon-line-play"></i></a>
												</div>
											</div>
											<div class="portfolio-desc center nobottompadding">
												<h3><a href="portfolio-single-video.html">No Event</a></h3>
												<span><a href="#">No Event</a></span>
											</div>
										</div>
									</div>

									<div class="oc-item">
										<div class="iportfolio">
											<div class="portfolio-image">
												<a href="portfolio-single.html">
													<img src="canvashtml-cdn.semicolonweb.com/images/events/thumbs/1.html" alt="">
												</a>
												<div class="portfolio-overlay">
													<a href="canvashtml-cdn.semicolonweb.com/images/portfolio/full/1.html" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
												</div>
											</div>
											<div class="portfolio-desc center nobottompadding">
												<h3><a href="portfolio-single.html">--</a></h3>
												<span><a href="#">--</a></span>
											</div>
										</div>
									</div>

								</div>


							</div>

						

						</div>
					</div>

				</div>

			</div>

		</section><!-- #content end -->
<!-- Footer============================================= -->
@extends('layouts.footer')
<!-- #footer end -->
</div>
<!-- #wrapper end -->
<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>
<!-- External JavaScripts
============================================= -->
<script type="text/javascript" src="../Scripts/js/jquery.js"></script>
<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="../Scripts/js/functions.js"></script>
</body>
</html>