<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Style -->
    <style>
        .input-box { position: relative; }
        .unit { position: absolute; display: block; left: 5px; top: 10px; z-index: 9; }
        .fixedTxtInput { display: block; border: 1px solid #d7d6d6; background: #fff; padding: 10px 10px 10px 30px; width: 100%; }
    </style>    

        <meta charset="utf-8" />
        <title>Royal Orchard</title>
        <meta name="description" content="overview & stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- basic styles -->
        <link href="{{URL::asset('assets/css/bootstrap.min.css')}} "rel="stylesheet" />
        <link href="{{URL::asset('assets/css/bootstrap-responsive.min.css')}}"  rel="stylesheet" />
        <link rel="stylesheet" href="{{URL::asset('assets/css/font-awesome.min.css')}}" />
        <!--[if IE 7]>
        <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
        <![endif]-->
        <!-- page specific plugin styles -->
        
        <!-- ace styles -->
        <link rel="stylesheet" href="{{URL::asset('assets/css/ace.min.css')}}" />
        <link rel="stylesheet" href="{{URL::asset('assets/css/ace-responsive.min.css')}}" />
        <link rel="stylesheet" href="{{URL::asset('assets/css/ace-skins.min.css')}}" />
        <!--[if lt IE 9]>
        <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
        <![endif]-->
    </head>
    <body>
        <div class="navbar navbar-inverse">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="brand" href="#"><small><i class="icon-leaf"></i> Royal Orchard Admin Panel</small> </a>
                   
                        </div><!--/.container-fluid-->
                        </div><!--/.navbar-inner-->
                        </div><!--/.navbar-->
                        <div class="container-fluid" id="main-container">
                            <a href="#" id="menu-toggler"><span></span></a><!-- menu toggler -->
                            @if(Auth::check())
                            <div id="sidebar">
                                
                                <div id="sidebar-shortcuts">
                                    <div id="sidebar-shortcuts-large">
                                        <button class="btn btn-small btn-success"><i class="icon-signal"></i></button>
                                        <button class="btn btn-small btn-info"><i class="icon-pencil"></i></button>
                                        <button class="btn btn-small btn-warning"><i class="icon-group"></i></button>
                                        <button class="btn btn-small btn-danger"><i class="icon-cogs"></i></button>
                                    </div>
                                    <div id="sidebar-shortcuts-mini">
                                        <span class="btn btn-success"></span>
                                        <span class="btn btn-info"></span>
                                        <span class="btn btn-warning"></span>
                                        <span class="btn btn-danger"></span>
                                    </div>
                                    </div><!-- #sidebar-shortcuts -->
                                    
                                    <ul class="nav nav-list">
                                        
                                        <li class="active">
                                            <a href="{{url('/home')}}">
                                                <i class="icon-dashboard"></i>
                                                <span>Dashboard</span>
                                                
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-toggle" >
                                                <i class="icon-desktop"></i>
                                                <span>Manage Projects</span>
                                                <b class="arrow icon-angle-down"></b>
                                            </a>
                                            <ul class="submenu">
                                                <li><a href="{{url('projects')}}"><i class="icon-double-angle-right"></i> Projects List</a></li>
                                                <li><a href="{{url('project_location')}}"><i class="icon-double-angle-right"></i>Project Location</a></li>
                                                <li><a href="{{url('master_plan')}}"><i class="icon-double-angle-right"></i>Master Plan</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#" class="dropdown-toggle" >
                                                <i class="icon-desktop"></i>
                                                <span>Manage Media</span>
                                                <b class="arrow icon-angle-down"></b>
                                            </a>
                                            <ul class="submenu">
                                                <li><a href="{{url('news')}}"><i class="icon-double-angle-right"></i>Manage News/Events</a></li>
                                                <li><a href="{{url('newsletters')}}"><i class="icon-double-angle-right"></i>Manage Newsletters</a></li>
                                                <li><a href="{{url('slider')}}"><i class="icon-double-angle-right"></i>Manage Slider</a></li>
                                                <li><a href="{{url('gallery')}}"><i class="icon-double-angle-right"></i>Image Gallery</a></li>
                                            </ul>
                                        </li>
                                        
                                        <li>
                                            <a href="#" class="dropdown-toggle" >
                                                <i class="icon-desktop"></i>
                                                <span>Manage Key Feature</span>
                                                <b class="arrow icon-angle-down"></b>
                                            </a>
                                            <ul class="submenu">
                                                <li><a href="{{url('kfcategory')}}"><i class="icon-double-angle-right"></i>Manage Category</a></li>
                                                <li><a href="{{url('kfeature')}}"><i class="icon-double-angle-right"></i>Manage Features</a></li>
                                                 <li><a href="{{url('kfgallery')}}"><i class="icon-double-angle-right"></i>Manage Image Gallery</a></li>
                                                
                                            </ul>
                                        </li>
                                    </li>
                                    <li>
                                        <a href="{{url('sfeature')}}">
                                            <i class="icon-list-alt"></i>
                                            <span>Salient Feature</span>
                                            
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="dropdown-toggle" >
                                            <i class="icon-edit"></i>
                                            <span>Villas/Homes</span>
                                            <b class="arrow icon-angle-down"></b>
                                        </a>
                                        <ul class="submenu">
                                            <li><a href="{{url('homes_category')}}"><i class="icon-double-angle-right"></i>Villa/Home category</a></li>
                                            <li><a href="{{url('villas')}}"><i class="icon-double-angle-right"></i> Villas/Homes List</a></li>
                                            <li><a href="{{url('file')}}"><i class="icon-double-angle-right"></i>File Section</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{url('royal_medicare')}}">
                                            <i class="icon-calendar"></i>
                                            <span>Royal Medicare</span>
                                            
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('payment_plan')}}">
                                            <i class="icon-picture"></i>
                                            <span>Payment Plan</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="dropdown-toggle" >
                                            <i class="icon-edit"></i>
                                            <span>Development Progress</span>
                                            <b class="arrow icon-angle-down"></b>
                                        </a>
                                        <ul class="submenu">
                                            <li><a href="{{url('development_categories')}}"><i class="icon-double-angle-right"></i>Development category</a></li>
                                            <li><a href="{{url('development_progress')}}"><i class="icon-double-angle-right"></i> Development Progress List</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{url('salescenter')}}">
                                            <i class="icon-list-alt"></i>
                                            <span>Sales Center</span>
                                            
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="dropdown-toggle" >
                                            <i class="icon-edit"></i>
                                            <span>Miscellaneous</span>
                                            <b class="arrow icon-angle-down"></b>
                                        </a>
                                        <ul class="submenu">
                                            <li><a href="{{url('setting')}}"><i class="icon-double-angle-right"></i> Manage Setting</a></li>
                                            <li><a href="{{url('size')}}"><i class="icon-double-angle-right"></i> Manage Size</a></li>
                                            <li><a href="{{url('associates')}}"><i class="icon-double-angle-right"></i> Manage Associates</a></li>
                                             <li><a href="{{url('pages')}}"><i class="icon-double-angle-right"></i> Manage Pages</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#" class="dropdown-toggle" >
                                            <i class="icon-file"></i>
                                            <span>Other Pages</span>
                                            <b class="arrow icon-angle-down"></b>
                                        </a>
                                        <ul class="submenu">
                                            <li><a href="pricing.html"><i class="icon-double-angle-right"></i> Pricing Tables</a></li>
                                            <li><a href="invoice.html"><i class="icon-double-angle-right"></i> Invoice</a></li>
                                            <li><a href="login.html"><i class="icon-double-angle-right"></i> Login & Register</a></li>
                                            <li><a href="error-404.html"><i class="icon-double-angle-right"></i> Error 404</a></li>
                                            <li><a href="error-500.html"><i class="icon-double-angle-right"></i> Error 500</a></li>
                                            <li><a href="blank.html"><i class="icon-double-angle-right"></i> Blank Page</a></li>
                                        </ul>
                                    </li>
                                    
                                    </ul><!--/.nav-list-->
                                    
                                    <div id="sidebar-collapse"><i class="icon-double-angle-left"></i></div>
                                    </div><!--/#sidebar-->
                                    
                                    <div id="main-content" class="clearfix">
                                        
                                        <div id="breadcrumbs">
                                            <ul class="breadcrumb">
                                                <li><i class="icon-home"></i> <a href="#">Home</a><span class="divider"><i class="icon-angle-right"></i></span></li>
                                                <li class="active">Dashboard</li>
                                                </ul><!--.breadcrumb-->
                                              @if(Auth::check())
                    <ul class="nav ace-nav pull-right">
                        
                        <li class="light-blue user-profile">
                            <a class="user-menu dropdown-toggle" href="#" data-toggle="dropdown">
                                <img alt="Jason's Photo" src="{{asset('assets/avatars/user.jpg')}}" class="nav-user-photo" />
                                <span id="user_info">
                                    @if(Auth::check())<small>Welcome,</small> {{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}
                                    @endif
                                </span>
                                <i class="icon-caret-down"></i>
                            </a>
                            <ul id="user_menu" class="pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
                               
                               
                                <li><a href="{{ url('/logout') }}"><i class="icon-off"></i> Logout</a></li>
                            </ul>
                        </li>
                        </ul><!--/.ace-nav-->
                        @endif
                                                <!--#nav-search-->
                                                    </div><!--#breadcrumbs-->
                                                    <div id="page-content" class="clearfix">
                                                        
                                                        <div class="page-header position-relative">
                                                            <h1>Dashboard <small><i class="icon-double-angle-right"></i> overview & stats</small></h1>
                                                            </div><!--/page-header-->@endif
                                                            <div id="app">
                                                                
                                                                <main class="py-6">
                                                                    @yield('content')
                                                                </main>
                                                            </div>
                                                            
                                                            
                                                            </div><!--/#page-content-->
                                                            
                                                            
                                                            </div><!-- #main-content -->
                                                            </div><!--/.fluid-container#main-container-->
                                                            <a href="#" id="btn-scroll-up" class="btn btn-small btn-inverse">
                                                                <i class="icon-double-angle-up icon-only"></i>
                                                            </a>
                                                            <!-- basic scripts -->
                                                            <script src="{{asset('http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js')}}"></script>
                                                            <script type="text/javascript">
                                                            window.jQuery || document.write("<script src='{{asset('assets/js/jquery-1.9.1.min.js')}}>\x3C/script>");
                                                            </script>
                                                            
                                                            <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
                                                            <!-- page specific plugin scripts -->
                                                            
                                                            <!--[if lt IE 9]>
                                                            <script type="text/javascript" src="assets/js/excanvas.min.js"></script>
                                                            <![endif]-->
                                                            <script type="text/javascript" src="{{asset('assets/js/jquery-ui-1.10.2.custom.min.js')}}"></script>
                                                            <script type="text/javascript" src="{{asset('assets/js/jquery.ui.touch-punch.min.js')}}"></script>
                                                            <script type="text/javascript" src="{{asset('assets/js/jquery.slimscroll.min.js')}}"></script>
                                                            <script type="text/javascript" src="{{asset('assets/js/jquery.easy-pie-chart.min.js')}}"></script>
                                                            <script type="text/javascript" src="{{asset('assets/js/jquery.sparkline.min.js')}}"></script>
                                                            <script type="text/javascript" src="{{asset('assets/js/jquery.flot.min.js')}}"></script>
                                                            <script type="text/javascript" src="{{asset('assets/js/jquery.flot.pie.min.js')}}"></script>
                                                            <script type="text/javascript" src="{{asset('assets/js/jquery.flot.resize.min.js')}}"></script>
                                                            <!-- ace scripts -->
                                                            <script src="{{asset('assets/js/ace-elements.min.js')}}"></script>
                                                            <script src="{{asset('assets/js/ace.min.js')}}"></script>
                                                            <!-- inline scripts related to this page -->
                                                            
                                                            <script type="text/javascript">
                                                            
                                                            $(function() {
                                                            $('.dialogs,.comments').slimScroll({
                                                            height: '300px'
                                                            });
                                                            
                                                            $('#tasks').sortable();
                                                            $('#tasks').disableSelection();
                                                            $('#tasks input:checkbox').removeAttr('checked').on('click', function(){
                                                            if(this.checked) $(this).closest('li').addClass('selected');
                                                            else $(this).closest('li').removeClass('selected');
                                                            });
                                                            var oldie = $.browser.msie && $.browser.version < 9;
                                                            $('.easy-pie-chart.percentage').each(function(){
                                                            var $box = $(this).closest('.infobox');
                                                            var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
                                                            var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
                                                            var size = parseInt($(this).data('size')) || 50;
                                                            $(this).easyPieChart({
                                                            barColor: barColor,
                                                            trackColor: trackColor,
                                                            scaleColor: false,
                                                            lineCap: 'butt',
                                                            lineWidth: parseInt(size/10),
                                                            animate: oldie ? false : 1000,
                                                            size: size
                                                            });
                                                            })
                                                            $('.sparkline').each(function(){
                                                            var $box = $(this).closest('.infobox');
                                                            var barColor = !$box.hasClass('infobox-dark') ? $box.css('color') : '#FFF';
                                                            $(this).sparkline('html', {tagValuesAttribute:'data-values', type: 'bar', barColor: barColor , chartRangeMin:$(this).data('min') || 0} );
                                                            });
                                                            
                                                            var data = [
                                                            { label: "social networks",  data: 38.7, color: "#68BC31"},
                                                            { label: "search engines",  data: 24.5, color: "#2091CF"},
                                                            { label: "ad campaings",  data: 8.2, color: "#AF4E96"},
                                                            { label: "direct traffic",  data: 18.6, color: "#DA5430"},
                                                            { label: "other",  data: 10, color: "#FEE074"}
                                                            ];
                                                            var placeholder = $('#piechart-placeholder').css({'width':'90%' , 'min-height':'150px'});
                                                            $.plot(placeholder, data, {
                                                            
                                                            series: {
                                                            pie: {
                                                            show: true,
                                                            tilt:0.8,
                                                            highlight: {
                                                            opacity: 0.25
                                                            },
                                                            stroke: {
                                                            color: '#fff',
                                                            width: 2
                                                            },
                                                            startAngle: 2
                                                            
                                                            }
                                                            },
                                                            legend: {
                                                            show: true,
                                                            position: "ne",
                                                            labelBoxBorderColor: null,
                                                            margin:[-30,15]
                                                            }
                                                            ,
                                                            grid: {
                                                            hoverable: true,
                                                            clickable: true
                                                            },
                                                            tooltip: true, //activate tooltip
                                                            tooltipOpts: {
                                                            content: "%s : %y.1",
                                                            shifts: {
                                                            x: -30,
                                                            y: -50
                                                            }
                                                            }
                                                            
                                                            });
                                                            
                                                            var $tooltip = $("<div class='tooltip top in' style='display:none;'><div class='tooltip-inner'></div></div>").appendTo('body');
                                                            placeholder.data('tooltip', $tooltip);
                                                            var previousPoint = null;
                                                            placeholder.on('plothover', function (event, pos, item) {
                                                            if(item) {
                                                            if (previousPoint != item.seriesIndex) {
                                                            previousPoint = item.seriesIndex;
                                                            var tip = item.series['label'] + " : " + item.series['percent']+'%';
                                                            $(this).data('tooltip').show().children(0).text(tip);
                                                            }
                                                            $(this).data('tooltip').css({top:pos.pageY + 10, left:pos.pageX + 10});
                                                            } else {
                                                            $(this).data('tooltip').hide();
                                                            previousPoint = null;
                                                            }
                                                            
                                                            });
                                                            var d1 = [];
                                                            for (var i = 0; i < Math.PI * 2; i += 0.5) {
                                                            d1.push([i, Math.sin(i)]);
                                                            }
                                                            var d2 = [];
                                                            for (var i = 0; i < Math.PI * 2; i += 0.5) {
                                                            d2.push([i, Math.cos(i)]);
                                                            }
                                                            var d3 = [];
                                                            for (var i = 0; i < Math.PI * 2; i += 0.2) {
                                                            d3.push([i, Math.tan(i)]);
                                                            }
                                                            
                                                            var sales_charts = $('#sales-charts').css({'width':'100%' , 'height':'220px'});
                                                            $.plot("#sales-charts", [
                                                            { label: "Domains", data: d1 },
                                                            { label: "Hosting", data: d2 },
                                                            { label: "Services", data: d3 }
                                                            ], {
                                                            hoverable: true,
                                                            shadowSize: 0,
                                                            series: {
                                                            lines: { show: true },
                                                            points: { show: true }
                                                            },
                                                            xaxis: {
                                                            tickLength: 0
                                                            },
                                                            yaxis: {
                                                            ticks: 10,
                                                            min: -2,
                                                            max: 2,
                                                            tickDecimals: 3
                                                            },
                                                            grid: {
                                                            backgroundColor: { colors: [ "#fff", "#fff" ] },
                                                            borderWidth: 1,
                                                            borderColor:'#555'
                                                            }
                                                            });
                                                            $('[data-rel="tooltip"]').tooltip();
                                                            })
                                                            </script>
                                                            
                                                        </body>
                                                    </html>