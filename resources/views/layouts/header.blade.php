<!-- Header
============================================= -->
<header id="header" class="transparent-header full-header" data-sticky-class="not-dark">
	<div id="header-wrap">
	<div id="top-bar">
		
		<div class="container clearfix">
			<div class="col_one&quot;">
				<!-- Top Social============================================= -->
				<div id="top-social">
					<ul>
						@php $setting = DB::table('settings')
						->get();
						@endphp
						@foreach($setting as $setting1)
						
						@if($setting1->name=='UAN')
						<li>&nbsp;&nbsp;&nbsp;UAN:&nbsp;111 444 475&nbsp;&nbsp;&nbsp;</li>@endif
						@if($setting1->name=='Toll Free')
						<li> &nbsp; Toll Free&nbsp;: &nbsp; 0800 &nbsp;(ROYAL) &nbsp;76925</li>
						@endif
						@endforeach
					</ul>
					</div><!-- #top-social end -->
				</div>
				<div class="col_half fright col_last nobottommargin">
					<!-- Top Social
					============================================= -->
					<div id="top-social"><ul>
						@foreach($setting as $setting)
								@if($setting->name=='Member Login')
						<li>
							<a target="_blank" href="{{$setting->value}}" class="si-facebook" style="width: 40px;" data-hover-width="136"><span class="ts-icon"><i class="fa fa-user"></i></span><span class="ts-text"></span></a>
						</li>
						@endif		
						@if($setting->name=='Facebook Page Link')
						<li><a target="_blank" href="{{$setting->value}}" class="si-facebook" style="width: 40px;" data-hover-width="109"><span class="ts-icon"><i class="fa fa-facebook"></i></span><span class="ts-text">Facebook</span></a></li>@endif
						@if ($setting->name=='Twitter Page Link')
						<li><a target="_blank" href="{{$setting->value}}" class="si-twitter" style="width: 40px;" data-hover-width="95"><span class="ts-icon"><i class="fa fa-twitter"></i></span><span class="ts-text">Twitter</span></a></li>
						@endif
						@if ($setting->name=='Youtube Channel Link')
						<li><a target="_blank" href="{{$setting->value}}" class="si-dribbble" style="width: 40px;" data-hover-width="103"><span class="ts-icon"><i class="fa fa-youtube"></i></span><span class="ts-text">Youtube</span></a></li>@endif
						@if($setting->name=='Email/Mail To')
						<li><a href="{{$setting->value}}" class="si-email3" style="width: 40px;" data-hover-width="153"><span class="ts-icon"><i class="fa fa-envelope"></i></span><span class="ts-text">sales@royalorchard.pk</span></a></li>
						@endif
						
						<!-- #top-social end -->@endforeach</ul></div>
					</div>
								
				</div></div>
				
					<div class="container clearfix">
						<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
						<div id="logo">
							<a href="../index.html" class="standard-logo" data-dark-logo="/Images/logo/ro-logo.png"><img   src="http://localhost/royalorchard/images/logo/ro-logo.png"alt="Royal Orchard Logo"></a>
							<a href="../index.html" class="retina-logo" data-dark-logo="/Images/logo/ro-logo.png"><img " src=".http://localhost/royalorchard/images/Images/logo/ro-logo.png" alt="Royal Orchard Logo"></a>
						</div>
						<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
						<!-- Primary Navigation
						============================================= -->
						<nav id="primary-menu" class="dark">
							<ul>
								<li class="current"><a href="{{url('/web')}}"><div>Home</div></a>
								
							</li>
							<li class="current"><a href="#"><div>About US</div></a>
							<ul>
								<li><a href="{{url('/web/mission')}}"><div>Mission & Vision</div></a>
								<li><a href="{{url('/web/objective')}}"><div>Objectives</div></a>
								<li><a href="#"><div>Certifications</div></a>
								
							</li>
							
							
						</ul>
					</li>
					
					<li class="current"><a href="../Multan.html"><div>Current Projects</div></a>
					<ul>
						<li><a href="{{url('/web/multan')}}"><div>Royal Orchard Multan</div></a>
						<li><a href="{{url('/web/sargodha')}}"><div>Royal Orchard Sargodha</div></a>
						<li><a href="{{url('/web/sahiwal')}}"><div>Royal Orchard Sahiwal</div></a>
						
					</li>
					
					
				</ul>
			</li>
			<li class="current"><a href="#"><div>Upcoming Projects</div></a>
			<ul>
				<li><a href="#"><div>Royal Orchard Peshawer</div></a>
				<li><a href="#"><div>Royal Orchard Rahim Yar Khan</div></a>
				<li><a href="#"><div>Royal Orchard Mandi Bahuddin </div></a>
				<li><a href="#"><div>Royal Orchard Gujrat </div></a>
				<li><a href="#"><div>Royal Orchard DG Khan</div></a>
				<li><a href="#"><div>Royal Orchard Islamabad</div></a>
				<li><a href="#"><div>Royal Orchard Lahore</div></a>
				<li><a href="#"><div>Royal Orchard Karachi</div></a>
			</li>
			
			
		</ul>
	</li>
	<li class="current"><a href="#"><div>Media Center</div></a>
	<ul>
		<li><a href="{{url('/web/brouchers')}}"><div>Brouchers</div></a>
		<li><a href="{{url('/web/newsletters')}}"><div>Newsletter Archive</div></a>
		<li><a href="{{url('/web/news')}}"><div>News & Events</div></a>
		
	</li>
	
	
</ul>
</li>
<li class="current"><a href="{{url('/web/contactus')}}"><div>Contact Us</div></a>
</li>
</ul>
<!-- Top Cart
============================================= -->
<!-- Top Search
============================================= -->
</nav><!-- #primary-menu end -->
</div>
</div>
</header><!-- #header end -->
