<footer id="footer" class="dark">
	<div class="container">
		<!-- Footer Widgets
		============================================= -->
		<div class="footer-widgets-wrap clearfix">
			<div class="col_one_third">
				<div class="widget clearfix">
					<img src="http://localhost/royalorchard/images/logo/ro-logo.png" alt="" class="footer-logo">
					
					<div style="background: url('Images/world-map.png') no-repeat center center; background-size: 100%;">
						
						<address>
							<strong>Central Office:</strong><br>
							Plaza #11, Jinnah Boulevard Sector-A, Near Gate #1<br>
							DHA Phase II, G.T.Road, Islamabad<br>
						</address>
						
					<abbr title="Phone"><strong>Phone:</strong></abbr>+92 51 5419180-82
					<br>
				<abbr title="Email Address"><strong>Email:</strong></abbr>
				sales@royalorchard.pk <br>
			<abbr title="UAN"><strong>UAN:</strong></abbr>&nbsp;&nbsp;&nbsp;111 444 475 &nbsp;(With Area Code)  <br>
		<abbr title="Toll Free"><strong>Toll Free:</strong></abbr> &nbsp;&nbsp;&nbsp;0800 (ROYAL) 76925
	</div>
</div>
</div>

<div class="col_one_third">
<div class="widget clearfix">
	<div class="">
		<div class="widget subscribe-widget clearfix">
			<h5><strong>Get in touch with us:</strong></h5>
		</div>
	</div>
</div>
<div class="widget clearfix">
	
	<a href="https://www.facebook.com/royalorchardpk" class="social-icon si-light si-rounded si-facebook">
		<i class="icon-facebook"></i>
		<i class="icon-facebook"></i>
	</a>
	<a href="https://twitter.com/rdblpk" class="social-icon si-light si-rounded si-facebook">
		<i class="icon-twitter"></i>
		<i class="icon-twitter"></i>
	</a>
	<a href="mailto:sales@royalorchard.pk" class="social-icon si-light si-rounded si-facebook">
		<i class="icon-envelope"></i>
		<i class="icon-envelope"></i>
	</a>
	
	<a href="https://www.youtube.com/channel/UCNPoa74LguQ_B68bugKW-pA" class="social-icon si-light si-rounded si-facebook">
		<i class="icon-youtube"></i>
		<i class="icon-youtube"></i>
	</a>
	
	
	
</div>
</div>
<div class="col_one_third col_last">
<div class="widget quick-contact-widget clearfix">
	<h4>Send Message</h4>
	<div class="quick-contact-form-result"></div>
	<form id="quick-contact-form" name="quick-contact-form"  action="{{url('/sendmail')}}" method="post" enctype="multipart/form-data" class="quick-contact-form nobottommargin" novalidate="novalidate">
		<div class="form-process"></div>
		<div class="input-group divcenter">
			<span class="input-group-addon"><i class="fa fa-user"></i></span>
			<input class="required form-control input-block-level" id="quick-contact-form-name" name="name" value="" placeholder="Full Name" aria-required="true" type="text">
		</div>
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div class="input-group divcenter">
			<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
			<input class="required form-control email input-block-level" id="quick-contact-form-email" name="email" value="" placeholder="Email Address" aria-required="true" type="text">
		</div>
		<textarea class="required form-control input-block-level short-textarea" id="quick-contact-form-message" name="message" rows="4" cols="30" placeholder="Message" aria-required="true"></textarea>
		
		<script src='https://www.google.com/recaptcha/api.js'></script>
		<input class="hidden" id="quick-contact-form-botcheck" name="quick-contact-form-botcheck" value="" type="text">
		<div class="form-group">
			<div class="col-sm-4">
				<button type="submit" id="quick-contact-form-submit" name="quick-contact-form-submit" class="btn btn-danger nomargin" value="submit">Send Email</button>
			</div>
			 <div class="col-md-4"><div class="g-recaptcha"
			data-sitekey="6LetCcUUAAAAAOR_IV7pzTt6wGN6W7uRc6DFjAfJ"></div>
		</div> 
	</div>
	
</form>
</div>
</div>
</div>
<!-- .footer-widgets-wrap end -->
</div>
<!-- Copyrights
============================================= -->
<div id="copyrights">
<div class="container clearfix">
<div class="col_half">
Copyrights &copy; 2017 All Rights Reserved by HRL Development Team.
</div>
<div class="col_half col_last tright">
<div class="fright clearfix">
<div class="copyrights-menu copyright-links nobottommargin">
	<a target="_self" href="{{url('/web')}}">Home</a>/
	<a target="_self" href="{{url('/web/mission')}}">Mission/Vision</a>/
	<a target="_self" href="{{url('/web/objective')}}">Objective</a>/
	
	<a target="_self" href="{{url('/web/contactus')}}">Contact</a>
</div>
</div>
</div>
</div>
</div>
<!-- #copyrights end -->
</footer>