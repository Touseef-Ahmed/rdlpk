@extends('layouts.app')
@section('title',' List')
@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New User</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{route('userData.index')}}"> Back</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="row-fluid">
        <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
    <label for="email">Name:</label>
    <input type="text" class="autosize-transition span6" id="name" placeholder="Enter Name" name="name">
  </div>
  <div class="row-fluid">
    <label for="email">Email:</label>
    <input type="email" class="autosize-transition span6" id="email" placeholder="Enter Email" name="email">
  </div>
  <div class="row-fluid">
    <label for="email">Phone:</label>
    <input type="text" class="autosize-transition span6" id="phone" placeholder="Enter Phone" name="phone">
  </div>
  <div class="row-fluid">
    <label for="email">City:</label>
    <input type="text" class="autosize-transition span6" id="city" placeholder="Enter City" name="city">
  </div>
  <button type="submit" class="btn btn-primary" id="butsave">Submit</button>
</div>
<script>

$(document).ready(function() {
   
    $('#butsave').on('click', function() {
      var name = $('#name').val();
      var email = $('#email').val();
      var phone = $('#phone').val();
      var city = $('#city').val();
      var password = $('#password').val();
      if(name!="" && email!="" && phone!="" && city!=""){
        //   $("#butsave").attr("disabled", "disabled");
          $.ajax({
            type: "POST",
              // url: "/userData",
                 url: "{{URL('/userData')}}",
              
              data: {
                  _token: $("#csrf").val(),
                  type: 1,
                  name: name,
                  email: email,
                  phone: phone,
                  city: city
              },
              cache: false,
              success: function(dataResult){
                  console.log(dataResult);
                  var dataResult = JSON.parse(dataResult);
                  if(dataResult.statusCode==200){
                    window.location = "{{URL('/userData/')}}";              
                  }
                  else if(dataResult.statusCode==201){
                     alert("Error occured !");
                  }
                  
              }
          });
      }
      else{
          alert('Please fill all the field !');
      }
  });
});
</script>

                      
  @endsection