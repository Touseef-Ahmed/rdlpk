@extends('layouts.app')
@section('title','Edit News')
@section('content')
<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit User</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{route('userData.index')}}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    {{ Form::open(array('method'=>'PUT','route' => ['userData.update', $userData->id],'files' =>true,'enctype'=>'multipart/form-data')) }}
        {{-- {{ csrf_field() }}
        {{ method_field('PATCH') }} --}}
   
       <div class="row-fluid">
        <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
    <label for="email">Name:</label>
    <input type="text" id="name" name="name" value="{{ $userData->name }}" class="autosize-transition span6" placeholder="Name">
  </div>
           <div class="row-fluid">
    <label for="email">Email ID:</label>
    <input type="text" class="autosize-transition span6" id="email" name="email" value ="{{ $userData->email }}" placeholder="Email ID">
  </div>
            
             <div class="row-fluid">
    <label for="email">Phone:</label>
   <input type="text" class="autosize-transition span6" id ="phone" name="phone" value ="{{ $userData->phone }}" placeholder="Phone Number">
  </div>
           
           
             <div class="row-fluid">
    <label for="email">City:</label>
   <input type="text" class="autosize-transition span6" id = "city" name="city" value ="{{ $userData->city }}" placeholder="city">
   <input type="hidden" class="autosize-transition span6" id = "type" name="type" value ="{{ $userData->type }}" placeholder="city">
            <!-- -->
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button class="btn btn-primary" id="update_data" value="{{ $userData->id }}">Submit</button>
            </div>
      
   
    {{-- </form> --}}
    <script>
        $(document).ready(function(){

    $(document).on("click", "#update_data", function() { 
         //var url = "{{URL('userData/update/'.$userData->id)}}";


        var id= 
		$.ajax({
			url:"{{url('/userData/')}}",
			type: "PATCH",
			cache: false,
			data:{
                _token:'{{ csrf_token() }}',
				type: 3,
				name: $('#name').val(),
				email: $('#email').val(),
				phone: $('#phone').val(),
				city: $('#city').val()
			},
			success: function(dataResult){
                dataResult = JSON.parse(dataResult);
             if(dataResult.statusCode)
             {
                window.location = "{{url('/userData/')}}";
             }
             else{
                 alert("Internal Server Error");
             }
				
			}
		});
	}); 
});

    </script>
		@endsection