@extends('layouts.app')
@section('title','Edit gallery')
@section('content')
<div class="row-fluid">
	@if(count($errors)>0)
	<ul>
		
		<div class="note note-success"> @foreach($errors->all() as $error)
			<strong style="color: red"> {{  $error }}</strong><br>
			@endforeach
		</div>
		
	</ul>
	@endif
	<div class="span12">
		
		<div class="widget-box">
			<div class="widget-header">
				<h4>Edit gallery</h4>
				<span class="widget-toolbar">
				</span>	</div>
				<div class="widget-body"><div class="widget-body-inner">
					<div class="widget-main">
					{{ Form::open(array('method'=>'PUT','route' => ['gallery.update', $gallery->id],'files' =>true,'enctype'=>'multipart/form-data')) }}

							<div class="row-fluid">
								  <input type="hidden" name="noimg" value="No-Image" placeholder="">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<label for="form-field-8">Gallery Title</label>
								<input name="title" value="{{$gallery->title}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter gallery title">
							</div>
							<hr>
							<div class="row-fluid">
								<label for="form-field-9">Project Name</label>
								<select  name="project_id" class="span6" id="form-field-select-1">
									
									<option selected value="{{$gallery->project->id}}">{{$gallery->project->project_name}}</option>
									@foreach($projects as $project)
									<option value="{{$project->id}}">{{$project->project_name}}</option>
									@endforeach
								</select>
							</div>	
							<hr>
							<table>
								<tbody>
									<tr>
									<td><div class="row-fluid">
								<label for="form-field-8">gallery Image 1</label>
								<img src="{{url('/images/gallery/'.$gallery->image1)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image1" value="" placeholder="">
							</div></td>
							<td><div class="row-fluid">
								<label for="form-field-8">gallery Image 2</label>
								<img src="{{url('/images/gallery/'.$gallery->image2)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image2" value="" placeholder="">
							</div></td>
							<td>	
							<div class="row-fluid">
								<label for="form-field-8">gallery Image 3</label>
								<img src="{{url('/images/gallery/'.$gallery->image3)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image3" value="" placeholder="">
							</div></td>
								<td><div class="row-fluid">
								<label for="form-field-8">gallery Image 4</label>
								<img src="{{url('/images/gallery/'.$gallery->image4)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image4" value="" placeholder="">
							</div></td>
								<td><div class="row-fluid">
								<label for="form-field-8">gallery Image 5</label>
								@if(empty($gallery->image5))
								no-image.png
								@else
								<img src="{{url('/images/gallery/'.$gallery->image5)}}" width="100px"  alt="No Image"><br>
								@endif
								<input type="file" name="image5" value="" placeholder="">
							</div></td>
									</tr>
									<tr><td style="height: 50px;" colspan="10"></td></tr>
									<tr>
										<td><div class="row-fluid">
								<label for="form-field-8">gallery Image 6</label>
								<img src="{{url('/images/gallery/'.$gallery->image6)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image6" value="" placeholder="">
							</div></td>
										<td><div class="row-fluid">
								<label for="form-field-8">gallery Image 7</label>
								<img src="{{url('/images/gallery/'.$gallery->image7)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image7" value="" placeholder="">
							</div></td>
										<td><div class="row-fluid">
								<label for="form-field-8">gallery Image 8</label>
								<img src="{{url('/images/gallery/'.$gallery->image8)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image8" value="" placeholder="">
							</div></td>
										<td><div class="row-fluid">
								<label for="form-field-8">gallery Image 9</label>
								<img src="{{url('/images/gallery/'.$gallery->image9)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image9" value="" placeholder="">
							</div></td>
										<td><div class="row-fluid">
								<label for="form-field-8">gallery Image 10</label>
								<img src="{{url('/images/gallery/'.$gallery->image10)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image10" value="" placeholder="">
							</div></td>


									</tr>
								</tbody>
							</table>
							
						
						
							<div class="form-actions">
								<button class="btn btn-info" type="submit"><i class="icon-ok"></i> Submit</button>
								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset"><i class="icon-undo"></i> Reset</button>
							</div> </form>
						</div>
					</div></div>
				</div>
			</div>
			
		</div>
		@endsection