@extends('layouts.app')
@section('title',' List')
@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add Slider Image</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{route('userData.index')}}"> Back</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form method="post" id="upload_form" enctype="multipart/form-data">
	<div class="alert" id="message" style="display: none"></div>
<div class="row-fluid">
        <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
    <label for="title">Title:</label>
    <input type="text" class="autosize-transition span6" id="title" placeholder="Enter Name" name="title">
  </div>
 	<div class="row-fluid">
								<label for="form-field-9">Status</label>
								<select  name="status" class="span6" id="status">
									
									<option selected value="">Select Status</option>
									
									<option value="1">Enabled</option>
									<option value="0">Disabled</option>
								
								</select>
							</div>
									
  <div class="row-fluid">
    <label for="email">Image:</label>
    <input type="file" class="autosize-transition span6" id="image" name="image">
  </div>
 <hr>
  <button type="submit" class="btn btn-primary" id="butsave">Submit</button>
</div></form>
<script>
$(document).ready(function(){

 $('#upload_form').on('submit', function(event){
  event.preventDefault();
  $.ajax({
   url:"{{ route('slider.store') }}",
   method:"POST",
   data:new FormData(this),
   dataType:'JSON',
   contentType: false,
   cache: false,
   processData: false,
   success:function(data)
   {
    $('#message').css('display', 'block');
    $('#message').html(data.message);
    $('#message').addClass(data.class_name);
    $('#uploaded_image').html(data.uploaded_image);
    window.location = "{{URL('/slider/')}}";    

   }
  })
 });

});
</script>
                      
  @endsection