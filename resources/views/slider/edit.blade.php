@extends('layouts.app')
@section('title','Edit News')
@section('content')
<div class="row-fluid">
	@if(count($errors)>0)
	<ul>
		
		<div class="note note-success"> @foreach($errors->all() as $error)
			<strong style="color: red"> {{  $error }}</strong><br>
			@endforeach
		</div>
		
	</ul>
	@endif
	<div class="span12">
		
		<div class="widget-box">
			<div class="widget-header">
				<h4>Edit Slider</h4>
				<span class="widget-toolbar">
				</span>	</div>
				<div class="widget-body"><div class="widget-body-inner">
					<div class="widget-main">
					{{ Form::open(array('method'=>'PUT','route' => ['slider.update', $slider->id],'files' =>true,'enctype'=>'multipart/form-data')) }}

							<div class="row-fluid">
								  <input type="hidden" name="noimg" value="No-Image" placeholder="">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<label for="form-field-8">Title</label>
								<input name="title" value="{{$slider->title}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Newsletters">
							</div>
							
							<hr>
							<div class="row-fluid">
								<label for="form-field-9">Status</label>
								<select  name="status" class="span6" id="form-field-select-1">		
									<option selected value="{{$slider->status}}">
									@if($slider->status==1)
									{{'Enabled'}}@else{{'Disabled'}}@endif	</option>
									<option value="1">Enabled</option>
									<option value="0">Disabled</option>
								</select>
							</div>
							<div class="row-fluid">
								<label for="form-field-8">Image</label>
								<img src="{{url('/images/slider/'.$slider->image)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image" value="" placeholder="">
							</div><hr>
						
						
							<div class="form-actions">
								<button class="btn btn-info" type="submit"><i class="icon-ok"></i> Submit</button>
								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset"><i class="icon-undo"></i> Reset</button>
							</div> </form>
						</div>
					</div></div>
				</div>
			</div>
			
		</div>
		@endsection