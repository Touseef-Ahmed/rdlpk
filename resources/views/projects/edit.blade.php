@extends('layouts.app')
@section('title','Projects List')
@section('content')
<div class="row-fluid">
	 @if(count($errors)>0)
  <ul>
   
    <div class="note note-success"> @foreach($errors->all() as $error)
     <strong style="color: red"> {{  $error }}</strong><br>
        @endforeach
    </div>
  
  </ul>
  @endif
			<div class="span12">
				
			<div class="widget-box">
				<div class="widget-header">
					<h4>Edit Project</h4>
					<span class="widget-toolbar">
					</span>	</div>
				<div class="widget-body"><div class="widget-body-inner">
				 <div class="widget-main">
				
				  {!! Form::open(['route'=>['projects.update',$project->id], 'class'=>'form-horizontal','method' => 'PATCH','enctype'=>'multipart/form-data']) !!}
					<div class="row-fluid">
						 <input type="hidden" name="_token" value="{{csrf_token()}}">
						   <input type="hidden" name="id" value="{{$project->id}}">
						<label for="form-field-8">Project Name</label>
						<input name="project_name" value="{{$project->project_name}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Project Name">
					</div>
					<hr>
					<div class="row-fluid">
						<label for="form-field-9">Project Code</label>
						<input value="{{$project->project_code}}"  name="project_code" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Project Code">
					</div>
					<div class="row-fluid">
						<label for="form-field-9">Project Status</label>
						<select class="span6" name="project_status" id="form-field-select-1">
							   <option value="{{ $project->project_status }}" @if($project->project_status==old('project_status')) selected @endif>{{ $project->project_status }}</option>
                      <option value="">Select Project Status</option>
                      <option value="Current">Current</option>
                      <option value="Upcomming">Upcomming</option>						</select>
					</div>
					<div class="row-fluid">
						<label for="form-field-9">Project Detail</label>
						<textarea class="span6" name="project_detail" class="span12 limited" id="form-field-9" >{{$project->project_detail}}</textarea>
					</div>
					<hr>
					<div class="row-fluid">
						<label for="form-field-11">Project Location</label>
						<textarea name="project_location" id="form-field-11" class="autosize-transition span6" style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 58px;">{{$project->project_location}}</textarea>
					</div>
					<div class="row-fluid">
						<label for="form-field-11">Project Attrraction</label>
						<textarea name="project_attraction" id="form-field-11" class="autosize-transition span6" style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 58px;">{{$project->project_attraction}}</textarea>
					</div>
					<div class="row-fluid">
						<label for="form-field-11">Project Type</label>
						<textarea name="project_type" id="form-field-11" class="autosize-transition span6" style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 58px;">{{$project->project_type}}</textarea>
					</div>
					<div class="row-fluid">
								<label for="form-field-8">Main Image</label>
								<img src="{{url('/images/projects/'.$project->imagemain)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="imagemain" value="" placeholder="">
							</div>
					<div class="row-fluid">
								<label for="form-field-8">Top Image</label>
								<img src="{{url('/images/projects/'.$project->imagetop)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="imagetop" value="" placeholder="">
							</div>
					<div class="row-fluid">
								<label for="form-field-8">Bottom Image</label>
								<img src="{{url('/images/projects/'.$project->imagebottom)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="imagebottom" value="" placeholder="">
							</div>

					<hr>
					<div class="form-actions">
			<button class="btn btn-info" type="submit"><i class="icon-ok"></i> Submit</button>
			&nbsp; &nbsp; &nbsp;
			<button class="btn" type="reset"><i class="icon-undo"></i> Reset</button>
		</div> </form>
				 </div>
				</div></div>
			</div>
		  </div>
			
		</div>
@endsection