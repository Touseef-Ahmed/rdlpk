
@extends('layouts.app')
@section('title','Projects List')
@section('content')
<div class="row-fluid">
	 @if(count($errors)>0)
  <ul>
   
    <div class="note note-success"> @foreach($errors->all() as $error)
     <strong style="color: red"> {{  $error }}</strong><br>
        @endforeach
    </div>
  
  </ul>
  @endif
			<div class="span12">
				
			<div class="widget-box">
				<div class="widget-header">
					<h4>Create New Project</h4>
					<span class="widget-toolbar">
					</span>	</div>
				<div class="widget-body"><div class="widget-body-inner">
				 <div class="widget-main">
				 <form   action="{{url('/projects')}}" enctype="multipart/form-data" method="post" accept-charset="utf-8">
					<div class="row-fluid">
						 <input type="hidden" name="_token" value="{{csrf_token()}}">
						<label for="form-field-8">Project Name</label>
						<input name="project_name" value="{{old('project_name')}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Project Name">
					</div>
					<hr>
					<div class="row-fluid">
						<label for="form-field-9">Project Code</label>
						<input value="{{old('project_code')}}"  name="project_code" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Project Code">
					</div>
					<div class="row-fluid">
						<label for="form-field-9">Project Status</label>
						<select class="span6" name="project_status" id="form-field-select-1">
							  <option value="">Select Status</option>
							  <option value="Current">Current </option><option value="Upcoming">Upcoming</option>						</select>
					</div>
					<div class="row-fluid">
						<label for="form-field-9">Project Detail</label>
						<textarea class="autosize-transition span6" id="summary-ckeditor" name="project_detail">{{old('project_detail') }}</textarea>
					</div>
					<hr>
					<div class="row-fluid">
						<label for="form-field-11">Project Location</label>
						<textarea name="project_location" id="form-field-11" class="autosize-transition span6" style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 58px;">{{old('project_location')}}</textarea>
					</div>
					<div class="row-fluid">
						<label for="form-field-11">Project Attrraction</label>
						<textarea name="project_attraction" id="form-field-11" class="autosize-transition span6" style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 58px;">{{old('project_attraction')}}</textarea>
					</div>
					<div class="row-fluid">
						<label for="form-field-11">Project Type</label>
						<textarea name="project_type" id="form-field-11" class="autosize-transition span6" style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 58px;">{{old('project_type')}}</textarea>
					</div>
					<hr>
							
							<div class="row-fluid">	
								<label for="form-field-8">Main Image</label>
								 <input type="file" name="imagemain" value="" placeholder="Select Images " >
							</div>
							<hr>
							
							<div class="row-fluid">	
								<label for="form-field-8">Top Image</label>
								 <input type="file" name="imagetop" value="" placeholder="Select Images " >
							</div>
							<hr>
							
							<div class="row-fluid">	
								<label for="form-field-8">Bottom Image</label>
								 <input type="file" name="imagebottom" value="" placeholder="Select Images " >
							</div>

					<div class="form-actions">
			<button class="btn btn-info" type="submit"><i class="icon-ok"></i> Submit</button>
			&nbsp; &nbsp; &nbsp;
			<button class="btn" type="reset"><i class="icon-undo"></i> Reset</button>
		</div> </form>
				 </div>
				</div></div>
			</div>
		  </div>
			<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace( 'summary-ckeditor' );
</script>
		</div>
@endsection
