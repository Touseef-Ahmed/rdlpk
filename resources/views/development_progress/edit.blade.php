@extends('layouts.app')
@section('title','Edit News')
@section('content')
<div class="row-fluid">
	@if(count($errors)>0)
	<ul>
		
		<div class="note note-success"> @foreach($errors->all() as $error)
			<strong style="color: red"> {{  $error }}</strong><br>
			@endforeach
		</div>
		
	</ul>
	@endif
	<div class="span12">
		
		<div class="widget-box">
			<div class="widget-header">
				<h4>Edit Development Progress</h4>
				<span class="widget-toolbar">
				</span>	</div>
				<div class="widget-body"><div class="widget-body-inner">
					<div class="widget-main">
					{{ Form::open(array('method'=>'PUT','route' => ['development_progress.update', $development_progress->id],'files' =>true,'enctype'=>'multipart/form-data')) }}

							<div class="row-fluid">
								<label for="form-field-9">Category Name</label>
								<select  name="category_id" class="span6" id="form-field-select-1">
									
									<option selected value="{{$development_progress->project->id}}">{{$development_progress->category->name}}</option>
									@foreach($category as $category)
									<option value="{{$category->id}}">{{$category->name}}</option>
									@endforeach
								</select>
							</div>
							<hr>
							<div class="row-fluid">
								<label for="form-field-9">Project Name</label>
								<select  name="project_id" class="span6" id="form-field-select-1">
									
									<option selected value="{{$development_progress->project->id}}">{{$development_progress->project->project_name}}</option>
									@foreach($projects as $project)
									<option value="{{$project->id}}">{{$project->project_name}}</option>
									@endforeach
								</select>
							</div>
							<div class="row-fluid">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<label for="form-field-8">Video Link 1</label>
								<input name="link1" value="{{$development_progress->link1}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Video Link">
							</div>
							<hr>
							
							<div class="row-fluid">
								<label for="form-field-11">Video Link 2</label>
								<input name="link2" value="{{$development_progress->link2}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Video Link">
							</div>
							<table>
								
								<tbody>
									<tr>
										<td><div class="row-fluid">
								<label for="form-field-8">Image 1</label>
								<img src="{{url('/images/development_progress/'.$development_progress->image1)}}" width="150px"  alt="No Image"><br>
								<input type="file" name="image1" value="" placeholder="">
							</div></td>
							<td><div class="row-fluid">
								<label for="form-field-8">Image 2</label>
								<img src="{{url('/images/development_progress/'.$development_progress->image2)}}" width="150px"  alt="No Image"><br>
								<input type="file" name="image2" value="" placeholder="">
							</div></td>
							<td>	
							<div class="row-fluid">
								<label for="form-field-8">Image 3</label>
								<img src="{{url('/images/development_progress/'.$development_progress->image3)}}" width="150px"  alt="No Image"><br>
								<input type="file" name="image3" value="" placeholder="">
							</div></td>
								<td><div class="row-fluid">
								<label for="form-field-8">Image 4</label>
								<img src="{{url('/images/development_progress/'.$development_progress->image4)}}" width="150px"  alt="No Image"><br>
								<input type="file" name="image4" value="" placeholder="">
							</div></td>
								<td><div class="row-fluid">
								<label for="form-field-8">Image 5</label>
								@if(empty($development_progress->image5))
								no-image.png
								@else
								<img src="{{url('/images/development_progress/'.$development_progress->image5)}}" width="150px"  alt="No Image"><br>
								@endif
								<input type="file" name="image5" value="" placeholder="">
							</div></td>
									</tr>
									<tr><td style="height: 50px;" colspan="10"></td></tr>
									<tr>
										<td><div class="row-fluid">
								<label for="form-field-8">Image 6</label>
								<img src="{{url('/images/development_progress/'.$development_progress->image6)}}" width="150px"  alt="No Image"><br>
								<input type="file" name="image6" value="" placeholder="">
							</div></td>
									


									</tr>
								</tbody>
							</table>
							
						
						
							<div class="form-actions">
								<button class="btn btn-info" type="submit"><i class="icon-ok"></i> Submit</button>
								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset"><i class="icon-undo"></i> Reset</button>
							</div> </form>
						</div>
					</div></div>
				</div>
			</div>
			
		</div>
		@endsection