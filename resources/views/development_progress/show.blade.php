@extends('layouts.app')
@section('title','Projects List')
@section('content')<button class="btn-link" style="float: right">  <a href="{{url('development_progress/create')}}">Create New Progress</a></button><br><br>
<div class="row-fluid">
	<!-- <h3 class="header smaller lighter blue">jQuery dataTables</h3> -->
	
	<div class="table-header">
		News Detail 
	</div>
	  @if(session('message'))
        <div class="note note-success"><p>{{session('message')}}</p> </div>
        @endif
		<div id="table_report_wrapper" class="dataTables_wrapper" role="grid"><div class="row-fluid"><div class="span6"><div id="table_report_length" class="dataTables_length"><label>Display <select size="1" name="table_report_length" aria-controls="table_report"><option value="10" selected="selected">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> records</label></div></div><div class="span6"><div class="dataTables_filter" id="table_report_filter"><label>Search: <input type="text" aria-controls="table_report"></label></div></div></div><table id="table_report" class="table table-striped table-bordered table-hover dataTable" aria-describedby="table_report_info">
			<thead>
				<tr role="row"><th class="center sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="
						
					" style="width: 71px;">
						<label><input type="checkbox"><span class="lbl"></span></label>
					</th><th class="sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Domain: activate to sort column ascending" style="width: 307px;">Category</th><th class="sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Price: activate to sort column ascending" style="width: 240px;">Project Name</th><th class="hidden-480 sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Clicks: activate to sort column ascending" style="width: 151px;">Image 1</th><th class="hidden-480 sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 150px;">Image 2</th>
					<th class="hidden-480 sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 150px;">Image 3</th>
					<th class="hidden-480 sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 150px;">Image 4</th>
					<th class="hidden-480 sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 150px;">Image 5</th>
					<th class="hidden-480 sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 150px;">Image 6</th>
					<th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="" style="width: 150px;"></th>
				</tr>
			</thead>
									
			
		<tbody role="alert" aria-live="polite" aria-relevant="all">
								
			
				<tr class="odd">
					<td class="center  sorting_1">
						<label><input type="checkbox"><span class="lbl"></span></label>
					</td>
					<td class=" "><a href="#">{{$progress->category->name}}</a></td>
					<td class=" ">
							

						{{$progress->project->project_name}}</td>
					<td class="">
					@if($progress->image1=='No-Image') 
							<img src="{{url('/images/no-image.png')}}" width="150px"  alt="">

						@else
					<img src="{{url('/images/development_progress/'.$progress->image1)}}" width="150px"  alt="">
						@endif
					</td>
					<td class="">
					@if($progress->image2=='No-Image') 
							<img src="{{url('/images/no-image.png')}}" width="150px"  alt="">

						@else
					<img src="{{url('/images/development_progress/'.$progress->image2)}}" width="150px"  alt="">
						@endif

					</td>
					<td class="">
						@if($progress->image3=='No-Image') 
							<img src="{{url('/images/no-image.png')}}" width="150px"  alt="">

						@else
					<img src="{{url('/images/development_progress/'.$progress->image3)}}" width="150px"  alt="">
						@endif
					</td>
					<td class="">
					@if($progress->image4=='No-Image') 
							<img src="{{url('/images/no-image.png')}}" width="150px"  alt="">

						@else
					<img src="{{url('/images/development_progress/'.$progress->image4)}}" width="150px"  alt="">
						@endif
					</td>
					<td class="">
					@if($progress->image5=='No-Image') 
							<img src="{{url('/images/no-image.png')}}" width="150px"  alt="">

						@else
					<img src="{{url('/images/development_progress/'.$progress->image5)}}" width="150px"  alt="">
						@endif
					</td>
					<td class="">
					@if($progress->image5=='No-Image') 
							<img src="{{url('/images/no-image.png')}}" width="150px"  alt="">

						@else
					<img src="{{url('/images/development_progress/'.$progress->image6)}}" width="150px"  alt="">
						@endif
					</td>
						<td class=" ">
						
						<div class="hidden-desktop visible-phone">
							<div class="inline position-relative">
								<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown"><i class="icon-caret-down icon-only"></i></button>
								<ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">
									<li><a href="#" class="tooltip-success" data-rel="tooltip" title="" data-placement="left" data-original-title="Edit"><span class="green"><i class="icon-edit"></i></span></a></li>
									<li><a href="#" class="tooltip-warning" data-rel="tooltip" title="" data-placement="left" data-original-title="Flag"><span class="blue"><i class="icon-flag"></i></span> </a></li>
									<li><a href="#" class="tooltip-error" data-rel="tooltip" title="" data-placement="left" data-original-title="Delete"><span class="red"><i class="icon-trash"></i></span> </a></li>
								</ul>
							</div>
						</div>
					</td>
				</tr> 
			
			</tbody></table><div class="row-fluid">
				<div class="span6">
				</div><div class="span6">
					<div class="dataTables_paginate paging_bootstrap pagination">
					</div></div></div></div>
	
</div>
  @endsection