@extends('layouts.app')
@section('title','Create News')
@section('content')
<div class="row-fluid">
	@if(count($errors)>0)
	<ul>
		
		<div class="note note-success"> @foreach($errors->all() as $error)
			<strong style="color: red"> {{  $error }}</strong><br>
			@endforeach
		</div>
		
	</ul>
	@endif
	<div class="span12">
		
		<div class="widget-box">
			<div class="widget-header">
				<h4>Add New Progress</h4>
				<span class="widget-toolbar">
				</span>	</div>
				<div class="widget-body"><div class="widget-body-inner">
					<div class="widget-main">
						<form   action="{{url('/development_progress')}}" method="post" enctype="multipart/form-data" accept-charset="utf-8">
							<div class="row-fluid">
								  <input type="hidden" name="noimg" value="No-Image" placeholder="">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<label for="form-field-8">Develpment Category</label>
								<select  name="category_id" class="span6" id="form-field-select-1">
									
									<option selected value="">Select Category</option>
									@foreach($category as $category)
									<option value="{{$category->id}}">{{$category->name}}</option>
									@endforeach
								</select>
							</div>
							<hr>
							<div class="row-fluid">
								<label for="form-field-9">Project Name</label>
								<select  name="project_id" class="span6" id="form-field-select-1">
									
									<option selected value="">Select Project</option>
									@foreach($projects as $project)
									<option value="{{$project->id}}">{{$project->project_name}}</option>
									@endforeach
								</select>
							</div>
							<div class="row-fluid">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<label for="form-field-8">Video Link 1</label>
								<input name="link1" value="{{old('link1')}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Video Link 1">
							</div>
							<div class="row-fluid">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<label for="form-field-8">Video Link 2</label>
								<input name="link2" value="{{old('link2')}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Video Link 2">
							</div>
							<hr>
							
							<div class="row-fluid">
								
								<label for="form-field-8">Development Progress Images</label>
								 <input type="file" name="image[]" value="" placeholder="Select Images" multiple>
							</div>
							<div class="form-actions">
								<button class="btn btn-info" type="submit"><i class="icon-ok"></i> Submit</button>
								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset"><i class="icon-undo"></i> Reset</button>
							</div> </form>
						</div>
					</div></div>
				</div>
			</div>
			
		</div>
		@endsection