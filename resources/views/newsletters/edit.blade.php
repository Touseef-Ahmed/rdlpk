@extends('layouts.app')
@section('title','Edit News')
@section('content')
<div class="row-fluid">
	@if(count($errors)>0)
	<ul>
		
		<div class="note note-success"> @foreach($errors->all() as $error)
			<strong style="color: red"> {{  $error }}</strong><br>
			@endforeach
		</div>
		
	</ul>
	@endif
	<div class="span12">
		
		<div class="widget-box">
			<div class="widget-header">
				<h4>Edit Newsletter</h4>
				<span class="widget-toolbar">
				</span>	</div>
				<div class="widget-body"><div class="widget-body-inner">
					<div class="widget-main">
					{{ Form::open(array('method'=>'PUT','route' => ['newsletters.update', $newsletters->id],'files' =>true,'enctype'=>'multipart/form-data')) }}

							<div class="row-fluid">
								  <input type="hidden" name="noimg" value="No-Image" placeholder="">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<label for="form-field-8">News Title</label>
								<input name="title" value="{{$newsletters->title}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Newsletters">
							</div>
							<hr>
							<div class="row-fluid">
								<label for="form-field-9">Project Name</label>
								<select  name="project_id" class="span6" id="form-field-select-1">
									
									<option selected value="{{$newsletters->project->id}}">{{$newsletters->project->project_name}}</option>
									@foreach($projects as $project)
									<option value="{{$project->id}}">{{$project->project_name}}</option>
									@endforeach
								</select>
							</div>
							<div class="row-fluid">
								
								<label for="form-field-8">Link</label>
								<input name="link" value="{{$newsletters->link}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Link">
							</div>
							<hr>
							<div class="row-fluid">
								<label for="form-field-9">Status</label>
								<select  name="status" class="span6" id="form-field-select-1">
									
									<option selected value="{{$newsletters->status}}">
									@if($newsletters->status==1)
						{{'Enabled'}}@else{{'Disabled'}}@endif	</option>
									<option value="1">Enabled</option>
									<option value="0">Disabled</option>
								</select>
							</div>
							<div class="row-fluid">
								<label for="form-field-8">Newsletter Pdf File</label>
								<img src="{{url('/images/newsletters/'.$newsletters->file)}}" width="100px"  alt="No Image"><br>
								<input type="file" name="image" value="" placeholder="">
							</div><hr>
						<div class="row-fluid">
								<label for="form-field-8">Newsletters</label>
								<img src="{{url('/images/newsletters/'.$newsletters->thumbnail)}}" width="300px"  alt="No Image"><br>
								<input type="file" name="thumbnail" value="" placeholder="">
							</div>
						
							<div class="form-actions">
								<button class="btn btn-info" type="submit"><i class="icon-ok"></i> Submit</button>
								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset"><i class="icon-undo"></i> Reset</button>
							</div> </form>
						</div>
					</div></div>
				</div>
			</div>
			
		</div>
		@endsection