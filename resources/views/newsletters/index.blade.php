@extends('layouts.app')
@section('title','Newsletter List')
@section('content')<button class="btn-link" style="float: right">  <a href="{{url('newsletters/create')}}">Create Newsletters</a></button><br><br>
<div class="row-fluid">
	<!-- <h3 class="header smaller lighter blue">jQuery dataTables</h3> -->
	
	<div class="table-header">
		Newsletters List
	</div>
	@if(session('message'))
	<div class="note note-success"><p>{{session('message')}}</p> </div>
	@endif
	<div id="table_report_wrapper" class="dataTables_wrapper" role="grid"><div class="row-fluid"><div class="span6"><div id="table_report_length" class="dataTables_length"><label>Display <select size="1" name="table_report_length" aria-controls="table_report"><option value="10" selected="selected">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> records</label></div></div><div class="span6"><div class="dataTables_filter" id="table_report_filter"><form method="get" action="{{url('/newsletters/search')}}">
	{!! Form::text("title",isset($title)?$Newsletters->title:null,[
	"class"=>"form-control input-inline input-large".($errors->has('title')?" is-invalid":"")
	,"autofocus"
	,"placeholder"=>"Enter Title"
	,"required"]) !!}
	<select  name="project_id" class="span6" id="form-field-select-1">
		
		@foreach($projects as $project)
		<option value="{{$project->id}}">{{$project->project_name}}</option>
		@endforeach
	</select>
	{{ Form::button('<i class="fa fa-search"></i> &nbsp;Search</button>', ['type' => 'submit', 'class' =>'btn btn-small btn-primary'] )  }}
</form></div></div></div><table id="table_report" class="table table-striped table-bordered table-hover dataTable" aria-describedby="table_report_info">
<thead>
	<tr role="row"><th class="center sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="
		
		" style="width: 71px;">
		<label><input type="checkbox"><span class="lbl"></span></label>
		</th><th class="sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Domain: activate to sort column ascending" style="width: 207px;">Title</th><th class="sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Price: activate to sort column ascending" style="width: 140px;">Project Name</th><th class="hidden-480 sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Clicks: activate to sort column ascending" style="width: 151px;">Status</th><th class="hidden-480 sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 202px;">Sort Order</th><th class="sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Domain: activate to sort column ascending" style="width: 207px;">Thumbnail</th>
		<th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="" style="width: 50px;">Actions</th></tr>
	</thead>
	
	
	<tbody role="alert" aria-live="polite" aria-relevant="all">
		@php $count=0;
		@endphp
		@foreach($newsletters as $newsletters)
		@php $count++;
		@endphp
		<tr class="odd">
			<td class="center  sorting_1">
				<label><input type="checkbox"><span class="lbl"></span></label>
			</td>
			<td class=" "><a href="#">{{$newsletters->title}}</a></td>
			<td class=" ">
				
			{{$newsletters->project->project_name}}</td>
			<td class="">@if($newsletters->status==1)
				{{'Enabled'}}@else{{'Disabled'}}@endif
			</td>
			<td class="">{{$newsletters->sort_order}}</td>
			<td class="">
				
			<img src="{{url('/images/newsletters/'.$newsletters->thumbnail)}}" width="100px"  alt="No Image"></td>
			<td class=" ">
				<div class="hidden-phone visible-desktop btn-group">
					<a href="{{ route('newsletters.show', $newsletters->id) }}">
					<button class="btn btn-mini btn-success"><i class="icon-search"></i></button> </a>
					<a href="{{ route('newsletters.edit', $newsletters->id) }}">
					<button class="btn btn-mini btn-info"><i class="icon-edit"></i></button></a>
					{!! Form::open(['class'=>'confirm_delete_form inline','method' => 'DELETE','route' => ['newsletters.destroy', $newsletters->id ] ,'data-partial'=>'/newsletters']) !!}	<button class="btn btn-mini btn-danger"><i class="icon-trash"></i></button> {!! Form::close() !!}
					<!-- <button class="btn btn-mini btn-warning"><i class="icon-flag"></i></button> -->
				</div>
				<div class="hidden-desktop visible-phone">
					<div class="inline position-relative">
						<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown"><i class="icon-caret-down icon-only"></i></button>
						<ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">
							<li><a href="#" class="tooltip-success" data-rel="tooltip" title="" data-placement="left" data-original-title="Edit"><span class="green"><i class="icon-edit"></i></span></a></li>
							<li><a href="#" class="tooltip-warning" data-rel="tooltip" title="" data-placement="left" data-original-title="Flag"><span class="blue"><i class="icon-flag"></i></span> </a></li>
							<li><a href="#" class="tooltip-error" data-rel="tooltip" title="" data-placement="left" data-original-title="Delete"><span class="red"><i class="icon-trash"></i></span> </a></li>
						</ul>
					</div>
				</div>
			</td>
		</tr>  @endforeach
		</tbody></table><div class="row-fluid"><div class="span6"><div class="dataTables_info" id="table_report_info">Showing 1 to 10 of 22 entries</div></div><div class="span6"><div class="dataTables_paginate paging_bootstrap pagination"><ul><li class="prev disabled"><a href="#"><i class="icon-double-angle-left"></i></a></li><li class="active"><a href="#">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li class="next"><a href="#"><i class="icon-double-angle-right"></i></a></li></ul></div></div></div></div>
		
	</div>
	@endsection