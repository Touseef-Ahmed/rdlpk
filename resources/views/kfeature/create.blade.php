@extends('layouts.app')
@section('title','Create News')
@section('content')
<div class="row-fluid">
	@if(count($errors)>0)
	<ul>
		
		<div class="note note-success"> @foreach($errors->all() as $error)
			<strong style="color: red"> {{  $error }}</strong><br>
			@endforeach
		</div>
		
	</ul>
	@endif
	<div class="span12">
		
		<div class="widget-box">
			<div class="widget-header">
				<h4>Add New Feature</h4>
				<span class="widget-toolbar">
				</span>	</div>
				<div class="widget-body"><div class="widget-body-inner">
					<div class="widget-main">
						
								{!! Form::open(['url'=>'kfeature','files' =>true,'enctype'=>'multipart/form-data']) !!}
							<div class="row-fluid">
								  <input type="hidden" name="noimg" value="No-Image" placeholder="">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<label for="form-field-8">Feature Name</label>
								<input name="name" value="{{old('name')}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Features Name">
							</div>
							<hr>
							<div class="row-fluid">
								<label for="form-field-9">Feature Category</label>
								<select  name="kfcategory_id" class="span6" id="form-field-select-1">
									
									<option selected value="">Select Category</option>
									@foreach($kfcategory as $kfcategory)
									<option value="{{$kfcategory->id}}">{{$kfcategory->name}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-actions">
								<button class="btn btn-info" type="submit"><i class="icon-ok"></i> Submit</button>
								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset"><i class="icon-undo"></i> Reset</button>
							</div> </form>
						</div>
					</div></div>
				</div>
			</div>
			
		</div>
		@endsection