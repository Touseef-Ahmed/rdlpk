@extends('layouts.app')
@section('title','Edit sfeature')
@section('content')
<div class="row-fluid">
	@if(count($errors)>0)
	<ul>
		
		<div class="note note-success"> @foreach($errors->all() as $error)
			<strong style="color: red"> {{  $error }}</strong><br>
			@endforeach
		</div>
		
	</ul>
	@endif
	<div class="span12">
		
		<div class="widget-box">
			<div class="widget-header">
				<h4>Edit Salient Features</h4>
				<span class="widget-toolbar">
				</span>	</div>
				<div class="widget-body"><div class="widget-body-inner">
					<div class="widget-main">
					{{ Form::open(array('method'=>'PUT','route' => ['sfeature.update', $sfeature->id],'files' =>true,'enctype'=>'multipart/form-data')) }}

							<div class="row-fluid">
								  <input type="hidden" name="noimg" value="No-Image" placeholder="">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<label for="form-field-8">Title</label>
								<input name="title" value="{{$sfeature->title}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter Title">
							</div>
							
							<div class="row-fluid">
								<label for="form-field-9">Project Name</label>
								<select  name="project_id" class="span6" id="form-field-select-1">
									
									<option selected value="{{$sfeature->project->id}}">{{$sfeature->project->project_name}}</option>
									@foreach($projects as $project)
									<option value="{{$project->id}}">{{$project->project_name}}</option>
									@endforeach
								</select>
							</div>
							
							<hr>
							
							<div class="row-fluid">
								<label for="form-field-11">Detail</label>
								<textarea name="detail" id="summary-ckeditor" class="autosize-transition span6" style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 58px;">{{$sfeature->detail}}</textarea>
							</div><hr>
							<table>
								
								<tbody>
									<tr>
										<td><div class="row-fluid">
								<label for="form-field-8">Image 1</label>
								<img src="{{url('/images/sfeatures/'.$sfeature->image1)}}" width="150px"  alt="No Image"><br>
								<input type="file" name="image1" value="" placeholder="">
							</div></td>
							<td><div class="row-fluid">
								<label for="form-field-8">Image 2</label>
								<img src="{{url('/images/sfeatures/'.$sfeature->image2)}}" width="150px"  alt="No Image"><br>
								<input type="file" name="image2" value="" placeholder="">
							</div></td>
							<td>	
							<div class="row-fluid">
								<label for="form-field-8">Image 3</label>
								<img src="{{url('/images/sfeatures/'.$sfeature->image3)}}" width="150px"  alt="No Image"><br>
								<input type="file" name="image3" value="" placeholder="">
							</div></td>
								<td><div class="row-fluid">
								<label for="form-field-8">Image 4</label>
								<img src="{{url('/images/sfeatures/'.$sfeature->image4)}}" width="150px"  alt="No Image"><br>
								<input type="file" name="image4" value="" placeholder="">
							</div></td>
								<td><div class="row-fluid">
								<label for="form-field-8">Image 5</label>
								@if(empty($sfeature->image5))
								no-image.png
								@else
								<img src="{{url('/images/sfeatures/'.$sfeature->image5)}}" width="150px"  alt="No Image"><br>
								@endif
								<input type="file" name="image5" value="" placeholder="">
							</div></td>
									</tr>
							
								</tbody>
							</table>
							
						
						
							<div class="form-actions">
								<button class="btn btn-info" type="submit"><i class="icon-ok"></i> Submit</button>
								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset"><i class="icon-undo"></i> Reset</button>
							</div> </form>
						</div>
					</div></div>
				</div>
			</div>
			<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace( 'summary-ckeditor' );
</script>
		</div>
		@endsection