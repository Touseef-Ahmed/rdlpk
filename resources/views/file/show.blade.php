@extends('layouts.app')
@section('title','Projects List')
@section('content')<button class="btn-link" style="float: right">  <a href="{{url('news/create')}}">Create New News</a></button><br><br>
<div class="row-fluid">
	<!-- <h3 class="header smaller lighter blue">jQuery dataTables</h3> -->
	
	<div class="table-header">
		News Detail 
	</div>
	  @if(session('message'))
        <div class="note note-success"><p>{{session('message')}}</p> </div>
        @endif
		<div id="table_report_wrapper" class="dataTables_wrapper" role="grid"><div class="row-fluid"><div class="span6"><div id="table_report_length" class="dataTables_length"><label>Display <select size="1" name="table_report_length" aria-controls="table_report"><option value="10" selected="selected">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> records</label></div></div><div class="span6"><div class="dataTables_filter" id="table_report_filter"><label>Search: <input type="text" aria-controls="table_report"></label></div></div></div><table id="table_report" class="table table-striped table-bordered table-hover dataTable" aria-describedby="table_report_info">
			<thead>
				<tr role="row"><th class="center sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="
						
					" style="width: 71px;">
						<label><input type="checkbox"><span class="lbl"></span></label>
					</th><th class="sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Domain: activate to sort column ascending" style="width: 307px;">News Heading</th><th class="sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Price: activate to sort column ascending" style="width: 140px;">Project Name</th><th class="hidden-480 sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Clicks: activate to sort column ascending" style="width: 151px;">News Location</th><th class="hidden-480 sorting" role="columnheader" tabindex="0" aria-controls="table_report" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 302px;">News Detail</th>
					<th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="" style="width: 150px;">Sort Order</th><th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="" style="width: 150px;">Created Date</th>
					<th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="" style="width: 150px;">Updated Date</th><th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="" style="width: 150px;"></th>
				</tr>
			</thead>
									
			
		<tbody role="alert" aria-live="polite" aria-relevant="all">
								
			<tr class="odd">
					<td class="center  sorting_1">
						<label><input type="checkbox"><span class="lbl"></span></label>
					</td>
					<td class=" "><a href="#">{{$news->heading}}</a></td>
					<td class=" ">
							

						{{$news->project->project_name}}</td>
					<td class="">{{$news->location}}</td>
					<td class="">{{$news->detail}}</td>
						<td class="">{{$news->sort_order}}</td><td class="">{{$news->created_at}}</td><td class="">{{$news->created_at}}</td><td class=""></td>
				</tr>
				<tr><tr ><td colspan="10" style="text-align: center;"><h4>Images</h4></td></tr>
                            <td><img src="{{url('/images/news/'.$news->image1)}}" width="100px"  alt="No Image"></td>
                            <td><img src="{{url('/images/news/'.$news->image2)}}" width="100px"  alt="No Image"></td>
                            <td><img src="{{url('/images/news/'.$news->image3)}}" width="100px"  alt="No Image"></td>
                            <td><img src="{{url('/images/news/'.$news->image4)}}" width="100px"  alt="No Image"></td>
                            <td><img src="{{url('/images/news/'.$news->image5)}}" width="100px"  alt="No Image"></td>
                            <td><img src="{{url('/images/news/'.$news->image6)}}" width="100px"  alt="No Image"></td>
                            <td><img src="{{url('/images/news/'.$news->image7)}}" width="100px"  alt="No Image"></td>
                            <td><img src="{{url('/images/news/'.$news->image8)}}" width="100px"  alt="No Image"></td>
                            <td><img src="{{url('/images/news/'.$news->image9)}}" width="100px"  alt="No Image"></td>
                            <td><img src="{{url('/images/news/'.$news->image10)}}" width="100px"  alt="No Image"></td>
                            </tr>

			</tbody></table><div class="row-fluid">
				<div class="span6">
				</div><div class="span6">
					<div class="dataTables_paginate paging_bootstrap pagination">
					</div></div></div></div>
	
</div>
  @endsection