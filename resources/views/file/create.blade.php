@extends('layouts.app')
@section('title','Create News')
@section('content')
<div class="row-fluid">
	@if(count($errors)>0)
	<ul>
		
		<div class="note note-success"> @foreach($errors->all() as $error)
			<strong style="color: red"> {{  $error }}</strong><br>
			@endforeach
		</div>
		
	</ul>
	@endif
	<div class="span12">
		
		<div class="widget-box">
			<div class="widget-header">
				<h4>Add New File</h4>
				<span class="widget-toolbar">
				</span>	</div>
				<div class="widget-body"><div class="widget-body-inner">
					<div class="widget-main">
						<form   action="{{url('/file')}}" method="post" enctype="multipart/form-data" accept-charset="utf-8">
							{{csrf_field()}}

							<div class="row-fluid">
								<label for="form-field-9">Project Name</label>
								<select  name="project_id" class="span6" id="form-field-select-1">
									
									<option selected value="">Select Project</option>
									@foreach($projects as $project)
									<option value="{{$project->id}}">{{$project->project_name}}</option>
									@endforeach
								</select>
							</div>
							<div class="row-fluid">
								<label for="form-field-9">Home Category</label>
								<select  name="category_id" class="span6" id="form-field-select-1">
									
									<option selected value="">Select Category</option>
									@foreach($category as $category)
									<option value="{{$category->id}}">{{$category->name}}</option>
									@endforeach
								</select>
							</div>
							
							<div class="row-fluid">
								<label for="form-field-9">Size</label>
								<select  name="size_id" class="span6" id="form-field-select-1">
									
									<option selected value="">Select Size</option>
									@foreach($size as $size)
									<option value="{{$size->id}}">{{$size->name}}</option>
									@endforeach
								</select>
							</div>
							<hr>
							
							
							<div class="row-fluid">
								
								<label for="form-field-8">File (Images) </label>
								 <input type="file" name="image" value="" placeholder="Select Images/Files" multiple>
							</div>
							<div class="form-actions">
								<button class="btn btn-info" type="submit"><i class="icon-ok"></i> Submit</button>
								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset"><i class="icon-undo"></i> Reset</button>
							</div> </form>
						</div>
					</div></div>
				</div>
			</div>
			
		</div>
		@endsection