@extends('layouts.app')
@section('title','Edit News')
@section('content')
<div class="row-fluid">
	@if(count($errors)>0)
	<ul>
		
		<div class="note note-success"> @foreach($errors->all() as $error)
			<strong style="color: red"> {{  $error }}</strong><br>
			@endforeach
		</div>
		
	</ul>
	@endif
	<div class="span12">
		
		<div class="widget-box">
			<div class="widget-header">
				<h4>Edit News</h4>
				<span class="widget-toolbar">
				</span>	</div>
				<div class="widget-body"><div class="widget-body-inner">
					<div class="widget-main">
						{{ Form::open(array('method'=>'PUT','route' => ['file.update', $file->id],'files' =>true,'enctype'=>'multipart/form-data')) }}
						<div class="row-fluid">
							<label for="form-field-9">Project Name</label>
							<select  name="project_id" class="span6" id="form-field-select-1">
								<option selected value="{{$file->project->id}}">{{$file->project->project_name}}</option>
								@foreach($projects as $project)
								<option value="{{$project->id}}">{{$project->project_name}}</option>
								@endforeach
							</select>
						</div>
						<input type="hidden" name="noimg" value="No-Image" placeholder="">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						
						<div class="row-fluid">
							<label for="form-field-9">Home Category</label>
							<select  name="category_id" class="span6" id="form-field-select-1">
								<option selected value="{{$file->category->id}}">{{$file->category->name}}</option>
								@foreach($category as $category)
								<option value="{{$category->id}}">{{$category->name}}</option>
								@endforeach
							</select>
						</div>
							<div class="row-fluid">
							<label for="form-field-9">Sizw</label>
							<select  name="size_id" class="span6" id="form-field-select-1">
								<option selected value="{{$file->size->id}}">{{$file->size->name}}</option>
								@foreach($size as $size)
								<option value="{{$size->id}}">{{$size->name}}</option>
								@endforeach
							</select>
						</div>
						<hr>
						
						
						<div class="row-fluid">
							<label for="form-field-11">File/Image</label>
							<img src="{{url('/images/files/'.$file->image)}}" width="200px"  alt="No Image"><br>
							<input type="file" name="image" value="" placeholder="">
						</div>
						
						<div class="form-actions">
							<button class="btn btn-info" type="submit"><i class="icon-ok"></i> Submit</button>
							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset"><i class="icon-undo"></i> Reset</button>
						</div> </form>
					</div>
				</div></div>
			</div>
		</div>
		
	</div>
	@endsection