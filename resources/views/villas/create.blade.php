@extends('layouts.app')
@section('title','Create News')
@section('content')
<div class="row-fluid">
	@if(count($errors)>0)
	<ul>
		
		<div class="note note-success"> @foreach($errors->all() as $error)
			<strong style="color: red"> {{  $error }}</strong><br>
			@endforeach
		</div>
		
	</ul>
	@endif
	<div class="span12">
		
		<div class="widget-box">
			<div class="widget-header">
				<h4>Create New News</h4>
				<span class="widget-toolbar">
				</span>	</div>
				<div class="widget-body"><div class="widget-body-inner">
					<div class="widget-main">
						<form   action="{{url('/news')}}" method="post" enctype="multipart/form-data" accept-charset="utf-8">
							<div class="row-fluid">
								  <input type="hidden" name="noimg" value="No-Image" placeholder="">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<label for="form-field-8">News Heading</label>
								<input name="heading" value="{{old('heading')}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter News Heading">
							</div>
							<hr>
							<div class="row-fluid">
								<label for="form-field-9">Project Name</label>
								<select  name="project_id" class="span6" id="form-field-select-1">
									
									<option selected value="">Select Project</option>
									@foreach($projects as $project)
									<option value="{{$project->id}}">{{$project->project_name}}</option>
									@endforeach
								</select>
							</div>
							<div class="row-fluid">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<label for="form-field-8">News Location</label>
								<input name="location" value="{{old('location')}}" class="span6 limited" type="text" id="form-field-4" placeholder="Enter location Location">
							</div>
							<hr>
							
							<div class="row-fluid">
								<label for="form-field-11">News Detail</label>
								<textarea name="detail" id="form-field-11" class="autosize-transition span6" style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 58px;">{{old('detail')}}</textarea>
							</div>
							<div class="row-fluid">
								
								<label for="form-field-8">News Images</label>
								 <input type="file" name="image[]" value="" placeholder="Select Images for News" multiple>
							</div>
							<div class="form-actions">
								<button class="btn btn-info" type="submit"><i class="icon-ok"></i> Submit</button>
								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset"><i class="icon-undo"></i> Reset</button>
							</div> </form>
						</div>
					</div></div>
				</div>
			</div>
			
		</div>
		@endsection