<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!*/



Route::get('/royalorchard', 'WebController@index');
Route::POST('/userData/getUserData','UserDataController@getUserData');
Route::POST('/slider/getSliderData','SliderController@getSliderData');
Route::resource('userData','UserDataController');
Route::get('userData/edit/{id}', 'UserDataController@edit');
///Route::post('userData/update', 'UserDataController@update');



//Route::resource('userData','UserDataController');


Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('projects/search','ProjectController@search');
Route::resource('projects', 'ProjectController');
Route::get('project_location/search','ProjectLocationController@search');
Route::resource('project_location','ProjectLocationController');
Route::resource('master_plan','MasterPlanController');
Route::get('news/search','NewsController@search');
Route::resource('news','NewsController');
Route::get('newsletters/search','NewsletterController@search');
Route::resource('newsletters','NewsletterController');
Route::resource('slider','SliderController');
Route::get('gallery/search','GalleryController@search');
Route::resource('gallery','GalleryController');
Route::resource('kfcategory','KfcategoryController');
Route::get('kfeature/search','KfeatureController@search');
Route::resource('kfeature','KfeatureController');
Route::resource('kfgallery','KfgalleryController');
Route::resource('sfeature','SfeatureController');
Route::resource('homes_category','HomesCategoryController');
Route::resource('villas','VillasController');
Route::get('payment_plan/search','PaymentPlanController@search');
Route::resource('payment_plan','PaymentPlanController');
Route::get('development_progress/search','DevelopmentProgressController@search');
Route::resource('development_categories','DevelopmentCategoryController');
Route::resource('development_progress','DevelopmentProgressController');
Route::resource('royal_medicare','RoyalMedicareController');
Route::resource('salescenter','SalescenterController');
Route::resource('size','SizeController');
Route::get('file/search','FileController@search');
Route::resource('file','FileController');
Route::resource('associates','AssociatesController');

Route::get('web/mission','WebController@mission');
Route::get('web/objective','WebController@objective');
Route::get('web/brouchers','WebController@brouchers');
Route::get('web/newsletters','WebController@newsletters');
Route::get('web/news','WebController@news');
Route::get('web/news_detail/{id}','WebController@news_detail');
Route::get('web/contactus','WebController@contactus');
Route::get('web/multan','WebController@multan');
Route::get('web/sargodha','WebController@sargodha');
Route::get('web/sahiwal','WebController@sahiwal');
Route::resource('web','WebController');
Route::resource('setting','SettingController');
Route::post('sendmail', 'EmailController@index');

/*Route::get('/', function () {
return view('web.index');
});*/
Auth::routes();
Route::get('/home', 'WebController@home')->name('home');
Route::get('/royalorchard', 'WebController@index');
/*Route::get('/royalorchard', function () {
    return view('web.index');
})->name('royalorchard');*/
///Route::get('/royalorchard', 'WebController@index')->name('royalorchard');