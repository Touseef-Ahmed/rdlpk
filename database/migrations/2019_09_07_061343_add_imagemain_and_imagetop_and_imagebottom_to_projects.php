<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImagemainAndImagetopAndImagebottomToProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->string('imagemain');
            $table->string('imagetop');
            $table->string('imagebottom');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
             $table->dropColumn(['imagemain']);
             $table->dropColumn(['imagetop']);
             $table->dropColumn(['imagebottom']);
        });
    }
}
